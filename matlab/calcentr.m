function [h,t,s]=calcentr(fname,method,s,isfirst,islast) ;
% Calculate entropy from file using a defined method. Returns the entropy,
% the time needed and a state which can be passed at the next invocation.
% 
% The methods are "brute" (brute force), "bbf" (best bin first), 
% "bbf-upd" (BBF, no incremental update), "bbf-tbb" (BBF, no tight boxes), 
% "bbf-ann" (repeated NN search), "ann" (ANN library), "flann" (FLANN library), 
% "lsh" (LSH)
    
%fd=fopen('../tmp/calcentr.cmd','w') ;

%maxvisited=400 ;
      
oldfolder=cd('~/work/increst/tmp') ;


if isfirst, % the very first time we are called
    if ~isfield('suffix',s),
        s.suffix=num2str(randi(1000000)) ;
    end
    system([ 'mkfifo frommatlab' s.suffix ]) ;
    system([ 'mkfifo tomatlab' s.suffix ]) ;
    system(['~/work/increst/bin/startallnn ' s.suffix] ) ;
    s.fd=fopen([ 'frommatlab' s.suffix ],'w') ;
    s.fd2=fopen([ 'tomatlab' s.suffix ],'r') ;
    a=load(fname) ;
    [s.n,s.dim]=size(a) ;
    writef(s.fd,'set_dim %d\n',s.dim) ;
end    

%delta=0.5 ;
delta=0.2 ;
lshw=0.25 ;
if s.dim<=2,
    % d=2
      maxvisited=42 ;
      leafsize=15 ;
      nlshfuncs=500 ;
      nlshtables=1 ;
      flannchecks=100 ;
      flanntrees=1 ;
      annepsilon=0.5 ;
elseif s.dim<=18,
      %    case 18 % d=3
      maxvisited=100 ;
      leafsize=50 ;
      nlshfuncs=500 ;
      nlshtables=4 ;
      flannchecks=150 ;
      flanntrees=1 ;
      annepsilon=0.5 ;
elseif s.dim<=50,
      %  case 50 % d=5
      annepsilon=0.9 ;
      maxvisited=400 ;
      leafsize=100 ;
      nlshfuncs=500 ;
      nlshtables=20 ;
      flannchecks=150 ;
      flanntrees=1 ;
%  otherwise
else
      annepsilon=0.9 ;
      maxvisited=500 ;
      leafsize=150 ;
      nlshfuncs=40 ;
      nlshtables=10 ;
      flannchecks=150 ;
      flanntrees=1 ;
end


writef(s.fd,'read_data_pts %s\n',fname) ;
switch method
  case 'brute'
    writef(s.fd,'brute_entropy\n') ;
    r=fscanf(s.fd2,'%f',2) ;
    % r=readnums(s.fd2,2) ;
    h=r(1) ;
    t=r(2) ;
  case 'ann'
    writef(s.fd,'set_ann_epsilon %f\n',annepsilon) ;
    writef(s.fd,'ann_build_tree\n') ;
    writef(s.fd,'ann_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'flann'
    writef(s.fd,'set_flann_checks %d\n',flannchecks) ;
    writef(s.fd,'set_flann_trees %d\n',flanntrees) ;
    writef(s.fd,'flann_build_tree\n') ;
    writef(s.fd,'flann_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'lsh'
    writef(s.fd,'set_nlshfuncs %d\n',nlshfuncs) ;
    writef(s.fd,'set_nlshtables %d\n',nlshtables) ;
    writef(s.fd,'set_lshw %f\n',lshw) ;
    writef(s.fd,'lsh_build_tree\n') ;
    writef(s.fd,'lsh_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'bbf-upd'
    writef(s.fd,'set_leaf_size %d\n',leafsize) ;
    writef(s.fd,'set_max_visited %d\n',maxvisited) ;
    writef(s.fd,'vnucko_build_tree\n') ;
    writef(s.fd,'vnucko_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    %r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'bbf'
    writef(s.fd,'set_leaf_size %d\n',leafsize) ;
    writef(s.fd,'set_delta %f\n',delta) ;
    writef(s.fd,'set_max_visited %d\n',maxvisited) ;
    if isfirst,
        writef(s.fd,'vnucko_build_tree\n') ;
    else
        writef(s.fd,'vnucko_update_tree\n') ;
    end
    writef(s.fd,'vnucko_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'bbf-tbb'
    writef(s.fd,'set_leaf_size %d\n',leafsize) ;
    writef(s.fd,'set_max_visited %d\n',maxvisited) ;
    writef(s.fd,'set_usetbb %d\n',0) ;
    if isfirst,
        writef(s.fd,'vnucko_build_tree\n') ;
    else
        writef(s.fd,'vnucko_update_tree\n') ;
    end
    writef(s.fd,'vnucko_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
  case 'bbf-ann'
    writef(s.fd,'set_leaf_size %d\n',leafsize) ;
    writef(s.fd,'set_delta %f\n',delta) ;
    writef(s.fd,'set_max_visited %d\n',maxvisited) ;
    if isfirst,
        writef(s.fd,'vnucko_build_tree\n') ;
    else
        writef(s.fd,'vnucko_update_tree\n') ;
    end
    writef(s.fd,'vnucko_nnn_entropy\n') ;
    r=fscanf(s.fd2,'%f',3) ;
    % r=readnums(s.fd2,3) ;
    h=r(2) ;
    t=r(1)+r(3) ;
    
  otherwise
    error('Unknown method') ; 
end

if islast,
    writef(s.fd,'quit\n') ;
    fclose(s.fd2) ;
    fclose(s.fd) ;
    system([ 'rm frommatlab' s.suffix ]) ;
    system([ 'rm tomatlab' s.suffix ]) ;
    system([ 'rm logfile' s.suffix ]) ;
    s=[] ;
end

cd(oldfolder) ;

function writef(f,varargin) 
fprintf(f,varargin{:}) ;
fprintf(varargin{:}) ;

function r=readnums(fd,num) ;
  r=zeros(num,1) ;
  disp(['readnums num=' num2str(num)]) ;
  for i=1:num,
      disp([ 'reading line ' num2str(i) ]) ;
      s=fgetl(fd) ;
      disp([ 'read ' s ]) ;
      r(i)=sscanf(s,'%f') ;
  end
  disp([ 'readnums: ' mat2str(r) ]) 
  

















    
    