% Visualize the results of the testentrbbf and testentrann experiments

clear all 
close all
% load BBF results
load testentrbbf1e5
%load testentrbbf1e5l30
bbftottime=bldtm+srtm ;
bbfenterr=enterr ;
bbfmv=mv ;

% load ANN results
load testentrann1e5
annenterr=enterr ;
anntottime=bldtm+srtm ;
anneps=mv ;

clc='kbrcmy' ;

if 0,
figure(1) ;
subplot(211) ; clf ; hold on ; title('BBF total time wrt M') ;
for i=nd:-1:1,
  loglog(bbfmv,bbftottime(i,:),[ '-' clc(i) ]) ;
end ;
legend('2','5','10','20') ;
subplot(212) ; hold on ; title('BBF error wrt M') ;
for i=1:nd,
  plot(bbfmv,bbfenterr(i,:),[ '-' clc(i) ]) ;
end ;
legend('5','10','20') ;
figure(2) ;
subplot(211) ; hold on ; title('ANN total time wrt eps') ;
for i=1:nd,
  semilogy(anneps,anntottime(i,:),[ '-' clc(i) ]) ;
end ;
legend('5','10','20') ;
subplot(212) ; hold on ; title('ANN error wrt eps') ;
for i=1:nd,
  plot(anneps,annenterr(i,:),[ '-' clc(i) ]) ;
end ;
legend('5','10','20') ;
end ;
  
% visualize for each 'd' separately
for i=1:nd,
  figure ;
  d=dv(i) ;
  disp(['Dimension ' num2str(d)]) ;
  loglog(bbftottime(i,:),bbfenterr(i,:),'r-',anntottime(i,:),annenterr(i,:),'g-') ;
  legend('BBF','ANN') ;
  xlabel('Time [s]') ;
  ylabel('MSE of entropy') ;
  title(sprintf('Time versus error for KL entropy estimation, d=%d',d)) ;
end ;

% put everything into one graph, in relative coordinates
figure ; % hold on ;
% normalize errors
%for id=1:nd,
for id=[1 2 3 5 6]
  d=dv(id) ; 
  trueentr=d*(0.5+0.5*log(2*pi)) ;
  bbfrelenterr(id,:)=bbfenterr(id,:)/trueentr ;
  annrelenterr(id,:)=annenterr(id,:)/trueentr ;
%  loglog(bbftottime(id,:),bbfrelenterr(id,:),[ 'o-' clc(id) ]) ; 
end ;
loglog(bbftottime',bbfrelenterr','o-') ; hold on ;
loglog(anntottime',annrelenterr','o--') ; hold off ;
xlabel('Time [s]') ;
ylabel('Relative MSE') ;
s={} ;
for id=1:nd,
  d=dv(id) ;
  s{id}=sprintf('BBF d=%d',d) ;
  s{id+nd}=sprintf('ANN d=%d',d) ;
%  loglog(anntottime(id,:),annrelenterr(id,:),[ 'o--' clc(id)] ) ;
end ;
legend(s) ;
hold off 

% show the results with L=30
load testentrbbf1e5l30
bbftottime=bldtm+srtm ;
bbfenterr=enterr ;
bbfmv=mv ;

figure ; % hold on ;
% normalize errors
for id=[1 2 3 5 6]
%for id=1:nd,
  d=dv(id) ; 
  trueentr=d*(0.5+0.5*log(2*pi)) ;
  bbfrelenterr(id,:)=bbfenterr(id,:)/trueentr ;
  annrelenterr(id,:)=annenterr(id,:)/trueentr ;
%  loglog(bbftottime(id,:),bbfrelenterr(id,:),[ 'o-' clc(id) ]) ; 
end ;
h1=loglog(bbftottime',bbfrelenterr','o-') ; hold on ;
h2=loglog(anntottime',annrelenterr','o--') ; hold off ;
xlabel('Time [s]') ;
ylabel('Relative MSE') ;
s={} ;
for id=1:nd,
  d=dv(id) ;
  s{id}=sprintf('BBF d=%d',d) ;
  s{id+nd}=sprintf('ANN d=%d',d) ;
%  loglog(anntottime(id,:),annrelenterr(id,:),[ 'o--' clc(id)] ) ;
end ;
  set(h1,'linewidth',2) ;
  set(h2,'linewidth',2) ;
  set(gca,'FontSize',14) ;
legend(s,'Location','EastOutside') ;
legend('boxoff') ;
print('-depsc2','entrgraphcomb.eps') ;
%hold off 

disp('converting') ;
system('for i in entrgraph*.eps ; do echo $i ; epstopdf $i ; done') ;
system('cp -v entrgraph*.pdf ~/tex/annsearch/figs/') ;
