function mres=showrot(d,method) ;
% shows the results of evalrotdata.m
%     
% Jan Kybic, April 2011
    
%angs=-5:0.2:5 ;
%angs=[-5 -0.2 0 0.2 5] ;
load(['../tmp/evalrot-' num2str(d) '-' method ]) ;
nang=length(angs) ;

figure ; clf ; 
plot(angs,mres(1,:)) ; xlabel('angle [deg]') ; ylabel('entropy') ;
meantime=mean(mres(2,:)) ;
sdevtime=std(mres(2,:)) ;
disp(['Method ' method ' meantime ' num2str(meantime) ' std ' num2str(sdevtime) ]) ;
