% Create a LaTeX table presenting the results of testdistr2.m

% load all data
tv={'normal','uniform','corrnormal','corruniform','clustered', ...
    'subspace','imgneighb'} ;
nt=length(tv) ;
fc=load(['testdistr2' tv{1}]) ;
nd=size(fc.srtm,1) ;
nn=size(fc.srtm,2) ;
dv=fc.dv ;
nv=fc.nv ;

bldtm=zeros(nd,nn,nt) ; 
srtm=zeros(nd,nn,nt) ; % search times
annbldtm=zeros(nd,nn,nt) ; 
annsrtm=zeros(nd,nn,nt) ; % search times
flannbldtm=zeros(nd,nn,nt) ; 
flannsrtm=zeros(nd,nn,nt) ; % search times

for it=1:nt,
    fc=load(['testdistr2' tv{it}]) ;
    % fcf is only temporary, since we had to recalculate it
    % fcf=load(['testdistr2flann' tv{it}]) ;
    bldtm(:,:,it)=fc.bldtm(:,:,1) ;
    srtm(:,:,it)=fc.srtm(:,:,1) ;
    annbldtm(:,:,it)=fc.annbldtm(:,:,1) ;
    annsrtm(:,:,it)=fc.annsrtm(:,:,1) ;
    flannbldtm(:,:,it)=fc.flannbldtm(:,:,1) ;
    flannsrtm(:,:,it)=fc.flannsrtm(:,:,1) ;
end ;

fd=fopen('distr5.tex','w') ;

% second table for fixed N and different 'd' and 'distribution
% load testdistr3

fprintf(fd,'\\begin{tabular}{lr|rrr|rrr|rrr}\n',nd) ;
fprintf(fd,'& & \\multicolumn{3}{c}{BBF} & \\multicolumn{3}{c}{ANN} & \\multicolumn{3}{c}{FLANN} \\\\\n') ;
fprintf(fd,'type & $d$ & bld & search & tot & bld & search & tot & bld & search & tot \\\\\\hline \n') ;
for it=1:nt,
  t=tv{it} ;
  for id=1:nd,
    d=dv(id) ;
    for in=nn:nn,
      n=nv(in) ;
      % the following line was to correct the omission in testdistr.m, 
      % to be removed later
      % srtm(id,in,it)=mean(srtmr(id,in,it,:)) ;
      %score=(annbldtm(id,in,it)+annsrtm(id,in,it)-(bldtm(id,in,it)+ ...
      %                                             srtm(id,in,it)))/ ...
      %      sqrt((var(squeeze(srtmr(id,in,it,:)))+var(squeeze(annsrtmr(id,in,it,:))))/2)*sqrt(nrep/2) ;
      
      anntot=annbldtm(id,in,it)+annsrtm(id,in,it) ;
      flanntot=flannbldtm(id,in,it)+flannsrtm(id,in,it) ;
      bbftot=bldtm(id,in,it)+srtm(id,in,it) ;
      
      if bbftot <= min(anntot,flanntot),
        fprintf(fd,['%s & %d & %0.1f & %0.1f & \\textbf{%0.1f} & %0.1f & ' ...
        '%0.1f & %0.1f & %0.1f & %0.1f & %0.1f \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bbftot,annbldtm(id,in,it),annsrtm(id,in,it),anntot,flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      elseif anntot <= min(bbftot,flanntot),
        fprintf(fd,['%s & %d &  %0.1f & %0.1f & %0.1f & %0.1f & %0.1f & ' ...
                    '\\textbf{%0.1f} & %0.1f & %0.1f & %0.1f \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bbftot,annbldtm(id,in,it),annsrtm(id,in,it),anntot,flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      else
        fprintf(fd,['%s & %d &  %0.1f & %0.1f & %0.1f & %0.1f & %0.1f & ' ...
                    '%0.1f & %0.1f & %0.1f & \\textbf{%0.1f} \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bbftot,annbldtm(id,in,it),annsrtm(id,in,it),anntot,flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      end ;
    end ;
  end ;
end ;
fprintf(fd,'\\end{tabular}\n') ;
fclose(fd) ;


system('cp -v distr5.tex ~/tex/annsearch/') ;