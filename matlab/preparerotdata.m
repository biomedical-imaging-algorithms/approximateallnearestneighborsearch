% Prepare data for the rotational experiment


% sizes of the neighborhoods
%ds=3 ;
% ds=[5] ;
%ds=[1 2 3 4 5 6 7 9] ;
%ds=[1 2 3 4 5 6] ;
ds=[1 3 ] ;
%ds=[1 3 5 7 9] ;
%ds=[5 7 9] ;
dn=length(ds) ;

% load image
a=imread('cameraman.tif') ;
% calculate edges
gh = imfilter(a,fspecial('sobel') /8,'replicate');
gv = imfilter(a,fspecial('sobel')'/8,'replicate');
g=-(double(gv).^2+double(gh).^2).^0.3 ;
angofs=5 ;
g=imrotate(g,5,'bilinear','crop') ;
figure(1) ; clf ;
imagesc(a) ; colormap(gray) ; colorbar
print('-depsc2','cameraman.eps') ;
figure(2) ; clf ;
imagesc(g) ;  colormap(gray) ; colorbar
print('-depsc2','cameramang.eps') ;

 
% rotation angles
%angs=[5] ; % degrees
%angs=-5:0.01:5 ;
angs=-5:0.1:5 ;
%angs=[0]
nang=length(angs) ;


for di=1:dn,
    d=ds(di) ;
    % calculate feature vectors
    c=size(a) ;
    cs=10 ; % FIXME, should not be a fixed number
    gc=g(1+cs:c(1)-cs,1+cs:c(2)-cs) ;
    fg=nbfeatures(gc,d) ;
    % save(sprintf('../tmp/imgnbg%d.txt',d),'fg','-ascii') ; 

    for j=1:nang, % for all rotations
        ang=angs(j) ;
        fprintf('d=%d Angle %f\n',d,ang) ;
        ar=imrotate(a,ang+angofs,'bilinear','crop') ;
        % cropped versions
        arc=ar(1+cs:c(1)-cs,1+cs:c(2)-cs) ;
        fa=nbfeatures(arc,d) ;
        fga=[fa'  fg' ]+random('uniform',-0.005,0.005,prod(size(arc)),2*d*d) ;
        % figure(2); clf ; 
        % subplot(121) ; imagesc(arc) ; colormap(gray) ; axis image
        % subplot(122) ; imagesc(gc) ; axis image
        save(sprintf('../tmp/imgnb%d-%.2f.txt',d,ang),'fga','-ascii') ; 
        % disp('Press any key') ;
        % pause ;
    end
end


