% Create a LaTeX table presenting the results of testdistr.m

%load testdistr5 % this was used in the first submitted version
load testdistr
fd=fopen('distr.tex','w') ;

% second table for fixed N and different 'd' and 'distribution
% load testdistr3

fprintf(fd,'\\begin{tabular}{lr|rrr|rrr|rrr}\n',nd) ;
fprintf(fd,'& & \\multicolumn{3}{c}{BBF} & \\multicolumn{3}{c}{ANN} & \\multicolumn{3}{c}{FLANN} \\\\\n') ;
fprintf(fd,'type & $d$ & bld & search & tot & bld & search & tot & bld & search & tot \\\\\\hline \n') ;
for it=1:nt,
  t=tv{it} ;
  for id=1:nd,
    d=dv(id) ;
    for in=nn:nn,
      n=nv(in) ;
      % the following line was to correct the omission in testdistr.m, 
      % to be removed later
      % srtm(id,in,it)=mean(srtmr(id,in,it,:)) ;
      %score=(annbldtm(id,in,it)+annsrtm(id,in,it)-(bldtm(id,in,it)+ ...
      %                                             srtm(id,in,it)))/ ...
      %      sqrt((var(squeeze(srtmr(id,in,it,:)))+var(squeeze(annsrtmr(id,in,it,:))))/2)*sqrt(nrep/2) ;
      
      anntot=annbldtm(id,in,it)+annsrtm(id,in,it) ;
      flanntot=flannbldtm(id,in,it)+flannsrtm(id,in,it) ;
      bbftot=bldtm(id,in,it)+srtm(id,in,it) ;
      
      if bbftot <= max(anntot,flanntot),
        fprintf(fd,['%s & %d & %0.2f & %0.2f & \\textbf{%0.2f} & %0.2f & ' ...
        '%0.2f & %0.2f & %0.2f & %0.2f & %0.2f \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it),flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      elseif anntot <= max(bbftot,flanntot),
        fprintf(fd,['%s & %d &  %0.2f & %0.2f & %0.2f & %0.2f & %0.2f & ' ...
                    '\\textbf{%0.2f} & %0.2f & %0.2f & %0.2f \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it),flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      else
        fprintf(fd,['%s & %d &  %0.2f & %0.2f & %0.2f & %0.2f & %0.2f & ' ...
                    '%0.2f & %0.2f & %0.2f & \\textbf{%0.2f} \\\\\n'],t,d,bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it),flannbldtm(id,in,it),flannsrtm(id,in,it),flanntot) ;
      end ;
    end ;
  end ;
end ;
fprintf(fd,'\\end{tabular}\n') ;
fclose(fd) ;


system('cp -v distr.tex ~/tex/annsearch/') ;