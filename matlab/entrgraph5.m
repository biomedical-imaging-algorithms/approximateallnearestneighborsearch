% Visualize the results of the testentrbbf4 + testentrann + testentrflann
% experiments

clear all 
close all

% load ANN results
load testentrannnorm
annenterr=enterr ;
anntottime=bldtm+srtm ;
anneps=mv ;

% load FLANN results
load testentrflannnorm
flannenterr=enterr ;
flanntottime=bldtm+srtm ;
flannmv=mv ;


clc='kbrcmy' ;

% show the results with L=30
%load testentrbbf1e5l30
% load ../tmp/testentrbbf2
% bbftottime=bldtm+srtm ;
% bbfenterr=enterr ;
% bbfmv=mv ;
load testentrbbf4
maxlen=0 ;
for i=1:length(srtm),
    if length(srtm{i})>maxlen, maxlen=length(srtm{i}) ; end ;
end ;

bbftotime=zeros(nd,maxlen) ;
bbfenterr=zeros(nd,maxlen) ;
for i=1:nd,
  bbftottime(i,:)=srtm{i}(end) ;
  bbftottime(i,1:length(srtm{i}))=srtm{i} ;
  bbfenterr(i,:)=enterr{i}(end) ;
  bbfenterr(i,1:length(enterr{i}))=enterr{i} ;
end ;



epsilon=1e-6 ;


figure ;  
% normalize errors
%for id=[1 2 3 5 6]
for id=1:nd,
  d=dv(id) ;
  trueentr=d*(0.5+0.5*log(2*pi)) ;
  bbfrelenterr(id,:)=bbfenterr(id,:)/trueentr ;
  annrelenterr(id,:)=annenterr(id,:)/trueentr ;
  flannrelenterr(id,:)=flannenterr(id,:)/trueentr ;
%  loglog(bbftottime(id,:),bbfrelenterr(id,:),[ 'o-' clc(id) ]) ; 
end ;
pbaspect([2 1 2]) ;  
h1=loglog(bbftottime',bbfrelenterr'+epsilon,'-') ; hold on ;
h2=loglog(anntottime',annrelenterr'+epsilon,'+--') ; 
h2a=loglog(flanntottime',flannrelenterr'+epsilon,'*:') ; hold off ;
h3=xlabel('Time [s]') ;
h4=ylabel('Relative MSE') ;
s={} ;
for id=1:nd,
  d=dv(id) ;
  s{id}=sprintf('BBF d=%d',d) ;
  s{id+nd}=sprintf('ANN d=%d',d) ;
  s{id+2*nd}=sprintf('FLANN d=%d',d) ;
%  loglog(anntottime(id,:),annrelenterr(id,:),[ 'o--' clc(id)] ) ;
end ;
  set(h1,'linewidth',1) ;
  set(h2,'linewidth',1) ;
  set(h2a,'linewidth',1) ;
  set(h1,'MarkerSize',10) ;
  set(h2,'MarkerSize',10) ;
  set(h2a,'MarkerSize',10) ;
  set(gca,'FontSize',12) ;
  set(h3,'FontSize',12) ;
  set(h4,'FontSize',12) ;
axis normal ;
% axis([0.1 100 1e-5 1]) ;
pbaspect([1.5 1 1]) ;
legend(s,'Location','EastOutside') ;
legend('boxoff') ;
print('-depsc2','entrgraphcomb5.eps') ;
%hold off 

disp('converting') ;
%system('for i in entrgraph*.eps ; do echo $i ; epstopdf $i ; done') ;
%system('cp -v entrgraph*.pdf ~/tex/annsearch/figs/') ;
