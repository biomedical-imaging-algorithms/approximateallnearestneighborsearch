function nndistr3() ;
% Plot the error versus dimension
  
ds=2:20 ;
nd=length(ds) ;
n=10000 ; % number of points
f=1/n ; % local density
vini=100 ;
b=5 ;
nmax=5 ;

ncs=zeros(1,nd) ;
ptrue=zeros(1,nd) ;
relerr=zeros(1,nd) ;
relerr2=zeros(1,nd) ;

for di=1:nd,
  d=ds(di) ;
  c=pi^(d/2)/gamma(d/2+1) ;
  alpha=f*c ; 
  rmax=alpha^(-1/d) ;
  nc=ceil((2/sqrt(pi)*gamma(d/2+1)^(1/d)/b^(1/d)+1)^d)*b ;
  ncs(di)=nc ;
  v=min(vini,nc-1) ;
  ptrue(di)=v/(nc-1) ;
  [acoefs,~,~]=approxfrpow(d,nmax) ; 
  i1=0 ;
  b1=beta(1-1/d,nc-v) ;
  for k=0:nmax,
    i1=i1+acoefs(k+1)/(k+v)*(b1-beta(1-1/d,nc+k)) ;
  end ;
  relerr(di)=v/nc+(nc-v)*v*(beta((nc-v),1-1/d)*beta(v,1/d+1)-i1)  ;
  relerr2(di)=exprelerrqv(d,v,nc,rmax,alpha)  ;
end 


figure(1) ; clf
%subplot(311) ;
%semilogy(ds,ncs) ; xlabel('d') ; ylabel('nc') ;
subplot(211) ;
plot(ds,ptrue) ; xlabel('d') ; ylabel('ptrue') ;
subplot(212) ;
plot(ds,relerr,ds,relerr2) ; % axis([0 30 0 20]) ; 
xlabel('d') ; ylabel('relerr') ;
keyboard
end

function q_=pdens(r,n,d,rmax,alpha) ;
q_=(r<rmax).*(alpha*d).*r.^(d-1).*n.*(1-alpha*r.^d).^(n-1) ;
end


function y_=relerruwq(uw,v,d,rmax,alpha) ;
  y_=uw*0 ;
  for ii=1:length(uw),
    y_(ii)=quadgk(@(rr) rr./min(rr,uw(ii)).* pdens(rr,v,d,rmax,alpha),0,rmax) ;
  end ;
end

function y=exprelerrqv(d,v,nc,rmax,alpha) ;

  y=quadl(@(uu) relerruwq(uu,v,d,rmax,alpha).* pdens(uu,nc-v,d,rmax,alpha),0,rmax) ;
end
