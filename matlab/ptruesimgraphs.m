function ptruesimgraphs()
% Analytically calculate predicted errors to generate the same graphs as
% in ptruegraphs.m (which come from experiments), for comparison

clear all
%n=1e6 ; % number of points - not needed
dv=[ 2 4 6 8 10 12 14 16 18 20 ] ;
mv = [ 5 10 15 20 25 30 40 60 80 100 120 140 160 180 200 240 280 320 360 400 500 1000 2000 5000 10000] ;
%mv = [ 5 10 15 20 25 30 40 60 80 100 ] ;
nd=length(dv) ; 
nm=length(mv) ;
ptrue=zeros(nd,nm) ;
relerr=zeros(nd,nm) ;
f=1/1e4 ; % local density
b=16 ;
nmax=5 ;

for id=1:nd ;
  d=dv(id) ; 
  c=pi^(d/2)/gamma(d/2+1) ;
  alpha=f*c ; 
  rmax=alpha^(-1/d) ;
  for im=1:nm,
    m=mv(im) ;
    nc=ceil((2/sqrt(pi)*gamma(d/2+1)^(1/d)/b^(1/d)+1)^d)*b ;
    v=min(nc-1,m) ;
    ptrue(id,im)=v/(nc-1) ;
    [acoefs,~,~]=approxfrpow(d,nmax) ; 
    i1=0 ;
    b1=beta(1-1/d,nc-v) ;
    for k=0:nmax,
      i1=i1+acoefs(k+1)/(k+v)*(b1-beta(1-1/d,nc+k)) ;
    end ;
    %relerr(id,im)=-1+v/nc+(nc-v)*v*(beta((nc-v),1-1/d)*beta(v,1/d+1)-i1)  ;
    relerr(id,im)=exprelerrqv(d,v,nc,rmax,alpha)-1  ;
  end 
end 


% what d do we want
indsd= [ find(dv==2) find(dv==4) find(dv==10) find(dv==20) ] ;
indsv=find(mv<=100) ;
indsv2=find(mv<=400) ;
%indsv=1:nm ;

% first the dependence of ptrue on V
figure(1) ; clf
%h1=plot(mv(indsv),ptrue(indsd,iandsv)) ;
h1=plot(mv(indsv2),ptrue(indsd,indsv2)) ;
axis([0 mv(indsv2(end)) -0.1 1]) ;
%h1=semilogx(mv,ptrue(indsd,:)) ;
h2=xlabel('V') ; 
h3=ylabel('Percentage of NN found') ;
s={} ;
for id=1:length(indsd),
  s{id}=sprintf('d=%d',dv(indsd(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruesimgraphpv.eps') ;

keyboard

% the dependence of relerr on V
figure(2) ; clf
h1=plot(mv(indsv),relerr(indsd,indsv)) ;
h2=xlabel('V') ; 
h3=ylabel('Relative error') ;
%axis([0 mv(indsv(end)) 0 1]) ;
s={} ;
for id=1:length(indsd),
  s{id}=sprintf('d=%d',dv(indsd(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruesimgraphrv.eps') ;

% now the dependencies on d 
% what V do we want
indsv= [ find(mv==20) find(mv==40) find(mv==100) find(dv==200) ] ;
% the dependence of ptrue on d
figure(3) ; clf
h1=plot(dv,ptrue(:,indsv)') ;
h2=xlabel('d') ; 
h3=ylabel('Percentage of NN found') ;
s={} ;
for id=1:length(indsv),
  s{id}=sprintf('V=%d',mv(indsv(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruesimgraphpd.eps') ;

% the dependence of relerr on d
figure(4) ; clf
h1=plot(dv,relerr(:,indsv)') ;
h2=xlabel('d') ; 
h3=ylabel('Relative error') ;
%axis([0 mv(end) 0 1]) ;
%axis([0 mv(end) 0 max(max(relerr(:,indsv)))]) ;
s={} ;
for id=1:length(indsv),
  s{id}=sprintf('V=%d',mv(indsv(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruesimgraphrd.eps') ;

end


function q_=pdens(r,n,d,rmax,alpha) ;
q_=(r<rmax).*(alpha*d).*r.^(d-1).*n.*(1-alpha*r.^d).^(n-1) ;
end


function y_=relerruwq(uw,v,d,rmax,alpha) ;
  y_=uw*0 ;
  for ii=1:length(uw),
    y_(ii)=quadgk(@(rr) rr./min(rr,uw(ii)).* pdens(rr,v,d,rmax,alpha),0,rmax) ;
  end ;
end

function y=exprelerrqv(d,v,nc,rmax,alpha) ;

  y=quadl(@(uu) relerruwq(uu,v,d,rmax,alpha).* pdens(uu,nc-v,d,rmax,alpha),0,rmax) ;
end

