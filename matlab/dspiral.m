function [x,y]=dspiral(n) ;
% discrete spiral coordinates with n turns


x=[0] ; y=[0] ;
l=1 ; % line segment length

for i=1:n,
  [x,y]=step(x,y,l,0,-1) ; % go up
  [x,y]=step(x,y,l,-1,0) ; % go left
  l=l+1 ;
  [x,y]=step(x,y,l,0,1) ; % go down
  [x,y]=step(x,y,l,1,0) ; % go right
  l=l+1 ;
end ;

function [x,y]=step(x,y,l,dx,dy) ;
  for j=1:l,
    x=[x x(end)+dx ] ;
    y=[y y(end)+dy ] ;
  end ;
  
    