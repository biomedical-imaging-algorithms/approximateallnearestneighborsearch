% test of the effect of the update operation

clear all
%n=20 ;
n=1e5 ;
%n=1e2 ;
d=5 ;
%n=2 ;
%n=10 ;
%d=1 ;
nrep=10 ;
%nrep=1 ;
deltav = [ 0 0.1 0.2 0.3 0.4 ] ;
%deltav = [0. ] ;
%deltav = [-1.] ;
%deltav = [ 0.01 ] ;
%sigmav = [ 1. ] ; 
sigmav = [ 0.001 0.01 0.1  ] ;
%sigmav=[ 0.];
%sigmav = [  0.1  ] ;
ndelta=length(deltav) ;
nsigma=length(sigmav) ;

bldtmr=zeros(ndelta,nsigma,nrep) ;
srtmr=zeros(ndelta,nsigma,nrep) ;
updtmr=zeros(ndelta,nsigma,nrep) ;
usrtmr=zeros(ndelta,nsigma,nrep) ; % search on the updated tree

bldtm=zeros(ndelta,nsigma) ;
srtm=zeros(ndelta,nsigma) ;
updtm=zeros(ndelta,nsigma) ;
usrtm=zeros(ndelta,nsigma) ; % search on the updated tree

if d<=5, % choose leaf size
   l=20 ;
else
    l=4 ;
end ;

% l=1 ;

for is=1:nsigma ;
  sigma=sigmav(is) ;
  for id=1:ndelta,
    delta=deltav(id) ;
    for ir=1:nrep,
      a=gendata('uniform',n,d) ;
      save('-ascii','nupddata.txt','a') ;
      ad=a+random('unif',-sigma,sigma,n,d) ;
      save('-ascii','nupddata2.txt','ad') ;
      disp(sprintf('Testupdate ir=%d delta=%g sigma=%g started\n',ir,delta,sigma)) 
      fd=fopen('update.cmd','w') ;
      fprintf(fd,'set_dim %d\n',d) ;
      fprintf(fd,'set_leaf_size %d\n',l) ;
      fprintf(fd,'set_delta %g\n',delta) ;
      fprintf(fd,'read_data_pts nupddata.txt\n') ;
      fprintf(fd,'vnucko_build_tree\n') ;
      fprintf(fd,'read_data_pts nupddata2.txt\n') ;
      fprintf(fd,'vnucko_update_tree\n') ;
      fprintf(fd,'vnucko_all_nn_search\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <update.cmd >update.out') ;
      r=load('update.out') ;
      updtmr(id,is,ir)=r(2) ;
      usrtmr(id,is,ir)=r(3) ;
      fd=fopen('update.cmd','w') ;
      fprintf(fd,'set_dim %d\n',d) ;
      fprintf(fd,'set_leaf_size %d\n',l) ;
      fprintf(fd,'set_delta %g\n',delta) ;
      fprintf(fd,'read_data_pts nupddata2.txt\n') ;
      fprintf(fd,'vnucko_build_tree\n') ;
      fprintf(fd,'vnucko_all_nn_search\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <update.cmd >update.out') ;
      r=load('update.out') ;
      bldtmr(id,is,ir)=r(1) ;
      srtmr(id,is,ir)=r(2) ;
    end ; % for ir
    bldtm(id,is)=mean(bldtmr(id,is,:)) ;
    srtm(id,is)=mean(srtmr(id,is,:)) ;
    updtm(id,is)=mean(updtmr(id,is,:)) ;
    usrtm(id,is)=mean(usrtmr(id,is,:)) ;
    disp(sprintf('Testupdate delta=%f sigma=%f bld=%d sr=%f upd= %f usr=%f\n',delta,sigma,bldtm(id,is),srtm(id,is),updtm(id,is),usrtm(id,is))) ; 
    save testupdate ;
  end ;
end ;

    