function [a,ytrue,yapprox]=approxfrpow(d,nmax) ;
% create a polynomial approximation of t^(1/d) on the interval [0,1] in
% terms of (1-t)^k minimizing least squares error
%  
% It returns the coefficients, the true and approximated values  
% 
% Jan Kybic, August 2011
  
n=1+nmax ; % this many unknowns 
npts=100*n ; % this many points
t=((1:npts)-1)/(npts-1) ;
f=zeros(npts,n) ;
ytrue=zeros(npts,1) ;
for j=1:n,
  f(:,j)=(1-t).^(j-1) ;
end ;
ytrue(:)=t(:).^(1/d) ;
a=f\ytrue ;
yapprox=f*a ;
%clf ; plot(t,ytrue,'r-',t,yapprox,'g-') ; 
%legend('true','approx') ;