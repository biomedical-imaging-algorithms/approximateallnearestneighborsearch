% show the distribution of aspect ratios for different d

%close all ; 
nrep=10000 ; % number of repetitions ;
for d=2,
  a=zeros(1,nrep) ;
  % figure ;
  for i=1:nrep,
    z=random('uniform',0,1,1,d) ;
    a(i)=min(z) ;
  end ;
  hist(a,floor(sqrt(nrep))) ;
  %title(sprintf('d=%d',d)) ;
  %y=prctile(a,[50 90 95 99]) ;
  %fprintf('d=%d prctiles=%s\n',d,num2str(y)) ;
end 
    
