function mres=evalrotdata(lmethods,angs,ds,dosave) ;
%function 
% Evaluate the rotation experiment

%lmethods={'bbf','brute'} ;
%lmethods={'bbf-upd'} ;
%lmethods={'bbf'} ;
%lmethods={'ann'} ;
%lmethods={'flann'} ;
%lmethods={'lsh'} ;
%lmethods={'brute'} ;

nmethods=length(lmethods) ;
% angs=[-5 -0.2 0 0.2 5] ;
%angs=[5] ;
%angs=-5:0.2:5 ;
nang=length(angs) ;

%ds=[ 3 ] ;
%ds=[1 3 5 7 9] ;
%ds=[5 7 9] ;
dn=length(ds) ;

if nargin<4,
  dosave=1 ;
end ;


% random number generator initialization
RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));


for di=1:dn,
    d=ds(di) ;
    for mi=1:nmethods,
        method=lmethods{mi} ;
            mres=zeros(2,nang) ;    
            disp(['Method ' method]) ;
            s=[] ; % state
            for j=1:nang, % for all rotations
                ang=angs(j) ;
                fprintf('d=%d Angle %f\n',d,ang) ;
                fname=sprintf('imgnb%d-%.2f.txt',d,ang) ;
                [h,t,s]=calcentr(fname,method,s,j==1,j==nang) ;
                mres(1,j)=h ; mres(2,j)=t ;
                fprintf('Angle %f entropy %f time %f\n',ang,h,t) ;
                %figure(1) ; clf
                %plot(angs(1:j),mres(1,1:j)) ; title([method 'entropy']) ;
            end ;
            if dosave,
              disp('Saving') ;
              save(['../tmp/evalrot-' num2str(d) '-' method ],'mres','angs') ;
            end
            tottime=sum(mres(2,:)) ;
            disp(['Method ' method ' tottime ' num2str(tottime) ]) ;
            figure(1) ; clf
            plot(angs,mres(1,:)) ; title([method 'entropy']) ;
    end
end        

