% test the accuracy of entropy estimation versus time
% for the BBF algorithm

clear all
%n=1e6 ;
%mv=[ 3 10 30 100 300 1000 3000 10000 1e5 1e6] ; % max number of visited points
%dv=[ 2 5 10 20 ] ; % dimensions to test
% n=1e5 ;
% mv=[ 3 5 10 30 100 1000 10000 ] ; % max number of visited points
% dv=[ 3 5 7 9 11 20 ] ; % dimensions to test
% nd=length(dv) ;
% nm=length(mv) ;
% nrep=100 ; 

%n=1e5 ;
%mv=[ 42 100 300 500 1000 3000 10000 ] ;
%dv=[ 2 4 6 ] ;
dv=[ 1 3 5 ] ;
nrep = 1 ;
nd=length(dv) ;

angl=0 ;


addpath('~/work/increst/matlab') ;

oldfolder=cd('~/work/increst/tmp') ;

srtm={} ;
enterr={} ;



for id=1:nd,
  d=dv(id) ; 
%  trueent=d*(0.5+0.5*log(2*pi))
% find true entropy
  fc=load(['evalrot-' num2str(d) '-brute']) ;
  ind=find(fc.angs==angl) ;
  trueent=fc.mres(1,ind) ;
  %  l=30 
  dim=2*d^2 ;
  ht=bbftrymany(d) ;
  [pt,perr]=boundbbftrymany(ht,trueent) ;
  srtm=[ srtm pt ] ;
  enterr=[ enterr perr ] ;
  save testentrbbf3
end ; % for id

cd(oldfolder) ;
