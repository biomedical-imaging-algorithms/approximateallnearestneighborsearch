function [pt,perr,pparams]=boundbbftrymany(ht,trueent)
% given the result of bbftrymany, find the Paretto optimal set of
% points and return them, ordered

err=abs(ht(1,:)-trueent) ;
tm=ht(2,:) ;
params=ht(3:end,:) ;


n=size(ht,2) ;

pt=[] ; perr=[] ; pparams=[] ;



for i=1:n,
  % is point ht(:,i) Paretto optimal?
  if sum(tm<tm(i) &  err<err(i))==0,
      pt=[ pt tm(i) ] ;
      perr= [ perr err(i) ] ;
      pparams= [ pparams params(:,i) ] ;
  end ;
end ;

[perr,ind]=sort(perr,'descend') ;
pt=pt(ind) ;
pparams=pparams(ind) ;


  
