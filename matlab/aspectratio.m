% show the distribution of aspect ratios for different d

%close all ; 
nrep=10000 ; % number of repetitions ;
expl=1 ; % expected edge length
for d=2:5,
  a=zeros(1,nrep) ;
  % figure ;
  for i=1:nrep,
    z=random('exp',expl,1,d) ;
    a(i)=max(z)/min(z) ;
  end ;
  %  hist(a,floor(sqrt(nrep))) ;
  %title(sprintf('d=%d',d)) ;
  y=prctile(a,[50 90 95 99]) ;
  fprintf('d=%d prctiles=%s\n',d,num2str(y)) ;
end 
    
