function f=nbfeatures(a,d) ;
% Calculate neighborhood features for a gray-scale image a and a neighborhood
% of size d (odd)
% 
% Jan Kybic, April 2011
    
sz=size(a) ;
npix=prod(sz) ;
%assert(mod(d,2)==1) ;
dh=ceil((d-1)/2) ;
% extend the image
a= [ repmat(a(:,1),1,dh) a repmat(a(:,end),1,dh) ] ;
a= [ repmat(a(1,:),dh,1) ; a ; repmat(a(end,:),dh,1) ] ;

% form an offset array
ofs=zeros(d*d,1) ;
for i=1:d,
    for j=1:d,
        ofs(j+(i-1)*d)=(j-dh-1)+size(a,1)*(i-dh-1) ;
    end ;
end ;

% calculate the features
f=zeros(d*d,npix) ;
for ix=1:sz(2),
    for iy=1:sz(1),
        i=iy+(ix-1)*sz(1) ;
        j=iy+dh+(ix+dh-1)*(sz(1)+2*dh) ;
        f(:,i)=a(j+ofs) ;
    end
end   
