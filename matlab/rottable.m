function rottable() ;
    

rotsubtable('rottable1i.tex',[1 2 3]) ;
rotsubtable('rottable2i.tex',[4 5 6]) ;

function rotsubtable(fname, ds) ;

%fd=fopen('rottable.tex','w') ;
fd=fopen(fname,'w') ;
%ds=[1 3 5 7 9] ;
%ds=[1 2 3 4 5 6 ] ;
%mtds={'bbf','bbf-upd','bbf-tbb','bbf-ann','flann','ann','lsh','brute'} ;
mtds={'bbfinf','bbf','bbf-tbb','bbf-ann','flann','ann','lsh','brute'} ;
nds=length(ds) ;
nmtds=length(mtds) ;

mns=zeros(nmtds,nds) ; % mean times
stds=zeros(nmtds,nds) ; % standard deviation
ordmin=zeros(nmtds,nds) ; % order
ordmax=zeros(nmtds,nds) ; % order

for i=1:nds,
  d=ds(i) ;
  for j=1:nmtds,
    mtd=mtds{j} ;
    % we show the results without incremental update
    if strcmp(mtd,'bbf'),
      mtd='bbf-upd' ;
    end ;
    fname=['../tmp/evalrot-' num2str(d) '-' mtd ] ;
    try
      fc=load(fname) ;
    catch exc,
      disp([ 'Could not read ' fname ]) ;
      fc=[] ;
      fc.mres=ones(2,nds)*9999 ;
    end
    mns(j,i)=mean(fc.mres(2,:)) ;
    stds(j,i)=std(fc.mres(2,:)) ;
  end ;
end ;
% calculate order
fmt='' ;
for i=1:nds,
  [~,ind]=sort(mns(:,i),'ascend') ;
  ord(ind,i)=1:nmtds ;
  [ordmin(:,i),ordmax(:,i)]=uncertorder(mns(:,i),stds(:,i),0.05) ;
  % fmt=[ fmt '|r@{}l@{}c' ] ;
  fmt=[ fmt '|r@{}l@{}' ] ;
end 
% generate table
fprintf(fd,'\\begin{tabular}{l%s}\n',fmt) ;
for i=1:nds,
  %  fprintf(fd,'& \\multicolumn{3}{c}{$h=%d$, $d=%d$} ',ds(i),2*(ds(i)^2)) ;
  fprintf(fd,'& \\multicolumn{2}{c}{$h=%d$, $d=%d$} ',ds(i),2*(ds(i)^2)) ;
end ;
fprintf(fd,'\\\\ \\hline\n') ;
for j=1:nmtds,
  fprintf(fd,'%s  ',mtds{j}) ;
  for i=1:nds,
    if ordmin(j,i)==ordmax(j,i),
      ordstring=sprintf('%d',ordmin(j,i)) ;
    else
      ordstring=sprintf('%d--%d',ordmin(j,i),ordmax(j,i)) ;
    end ;
    % if ordmin(j,i)==1,
    %   fprintf(fd,['& $\\mathbf{%.2f}$ & $\\mathbf{\\scriptstyle\\pm %.2f}$ & \\hspace{0.5em}\\textbf{\\textit{(%s)}}'], mns(j,i),stds(j,i),ordstring) ;
    % else
    %   fprintf(fd,['& $%.2f$ & ${\\scriptstyle\\pm %.2f}$ & \\hspace{0.5em}\' ...
    %             '\textit{(%s)}'], mns(j,i),stds(j,i),ordstring) ;
    % end
    %    if ordmin(j,i)==1,
    if ordmin(j,i)==1,
      fprintf(fd,['& $\\mathbf{%.2f}$ & $\\mathbf{\\scriptstyle\\pm %.3f}$ '], mns(j,i),stds(j,i)) ;
    else
      fprintf(fd,['& $%.2f$ & ${\\scriptstyle\\pm %.3f}$ '], mns(j,i),stds(j,i)) ;
    end
  
  end ;
  fprintf(fd,'\\\\\n') ;
end

fprintf(fd,'\\end{tabular}') ;

fclose(fd) ;