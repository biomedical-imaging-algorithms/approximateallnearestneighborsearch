function ht=bbftrymany(d,fname) ;
% Try many parameters for BBF

if nargin<2,
    fname=[] ;
end ;


lv=[10 15 30 50 100 150 200];
mv=[40 50 100 200 400 500];

nlv=length(lv) ;
nmv=length(mv) ;

ht=[] ;


for i=1:nlv,
    for j=1:nmv,
        [h,t]=bbfwithd(d,lv(i),mv(j),fname) ;
        ht=[ ht [ h ; t ] ] ;
    end ;
end ;

% save bbftrymany d ht





