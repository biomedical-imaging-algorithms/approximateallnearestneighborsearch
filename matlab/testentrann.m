% test the accuracy of entropy estimation versus time
% for the ANN algorithm

clear all
%n=1e6 ;
%n=1e5 ;
%mv=[ 100 10 1 0.1 0.01 0.001 ] ; % allowed relative error 
%%dv=[ 2 5 10 20 ] ; % dimensions to test
%dv=[ 3 5 7 9 11 20 ] ; % dimensions to test
n=1e5 ;
% mv=[ 10 3 1 0.3 0.1 0.03  ] ; % allowed relative error
mv=[ 100 30 10 3 1  ] ; % allowed relative error 
%dv=[ 2 5 10 20 ] ; % dimensions to test
%dv=[ 5 15 ] ; % dimensions to test
dv=[10 30 50] ;



nd=length(dv) ;
nm=length(mv) ;
%nrep=100 ; 
nrep=1 ;


bldtmr=zeros(nd,nm,nrep) ;
srtmr=zeros(nd,nm,nrep) ;
entr=zeros(nd,nm,nrep) ;
bldtm=zeros(nd,nm) ;
srtm=zeros(nd,nm) ;
enterr=zeros(nd,nm) ;

for id=1:nd,
  d=dv(id) ; 
  trueent=d*(0.5+0.5*log(2*pi))
  l=8 ; % bucket size for ANN
  for im=1:nm,
    m=mv(im) ;
    for ir=1:nrep,
      a=gendata('normal',n,d) ;
      save('-ascii','nentanndata.txt','a') ;
      disp(sprintf('Testentrann ir=%d d=%d m=%d started\n',ir,d,m)) 
      fd=fopen('entrann.cmd','w') ;
      fprintf(fd,'set_dim %d\n',d) ;
      %      fprintf(fd,'set_leaf_size %d\n',l) ;
      fprintf(fd,'set_ann_epsilon %d\n',m) ;
      fprintf(fd,'read_data_pts nentanndata.txt\n') ;
      % fprintf(fd,sprintf('read_data_pts normal10000d%d.txt\n',d)) ;
      fprintf(fd,'ann_build_tree\n') ;
      fprintf(fd,'ann_entropy\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; nice -19 ~/work/increst/bin/allnn_exp <entrann.cmd >entrann.out') ;
      r=load('entrann.out') ;
      bldtmr(id,im,ir)=r(1) ;
      entr(id,im,ir)=r(2) ;
      srtmr(id,im,ir)=r(3) ;
    end ; % end for ir
    bldtm(id,im)=mean(bldtmr(id,im,:)) ;
    srtm(id,im)=mean(srtmr(id,im,:)) ;
    enterr(id,im)=sqrt(mean((entr(id,im,:)-trueent).^2)) ;
    disp(sprintf('Testentrann d=%3d m=%d finished. Build %g;  Search %g Enterr %g\n',d,m,bldtm(id,im),srtm(id,im),enterr(id,im))) ;
    save testentrannnorm2
  end ; % for im
end ; % for id
