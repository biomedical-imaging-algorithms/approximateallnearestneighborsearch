function nndistr2() ;
% Plot relative error formulas 

d=2 ; % dimension
c=pi^(d/2)/gamma(d/2+1) ;
n=1000 ; % number of samples
f=1/n ; % local density
rmax=(f*c)^(-1/d) 
v=25 ; % number of points to examine
r=0:0.01:rmax ; % for these radii we shall evaluate

b=15 ; % number of points per cell
% number of points to be examined to find the NN (on the average)
nc=ceil((2/sqrt(pi)*gamma(d/2+1)^(1/d)/b^(1/d)+1)^d)*b 

alpha=f*c ; 

r=0:0.01:rmax ; % for these radii we shall evaluate
if 0,
figure(1) ;
pnc=pdens(r,nc) ;
pv=pdens(r,v) ;
plot(r,pnc,r,pv) ; legend('Nc','V') ; title('density') ;
end ;

if 0,
figure(2) ;
% plot weighted relative error for various uw

uws=[ 1 2 3 ];
relerrs=zeros(length(r),length(uws)) ;
for i=1:length(uws),
  relerrs(:,i)=r./min(r,uws(i)).*pdens(r,v) ;
end ;
plot(r,relerrs) ;
end ;
if 0,
figure(3) ;
uws=1e-3:0.1:rmax ;
relerruw=zeros(length(uws),1) ;
for i=1:length(uws),
  %relerruw(i)=quad(@(rr) rr./min(rr,uws(i)).* pdens(rr,v),0,rmax) ;
  relerruw(i)=relerruwq(uws(i),v) ;
end ;
plot(uws,relerruw) ; title('expected relative error') ; xlabel('uw') ;
end ;

if 0,
figure(4) ;
r=0:0.1:rmax ;
nr=length(r) ;
kmax=10 ;
epsw=zeros(nr,1) ;
epswq=zeros(nr,1) ;

for i=1:nr,
  %  epsw(i,1)=relerruwq2(r(i),v) ;
    epsw(i,1)=relerruwq3(r(i),v) ;
  epswq(i,1)=relerruwq(r(i),v) ;
end ;

clf ;
% the first element is Inf, so we skip it.
plot(r(2:end),epsw(2:end,1),'bx-',r(2:end),epswq(2:end),'mo-')
legend('expr','quad') ;
end ;


  
if 1,
figure(4) ;
% dependence on V
vs=[1:nc-1] ;
vn=length(vs) ;
merr=zeros(vn,1) ;
merr2=zeros(vn,1) ;
merr3=zeros(vn,1) ;
nmax=10 ;
[acoefs,~,~]=approxfrpow(d,nmax) ; 
for i=1:vn,
  %  merr(i)=exprelerrqv(vs(i))  ;
  merr2(i)=exprelerrf(vs(i),acoefs) ;
  merr3(i)=exprelerrqv3(vs(i)) ;
end ;
%plot(vs,merr,'bx-',vs,merr2,'ro-',vs,merr3,'g-') ; 
plot(vs,merr2,'ro-',vs,merr3,'g-') ; 
%plot(vs,merr2,'r-') ; 
%plot(vs,merr,'bx-',vs,merr3,'go-') ; 
sum((merr2-merr3).^2)/sum(merr3.^2)
title('expected relative error') ; xlabel('V') ;
%legend('quaddbl','fun','quadint') ;
legend('fun','quadint') ;
%legend('fun') ;
%legend('quaddbl','quadint') ;

end 

if 0,
  % dimensionality dependence
  dmax=20 ; nmax=5 ;
  derr=zeros(dmax,1) ;
  ds=1:dmax ; dn=length(ds) ;
  for di=1:dn,
    d=ds(di) ;
    rmax=(f*c)^(-1/d) 
    % number of points to be examined to find the NN (on the average)
    nc=ceil((2/sqrt(pi)*gamma(d/2+1)^(1/d)/b^(1/d)+1)^d)*b 
    [acoefs,~,~]=approxfrpow(d,nmax) ;
    derr(di)=exprelerrf(max(20,nc-1),acoefs) ;
  end ;
  plot(ds,derr) ; xlabel('d') ; ylabel('relerr') ;
end
  
keyboard

if 0,
  % test integral i0
figure(4) ;
vs=[1:nc-1] ;
vn=length(vs) ;
si=0*vs ;
sj=0*vs ;
nmax=10 ; 
[a,~,~]=approxfrpow(d,nmax) ;
for i=1:length(vs) ;
  v=vs(i);
  si(i)=quad(@(s)s.^(-1/d).*(1-s).^(nc-v-1).*betainc(s,1/d+1,v).*beta(1/d+1,v),0,1) ;
  i1=0 ;
  for k=0:nmax,
    i1=i1+a(k+1)/(k+v).*(beta(1-1/d,nc-v)-beta(1-1/d,nc+k)) ;
  end ;
  sj(i)=i1; 
end ;
plot(vs,si,vs,sj) ; legend('true','approx') ;
sqrt(sum((si-sj).^2)/sum(si.^2))
end
    
        
             

% test the series approximation of betainc
if 0,
s=0:0.01:1 ;
bi=betainc(s,1/d+1,v) ;
bf=0*bi ;
for i=1:length(s),
  sumbf=0 ;
    citatel=1 ;
  for nn=0:100,
    sumbf=sumbf+citatel/(factorial(nn)*(1/d+1+n))*s(i)^(1+1/d+nn) ;
    citatel=citatel*(nn-v) ;
  end ;
  bf(i)=sumbf ;
end ;
plot(s,bi,'go-',s,bf,'r-') ;
end ;
%keyboard 




% density of n points for distances r
function q_=pdens(r_,n_) ;
q_=(r_<rmax).*(alpha*d).*r_.^(d-1).*n_.*(1-alpha*r_.^d).^(n_-1) ;
end

function y_=relerruwq(uw_,v_) ;
  y_=uw_*0 ;
  for ii=1:length(uw_),
    y_(ii)=quadgk(@(rr) rr./min(rr,uw_(ii)).* pdens(rr,v_),0,rmax) ;
  end ;
end

function y_=relerruwq3(uw_,v_) ;
  y_=uw_*0 ;
  for ii=1:length(uw_),
    uw=uw_(ii) ;
    y_(ii)=1-(1-alpha*uw^d)^v_+v_*alpha*d/uw*alpha^(-1/d)*(alpha*d)^(-1)*beta(v_,1+1/d)*betainc(max(0,min(1,alpha*uw^d)),1+1/d,v_,'upper') ;
  end ;
end



function y_=exprelerrq() ;
  y_=quad(@(uu) relerruwq(uu).* pdens(uu,nc-v_),0,rmax) ;
end

function y_=exprelerrqv(v__) ;

  y_=(v__<nc).*quadl(@(uu) relerruwq(uu,v__).* pdens(uu,nc-v__),0,rmax)+(v__==nc)*1 ;
end

function y_=exprelerrqv2(v__) ;

  y_=(v__<nc).*quadl(@(uu) relerruwq2(uu,v__).* pdens(uu,nc-v__),0,rmax)+(v__==nc)*1 ;
end

function y_=exprelerrqv3(v__) ;

  y_=(v__<nc).*quadl(@(uu) relerruwq3(uu,v__).* pdens(uu,nc-v__),0,rmax)+(v__==nc)*1 ;
end



function y_=exprelerrf(v_,acoefs) ;
  sumk=0 ;
  nmax=size(acoefs,1)-1 ;
  %  for kk=0:min(v_-1,kmax),
  %  sumk=sumk+nchoosek(v_-1,kk)*(-1)^kk*alpha^(kk-kk/d)/(d+kk+1)*beta(nc-v_,2+kk/d) ;
  % end ;
% y_=1+(nc-v_)*(1/nc+v*beta(v,1/d+1)*beta(nc-v,1-1/d)-v*d*sumk) ; ;
%  y_=(v__<nc).*quadl(@(uu) relerruwq4(uu,v__).* pdens(uu,nc-v__),0,rmax)+(v__==nc)*1 ;
%  y_=1+(nc-v_)/nc+
%  y_=v_/nc+quadl(@(uu)  (v_.*alpha.*d./uu.*alpha.^(-1/d).*(alpha*d).^(-1).*beta(v_,1+1/d))  .*  pdens(uu,nc-v_),0,rmax)-quadl(@(uu)  (v_.*alpha.*d./uu.*alpha.^(-1/d).*(alpha*d).^(-1).*beta(v_,1+1/d).*betainc(alpha*uu.^d,1+1/d,v_))  .* pdens(uu,nc-v_),0,rmax) ; 
%    y_=v_/nc+(nc-v_)*v_*beta(v_,1/d+1)*beta((nc-v_),1-1/d)-quadl(@(uu)
%    (v_.*alpha.*d./uu.*alpha.^(-1/d).*(alpha*d).^(-1).*beta(v_,1+1/d).*betainc(alpha*uu.^d,1+1/d,v_))
%    .* pdens(uu,nc-v_),0,rmax) ; 
    %    y_=v_/nc+(nc-v_)*v_* beta(v_,1/d+1)*(beta((nc-v_),1-1/d)-beta(v_,1/d+1)*(beta((nc-v_),1-1/d)-beta(nc,1-1/d))) ;
  i1=0 ;
  b1=beta(1-1/d,nc-v_) ;
  for k=0:nmax,
    i1=i1+acoefs(k+1)/(k+v_)*(b1-beta(1-1/d,nc+k)) ;
  end ;
  %    y_=v_/nc+(nc-v_)*v_*beta(v_,1/d+1)*(beta((nc-v_),1-1/d)-i1) ;
  %      y_=v_/nc+(nc-v_)*v_*beta(v_,1/d+1)*beta((nc-v_),1-1/d)-quadl(@(s)  (v_.*alpha.*d.*s.^(-1/d).*(alpha*d).^(-1).*beta(v_,1+1/d).*betainc(s,1+1/d,v_))  .* pdens((s/alpha).^(1/d),nc-v_),0,1) ;
  %        y_=v_/nc+(nc-v_)*v_*beta(v_,1/d+1)*beta((nc-v_),1-1/d)-quadl(@(uu)(v_.*alpha.*d./uu.*alpha.^(-1/d).*(alpha*d).^(-1).*beta(v_,1+1/d).*betainc(alpha*uu.^d,1+1/d,v_)).* (alpha*d).*uu.^(d-1).*(nc-v_).*(1-alpha*uu.^d).^(nc-v_-1) ,0,rmax)  ;
  %          y_=v_/nc+(nc-v_)*v_*beta(v_,1/d+1)*(beta((nc-v_),1-1/d)-quadl(@(s)(s.^(-1/d).*betainc(s,1+1/d,v_)).* (1-s).^(nc-v_-1) ,0,1))  ;
  %          y_=v_/nc+(nc-v_)*v_*(beta((nc-v_),1-1/d)*beta(v_,1/d+1)-quadl(@(s)s.^(-1/d).*betainc(s,1+1/d,v_)*beta(v_,1/d+1).* (1-s).^(nc-v_-1) ,0,1))  ;
            y_=v_/nc+(nc-v_)*v_*(beta((nc-v_),1-1/d)*beta(v_,1/d+1)-i1)  ;
  


end 

end