% test of the dependence of the build and search times on a leaf size
calculate=1 ;
if calculate,

  n=100000 ;
%  n=1000 ;
  nrep=10 ;
%  nrep=1 ;
%  lv=[ 1 2 3 4 8 16 32 64 128 ]; % leaf sizes
  dv=[ 1 2 3 4 5 10 15 20  ] ; % dimensions
  lv=[ 1 2  4  6  8  10  15 20 25 30 ]; % leaf sizes
%  dv=[ 2   ] ; % dimensions


  nl=length(lv) ;
  nd=length(dv) ; 

  bldtm=zeros(nl,nd) ; % build times
  srtm=zeros(nl,nd) ; % search times
  bldtmstd=zeros(nl,nd) ; % build times standard deviation
  srtmstd=zeros(nl,nd) ; % search times standard deviation
  bldtmr=zeros(nl,nd,nrep) ; % build times
  srtmr=zeros(nl,nd,nrep) ; % search times

   for id=1:nd,
    d=dv(id) ;
    for il=1:nl,
      l=lv(il) ; 
      for ir=1:nrep,
        a=gendata('normal',n,d) ;
        save('-ascii','data.txt','a') ;
        disp(sprintf('Testleaf ir=%d d=%d l=%d started\n',ir,d,l)) ;
        fd=fopen('leaf.cmd','w') ;
        fprintf(fd,'set_dim %d\n',d) ;
        fprintf(fd,'set_leaf_size %d\n',l) ;
        fprintf(fd,'read_data_pts data.txt\n') ;
        fprintf(fd,'vnucko_build_tree\n') ;
        fprintf(fd,'vnucko_all_nn_search\n') ;
        fprintf(fd,'quit\n') ;
        fclose(fd) ;
        system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <leaf.cmd >leaf.out') ;
      r=load('leaf.out') ;
      bldtmr(il,id,ir)=r(1) ;
      srtmr(il,id,ir)=r(2) ;
      disp(sprintf('Testleaf ir=%d d=%3d l=%3d finished. Build %10g Search: %10g\n',ir,d,l,r(1),r(2))) ;
      end ;
      bldtm(il,id)=mean(bldtmr(il,id,:)) ;
      srtm(il,id)=mean(srtmr(il,id,:)) ;
      bldtmstd(il,id)=std(bldtmr(il,id,:)) ;
      srtmstd(il,id)=std(srtmr(il,id,:)) ;
      disp(sprintf('Testleaf d=%3d l=%3d finished. Build %10g+-%10g  Search: %10g+-%10g\n',d,l,bldtm(il,id),bldtmstd(il,id),srtm(il,id),srtmstd(il,id))) ;

      save testleafres bldtm srtm bldtmr srtmr bldtmstd srtmstd lv dv nl nd n
    end ;
  end ;


else
  
  load testleafres
  
end ;
    
% show ideal leaf sizes for search only    

[t,i]=min(srtm) ; t, lv(i)
 
% show ideal leaf sizes for build+search 

[t,i]=min(srtm+bldtm) ; t, lv(i)
 