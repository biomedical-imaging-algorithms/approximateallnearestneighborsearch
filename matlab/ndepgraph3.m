% Visualize the results of the testndep3 experiment with linf norm
% no FLANN

clear all 
close all
load('tmp/testndep')
fbrute=load('tmp/testndep3brute') ;
fbbf=load('tmp/testndep3bbf') ;
fann=load('tmp/testndep3ann') ;
fflann=load('tmp/testndep3flann') ;
fbrutei=load('tmp/testndep3infbrute') ;
fbbfi=load('tmp/testndep3infbbf') ;
fanni=load('tmp/testndep3infann') ;
%fflann=load('testndep3flann');

nv=fbbf.nv ;
dv=[ 5 15  ] ; % dimensions
nd=length(dv) ; 

epsilon=0 ;


bldtm=fbbf.bldtm+epsilon ;
srtm=fbbf.srtm ;
annbldtm=fann.bldtm+epsilon ;
annsrtm=fann.srtm ;
flannbldtm=fflann.bldtm ;
flannsrtm=fflann.srtm ;
brutesrtm=fbrute.srtm+epsilon ;

bldtmi=fbbfi.bldtm+epsilon ;
srtmi=fbbfi.srtm ;
annbldtmi=fanni.bldtm+epsilon ;
annsrtmi=fanni.srtm ;
brutesrtmi=fbrutei.srtm+epsilon ;



% visualize for each 'd' separately
for i=1:nd,
  d=dv(i) ;
  disp(['Dimension ' num2str(d)]) ;
  figure ;

  li=2 ;
  

  h=loglog(nv(li:end),bldtm(i,li:end)+srtm(i,li:end),'b-o',nv(li:end),annbldtm(i,li:end)+annsrtm(i,li:end),'g-o',nv(li:end),flannbldtm(i,li:end)+flannsrtm(i,li:end),'m-o',nv(li:end),brutesrtm(i,li:end),'r-o')  ; hold on ;
  h1=loglog(nv(li:end),bldtmi(i,li:end)+srtmi(i,li:end),'b--o',nv(li:end),annbldtmi(i,li:end)+annsrtmi(i,li:end),'g--o',nv(li:end),brutesrtmi(i,li:end),'r--o')  ; hold on ;
  
  set(h,'linewidth',2) ;
  % set(h1,'linewidth',2) ;
  set(gca,'FontSize',18) ;
  legend('BBF','ANN','FLANN','brute','BBFinf','ANNinf','bruteinf','Location','SouthEast') ;
  legend('boxoff') ;
  xlabel('number of points') ; ylabel('Time [s]') ;
  if d==15,
    axis([1e3 1e6 1e-2 1e5]) ;
  end       
  print('-depsc2',[ 'ndepgraphtotdinf' num2str(d) '.eps' ] ) ; 
  title(['Total times for d=' num2str(d)]) ;



end ;

% axis([1e3 1e6 1e-2 1e4])


disp('converting') ;
system('for i in ndepgraph*.eps ; do echo $i ; epstopdf $i ; done') ;
system('cp -v ndepgraph*.pdf ~/tex/annsearch/figs/') ;
