% Create a LaTeX table presenting the results of testdistr.m

load testdistr3
fd=fopen('distr.tex','w') ;

% first table for the normal distribution and different N and d

fprintf(fd,'\\begin{tabular}{ccc|ccc|ccc}\n',nd) ;
fprintf(fd,'& & & \\multicolumn{3}{c}{BBF} & \\multicolumn{3}{c}{ANN} \\\\\n') ;
fprintf(fd,'type & $d$ & $N$ & bld & search & tot & bld & search & tot \\\\\\hline \n') ;
for it=1,
  t=tv{it} ;
  for id=1:nd,
    d=dv(id) ;
    for in=1:nn,
      n=nv(in) ;
      % this is to correct the omission in testdistr.m, to be removed later
      srtm(id,in,it)=mean(srtmr(id,in,it,:)) ;
      if (bldtm(id,in,it)+srtm(id,in,it)<annbldtm(id,in,it)+annsrtm(id,in,it))
        fprintf(fd,'%s & %d & $10^{%d}$ & %0.3f & %0.3f & \\textbf{%0.3f} & %0.3f & %0.3f & %0.3f \\\\\n',t,d,log10(n),bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it)) ;
      else  
        fprintf(fd,'%s & %d & $10^{%d}$ & %0.3f & %0.3f & %0.3f & %0.3f & %0.3f & \\textbf{%0.3f} \\\\\n',t,d,log10(n),bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it)) ;
      end ;
    end ;
  end ;
end ;
fprintf(fd,'\\end{tabular}\n\n') ;

% second table for fixed N and different 'd' and 'distribution
% load testdistr3

fprintf(fd,'\\begin{tabular}{ccc|ccc|ccc}\n',nd) ;
fprintf(fd,'& & & \\multicolumn{3}{c}{BBF} & \\multicolumn{3}{c}{ANN} \\\\\n') ;
fprintf(fd,'type & $d$ & $N$ & bld & search & tot & bld & search & tot \\\\\\hline \n') ;
for it=1:nt,
  t=tv{it} ;
  for id=1:nd,
    d=dv(id) ;
    for in=nn:nn,
      n=nv(in) ;
      % this is to correct the omission in testdistr.m, to be removed later
      srtm(id,in,it)=mean(srtmr(id,in,it,:)) ;
      if (bldtm(id,in,it)+srtm(id,in,it)<annbldtm(id,in,it)+annsrtm(id,in,it))
        fprintf(fd,'%s & %d & $10^{%d}$ & %0.3f & %0.3f & \\textbf{%0.3f} & %0.3f & %0.3f & %0.3f \\\\\n',t,d,log10(n),bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it)) ;
      else  
        fprintf(fd,'%s & %d & $10^{%d}$ & %0.3f & %0.3f & %0.3f & %0.3f & %0.3f & \\textbf{%0.3f} \\\\\n',t,d,log10(n),bldtm(id,in,it),srtm(id,in,it),bldtm(id,in,it)+srtm(id,in,it),annbldtm(id,in,it),annsrtm(id,in,it),annbldtm(id,in,it)+annsrtm(id,in,it)) ;
      end ;
    end ;
  end ;
end ;
fprintf(fd,'\\end{tabular}\n') ;
fclose(fd) ;


