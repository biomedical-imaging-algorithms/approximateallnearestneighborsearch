function [ordmin,ordmax]=uncertorder(x,s,p) ;
% Given means and standard deviations, give possible orders
% at p=5% significance level. Returns maximum and minimum 
% orders
% 
% Jan Kybic, April 2011
  
if nargin<3,
  p=0.05 ;
end

% what probability is needed for one measurement excess, so that the two of them
% simultaneously occur with probability at most p.
q=1-sqrt(1-p) ;
% what multiple of the standar deviation does it correspond to.
k=icdf('normal',q,0,1) ;

% lower and upper bounds
lb=x-s*k ;
ub=x+s*k ;

n=length(x) ;
assert(length(s)==n) ;
ordmin=zeros(n,1) ;
for i=1:n,
  % minimum and maximum rank
  ordmax(i)=sum(ub<=lb(i)) ;
  ordmin(i)=sum(lb<=ub(i))+1 ;
end ;
  