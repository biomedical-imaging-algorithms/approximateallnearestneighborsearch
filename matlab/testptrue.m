% test the percentage of correctly identified points and relative error
% for the BBF algorithm

clear all

% first, test the dependence on V
n=1e4 ; % number of points
dv=[ 2 4 6 8 10 12 14 16 18 20 ] ;
mv = [ 5 10 15 20 25 30 40 60 80 100 120 140 160 180 200 240 280 320 360 400] ;
nm=length(mv) ;
nd=length(dv) ; 
nrep=100 ;

ptruer=zeros(nd,nm,nrep) ;
relerrr=zeros(nd,nm,nrep) ;
ptrue=zeros(nd,nm) ;
relerr=zeros(nd,nm) ;

for id=1:nd ;
  d=dv(id) ; 
  for im=1:nm,
    m=mv(im) ;
    for ir=1:nrep,
      a=gendata('uniform',n,d) ;
      save('-ascii','testptrue.txt','a') ;
      disp(sprintf('Testptrue ir=%d d=%d m=%d started\n',ir,d,m)) 
      fd=fopen('testptrue.cmd','w') ;
      fprintf(fd,'set_dim %d\n',d) ;
      fprintf(fd,'read_data_pts testptrue.txt\n') ;
      fprintf(fd,'set_max_visited %d\n',m) ;
      fprintf(fd,'vnucko_build_tree\n') ;
      fprintf(fd,'vnucko_ptrue\n') ;
      fprintf(fd,'vnucko_relerr\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <testptrue.cmd >testptrue.out') ;
      r=load('testptrue.out') ;
      ptruer(id,im,ir)=r(2) ;
      relerrr(id,im,ir)=r(3) ;
    end ; % end for ir
    ptrue(id,im)=mean(ptruer(id,im,:)) ;
    relerr(id,im)=mean(relerrr(id,im,:)) ;
    disp(sprintf('Testptrue d=%3d m=%d finished. ptrue=%g relerr=%g\n',d,m,ptrue(id,im),relerr(id,im))) ;
    save testptrue
  end ; % for im
end ; % for id
