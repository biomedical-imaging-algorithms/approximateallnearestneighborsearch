expl=1 ; % expected edge length
lambda=1/expl ;
d=2 ;
t=0:0.1:30 ;
figure(1) ;
y1=(1-exp(-lambda*t)).^d ;
y2=(1-exp(-lambda*d*t)) ;
y=(1-exp(-lambda*t)) ;
plot(t,y,'g-',t,y1,'r-',t,y2,'b-') ;
p1=d*lambda*(1-exp(-lambda*t)).^(d-1).*exp(-lambda*t) ;
p2=d*lambda*exp(-lambda*d*t) ;
p=lambda*exp(-lambda*t) ;
figure(2) ;
plot(t,p,'g-',t,p1,'r-',t,p2,'b-') ;

