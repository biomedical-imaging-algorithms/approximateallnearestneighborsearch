set_dim 5
set_leaf_size 70
read_data_pts ndata.txt
set_flann_checks 1000
set_flann_trees 1
flann_build_tree
flann_entropy
quit
