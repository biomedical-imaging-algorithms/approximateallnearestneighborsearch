function a=gendata(type,n,d) ;
% Generate n random points of a chosen type and dimensionality d.
%
% Each returned column corresponds to one point.
% Types can be 'normal','uniform','corrnormal','corruniform','clustered',
% 'subspace','imgneighb'. If the requested dimensionality is not supported,
% a NaN is reported.
  
switch type
 case 'normal'
  % normal distribution, mean 0, standard deviation 1, uncorrelated
  a=random('Normal',0,1,n,d)  ;
  
 case 'uniform'
  % uniform distribution between 0 and 1, uncorrelated
  a=random('Unif',0,1,n,d) ;
  
 case 'corrnormal'
  % as in "Arya et al. : An Optimal Algorithm for Approximate Nearest
  % Neighbor Searching", we make a correlated datasets using
  % a(n)=0.9*a(n-1)+b(n)     where 'b' is the source distribution, here normal
  b=random('Normal',0,1,n,d)  ;
  a=zeros(n,d) ;
  a(1,:)=b(1,:) ;
  for i=2:n,
    a(i,:)=0.9*a(i-1,:)+0.1*b(i,:) ;
  end ;
  
 case 'corruniform'
  % similar to corrnormal but with uniform source distribution
  b=random('Unif',0,1,n,d)  ;
  a=zeros(n,d) ;
  a(1,:)=b(1,:) ;
  for i=2:n,
    a(i,:)=0.9*a(i-1,:)+0.1*b(i,:) ;
  end ;

  
 case 'clustered'
  % choose m=10 centers uniformly and perturby slightly using normal distribution
  m=10 ; 
  c=random('Unif',0,1,m,d) ; % cluster centers
  ind=floor(random('unif',0,m,1,n)) ; % assignment to clusters
  b=random('Normal',0,0.05,n,d) ; % perturbation
  a=c(ind+1,:)+b ;
  
 case 'subspace'
  % a randomly generated linear subspace of dimension 1 plus normal
  % random perturbation
  v=random('Normal',0,1,1,d) ;
  v=v/norm(v) ;
  b=random('Normal',0,0.05,n,d) ; % perturbation
  a=random('unif',0,1,n,1)*v + b ;
  
  
 case 'imgneighb' 
  % neighborhoods from the Lena picture, 512x512
  %lena=imread('/home/kybic/data/images/lena.png') ;
  lena=imread('lena.png') ;
  lena=lena(:,:,2) ; % green channel
  dsturns=5 ;
  [sx,sy]=dspiral(dsturns) ; % discrete spiral with 10 turns
  if d>length(sx),
    error('Increase discrete spiral length.') ;
  end ;
  [ny,nx]=size(lena) ;
  ix=floor(random('unif',0,nx-2*dsturns,n,1))+dsturns+1 ;
  iy=floor(random('unif',0,ny-2*dsturns,n,1))+dsturns+1 ;
  a=zeros(n,d) ;
  for i=1:n,
    for j=1:d,
      a(i,j)=lena(iy(i)+sy(j),ix(i)+sx(j)) ;
    end ;
  end ;
  sgm=0.01 ;
  a=a+random('unif',-sgm,sgm,n,d)   ;
  
 otherwise
     error('Unknown type in gendata.')
end 
