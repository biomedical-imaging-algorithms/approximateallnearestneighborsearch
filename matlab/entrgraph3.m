% Visualize the results of the testentrbbf,testentrann, testentrflann
% experiments

clear all 
close all

% load ANN results
load testentrannnorm2
annenterr=enterr ;
anntottime=bldtm+srtm ;
anneps=mv ;

% load FLANN results
load testentrflannnorm2
flannenterr=enterr ;
flanntottime=bldtm+srtm ;
flannmv=mv ;


%clc='kbrcmy' ;
lnt={'-','--','-.'} ;


% show the results with L=30
%load testentrbbf1e5l30
load testentrbbfnorm2
bbftottime=bldtm+srtm ;
bbfenterr=enterr ;
bbfmv=mv ;




figure ;  
% normalize errors
%for id=[2 3 ]
for id=1:nd,
  d=dv(id) ; 
  trueentr=d*(0.5+0.5*log(2*pi)) ;
  bbfrelenterr(id,:)=bbfenterr(id,:)/trueentr ;
  annrelenterr(id,:)=annenterr(id,:)/trueentr ;
  flannrelenterr(id,:)=flannenterr(id,:)/trueentr ;
%  loglog(bbftottime(id,:),bbfrelenterr(id,:),[ 'o-' clc(id) ]) ; 
end ;
clf ; 
for id=1:nd,
    h1=loglog(bbftottime(id,:),bbfrelenterr(id,:),[ 'b' lnt{id} ]) ; hold on ; 
    h2=loglog(anntottime(id,:),annrelenterr(id,:),[ 'go' lnt{id} ]) ; 
    h2a=loglog(flanntottime(id,:),flannrelenterr(id,:),[ 'r+' lnt{id}] ) ; 
  set(h1,'linewidth',2) ;
  set(h2,'linewidth',2) ;
  set(h2a,'linewidth',2) ;
  set(h1,'MarkerSize',5) ;
  set(h2,'MarkerSize',5) ;
end
hold off ;
%h2=loglog(anntottime',annrelenterr','g') ; 
%h2a=loglog(flanntottime',flannrelenterr','m') ; hold off ;
h3=xlabel('Time [s]') ;
h4=ylabel('Relative MSE') ;
set(h3,'FontSize',12) ;
set(h4,'FontSize',12) ;
s={} ;
for id=1:nd,
  d=dv(id) ;
  s{(id-1)*3+1}=sprintf('BBF d=%d',d) ;
  s{(id-1)*3+2}=sprintf('ANN d=%d',d) ;
  s{(id-1)*3+3}=sprintf('FLANN d=%d',d) ;
%  loglog(anntottime(id,:),annrelenterr(id,:),[ 'o--' clc(id)] ) ;
end ;
%  set(h2a,'MarkerSize',5) ;
  set(gca,'FontSize',12) ;
axis normal ;
pbaspect([1.5 1 1]) ;
axis([0 1e3 1e-2 0.2]) ;
legend(s,'Location','EastOutside') ;
legend('boxoff') ;
print('-depsc2','entrgraphcomb.eps') ;
%hold off 

disp('converting') ;
system('for i in entrgraph*.eps ; do echo $i ; epstopdf $i ; done') ;
system('cp -v entrgraph*.pdf ~/tex/annsearch/figs/') ;
