% test the accuracy of entropy estimation versus time
% for the BBF algorithm

clear all
%n=1e6 ;
%mv=[ 3 10 30 100 300 1000 3000 10000 1e5 1e6] ; % max number of visited points
%dv=[ 2 5 10 20 ] ; % dimensions to test
% n=1e5 ;
% mv=[ 3 5 10 30 100 1000 10000 ] ; % max number of visited points
% dv=[ 3 5 7 9 11 20 ] ; % dimensions to test
% nd=length(dv) ;
% nm=length(mv) ;
% nrep=100 ; 

n=1e5 ;
mv=[ 10 15 30 50 100 300 1000 3000 ] ;
%dv=[ 5 15 ] ;
dv=[ 10 30 50 ] ;
nrep = 1 ;
nd=length(dv) ;
nm=length(mv) ;


bldtmr=zeros(nd,nm,nrep) ;
srtmr=zeros(nd,nm,nrep) ;
entr=zeros(nd,nm,nrep) ;
bldtm=zeros(nd,nm) ;
srtm=zeros(nd,nm) ;
enterr=zeros(nd,nm) ;

for id=1:nd,
  d=dv(id) ; 
  trueent=d*(0.5+0.5*log(2*pi))
  %  l=30 
 if d<=2, % choose leaf size
   l=15 ;
elseif d<=10
    l=20 ;
    
 elseif d<=30
   l=50 ;
 else 
   l=100 ;
 end ;
 %  if d<=4, % choose leaf size
%   l=20 ;
%  elseif d>=10
%    l=4 ;
%  else
%    l=round(16-6/5*d)
%  end ;
  for im=1:nm,
    m=mv(im) ;
    for ir=1:nrep,
      a=gendata('normal',n,d) ;
      % a=gendata('imgneighb',n,d) ;
      save('-ascii','nentdatabbf.txt','a') ;
      disp(sprintf('Testentrbbf ir=%d d=%d m=%d started\n',ir,d,m)) 
      fd=fopen('entrbbf.cmd','w') ;
      fprintf(fd,'set_dim %d\n',d) ;
      fprintf(fd,'set_leaf_size %d\n',l) ;
      fprintf(fd,'read_data_pts nentdatabbf.txt\n') ;
      fprintf(fd,'set_max_visited %d\n',m) ;
      fprintf(fd,'vnucko_build_tree\n') ;
      fprintf(fd,'vnucko_entropy\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <entrbbf.cmd >entrbbf.out') ;
      r=load('entrbbf.out') ;
      bldtmr(id,im,ir)=r(1) ;
      entr(id,im,ir)=r(2) ;
      srtmr(id,im,ir)=r(3) ;
    end ; % end for ir
    bldtm(id,im)=mean(bldtmr(id,im,:)) ;
    srtm(id,im)=mean(srtmr(id,im,:)) ;
    enterr(id,im)=sqrt(mean((entr(id,im,:)-trueent).^2)) ;
    disp(sprintf('Testentrbbf d=%3d m=%d finished. Build %g;  Search %g Enterr %g\n',d,m,bldtm(id,im),srtm(id,im),enterr(id,im))) ;
    save testentrbbfnorm2
  end ; % for im
end ; % for id
