% Visualize the results of testptrue

clear all
%close all

% load results
%load testptrue2
load testptrue2
relerr(5,4)=0.2537 ; % fixing a measurement error, redo experiments
%relerr=relerr-1 ;

% what d do we want
indsd= [ find(dv==2) find(dv==4) find(dv==10) find(dv==20) ] ;
indsv=find(mv<=100) ;

% first the dependence of ptrue on V
figure(1) ; clf
h1=plot(mv(indsv),ptrue(indsd,indsv)) ;
h2=xlabel('V') ; 
h3=ylabel('Percentage of NN found') ;
s={} ;
for id=1:length(indsd),
  s{id}=sprintf('d=%d',dv(indsd(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','SouthEast') ;
print('-depsc2','ptruegraphpv.eps') ;

% the dependence of relerr on V
figure(2) ; clf
h1=plot(mv(indsv),relerr(indsd,indsv)) ;
h2=xlabel('V') ; 
h3=ylabel('Relative error') ;
axis([0 mv(indsv(end)) 0 1]) ;
s={} ;
for id=1:length(indsd),
  s{id}=sprintf('d=%d',dv(indsd(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruegraphrv.eps') ;

% now the dependencies on d 
% what V do we want
indsv= [ find(mv==20) find(mv==40) find(mv==100) ] ;
% the dependence of ptrue on d
figure(3) ; clf
h1=plot(dv,ptrue(:,indsv)') ;
h2=xlabel('d') ; 
h3=ylabel('Percentage of NN found') ;
s={} ;
for id=1:length(indsv),
  s{id}=sprintf('V=%d',mv(indsv(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','SouthWest') ;
print('-depsc2','ptruegraphpd.eps') ;

% the dependence of relerr on d
figure(4) ; clf
h1=plot(dv,relerr(:,indsv)') ;
h2=xlabel('d') ; 
h3=ylabel('Relative error') ;
%axis([0 mv(end) 0 1]) ;
s={} ;
for id=1:length(indsv),
  s{id}=sprintf('V=%d',mv(indsv(id))) ;
end ;
set(h1,'linewidth',2) ;
set(gca,'FontSize',12) ;
set(h2,'FontSize',12) ;
set(h3,'FontSize',12) ;
legend(s,'Location','NorthEast') ;
print('-depsc2','ptruegraphrd.eps') ;

