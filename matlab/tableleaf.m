function tableleaf() ;
% Take the results calculated by testleaf.m and
% create a LaTeX table source leaf.tex

load testleafres
fd=fopen('leaf.tex','w') ;
nl=length(lv) ;
nd=length(dv) ; 


fprintf(fd,'\\begin{tabular}{c|*{%d}{c|}}\n',nd) ;
fprintf(fd,'$L/d$  ') ;
for d=dv,
  fprintf(fd,'& %d',d) ;
end ;
fprintf(fd,'\\\\ \\hline \n') ;
for il=1:nl,
  l=lv(il) ;
  for id=1:nd,
    d=dv(id) ;
%    if bldtm(il,id)==min(bldtm(:,id)),
%      fprintf(fd,'& $\\mathbf{%8.3f\\pm %8.3f}$ ',bldtm(il,id),bldtmstd(il,id)) ;
%      fprintf(fd,'& $\\mathbf{%8.3f}$ ',bldtm(il,id)) ;
%    else,
%      fprintf(fd,'& $%8.3f\\pm %8.3f$ ',bldtm(il,id),bldtmstd(il,id)) ;
      fprintf(fd,'& $%8.3f$ ',bldtm(il,id)) ;
%    end ;
  end ;
  fprintf(fd,'\\\\\n') ;
  fprintf(fd,'%d ',l) ;
  for id=1:nd,
    d=dv(id) ;
%    if srtm(il,id)==min(srtm(:,id)),
%%      fprintf(fd,'& $\\mathbf{%8.3f\\pm %8.3f}$ ',srtm(il,id),srtmstd(il,id)) ;
%      fprintf(fd,'& $\\mathbf{%8.3f}$ ',srtm(il,id)) ;
%    else
%      fprintf(fd,'& $%8.3f\\pm %8.3f$ ',srtm(il,id),srtmstd(il,id)) ;
      fprintf(fd,'& $%8.3f$ ',srtm(il,id)) ;
%    end ;
  end ;
  fprintf(fd,'\\\\\n') ;
  for id=1:nd,
    d=dv(id) ;
    stdtot=std(bldtmr(il,id,:)+srtmr(il,id,:)) ;
    if srtm(il,id)+bldtm(il,id)==min(srtm(:,id)+bldtm(:,id)),
%      fprintf(fd,'& $\\mathbf{%8.3f\\pm %8.3f}$ ',srtm(il,id)+bldtm(il,id),stdtot) ;
      fprintf(fd,'& $\\mathbf{%8.3f}$ ',srtm(il,id)+bldtm(il,id)) ;
    else
%      fprintf(fd,'& $%8.3f\\pm %8.3f$ ',srtm(il,id)+bldtm(il,id),stdtot) ;
      fprintf(fd,'& $%8.3f$ ',srtm(il,id)+bldtm(il,id)) ;
    end ;
  end ;
  fprintf(fd,'\\\\ \\hline\n') ;
end ;
fprintf(fd,'\\end{tabular}\n') ;



fclose(fd) ;














function r=strrep(s,n) ;
% Concatenate n-times the string s
  r=''
  for i=1:n,
    r=r+s ;
  end ;
  
    