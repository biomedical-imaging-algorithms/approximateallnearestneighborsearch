% Create a LaTeX table presenting the result of testupdate.m

%load testupdate6
%load testupdate7  % this is the version in the article
load testupdate

fd=fopen('update.tex','w') ;

fprintf(fd,'\\begin{tabular}{cc|cccc|cc}\n') ;
fprintf(fd,' $\\sigma$ & $\\delta$ & $T_b$ & $T_s$ & $T_u$ & $T_{s''}$ & $T_{b+s}$ & $T_{u+s''}$ \\\\ \\hline\n') ;

for is=1:nsigma ;
  sigma=sigmav(is) ;
  for id=1:ndelta,
    delta=deltav(id) ;
    % bldtm(id,is)=trimmean(bldtmr(id,is,:),50) ;
    %srtm(id,is)=trimmean(srtmr(id,is,:),50) ;
    %updtm(id,is)=trimmean(updtmr(id,is,:),50) ;
    %usrtm(id,is)=trimmean(usrtmr(id,is,:),50) ;
    
    
     t1=bldtm(id,is)+srtm(id,is) ;
     t2=updtm(id,is)+usrtm(id,is) ;
%       fprintf(fd,'%0.2f & %0.1f & %0.2f & %0.2f & %0.2f & %0.2f & %0.2f & %0.2f \\\\\n',sigma, delta,bldtm(id,is),srtm(id,is),updtm(id,is),usrtm(id,is),t1,t2) ;
     if bldtm(id,is)<updtm(id,is),
         s1=sprintf('\\textbf{%0.2f}',bldtm(id,is)) ;
         s2=sprintf('%0.2f',updtm(id,is)) ;
     else
         s1=sprintf('%0.2f',bldtm(id,is)) ;
         s2=sprintf('\\textbf{%0.2f}',updtm(id,is)) ;
     end ;
     

      if t1<t2,
       fprintf(fd,...
   '%0.3f & %0.1f & %s & %0.2f & %s & %0.2f & \\textbf{%0.2f} & %0.2f \\\\\n',...
               sigma, delta,s1,srtm(id,is),s2,usrtm(id,is),t1,t2) ;
     else
       fprintf(fd,...
     '%0.3f & %0.1f & %s & %0.2f & %s & %0.2f & %0.2f & \\textbf{%0.2f} \\\\\n',sigma, delta,s1,srtm(id,is),s2,usrtm(id,is),t1,t2) ;
    end ;
  end ;
end ;
fprintf(fd,'\\end{tabular}\n') ;
fclose(fd) ;

system('cp -v update.tex ~/tex/annsearch') ;