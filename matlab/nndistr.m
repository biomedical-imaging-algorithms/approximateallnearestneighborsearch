function nndistr() ;
% Plot formulas for NN distances cdf and pdf

d=2 ; % dimension
c=pi^(d/2)/gamma(d/2+1) ;
n=1000 ; % number of samples
f=1/n ; % local density
rmax=(f*c)^(-1/d) 
v=100 ; % number of points to examine
r=0:0.01:rmax ; % for these radii we shall evaluate

b=15 ;
nc=ceil((2/sqrt(pi)*gamma(d/2+1)^(1/d)/b^(1/d)+1)^d)*b 

% exponential cdf
Qe=exp(-f*c*n*r.^d) ;

% polynomial cdf
Qp=(r<rmax).*(1-f*c*r.^d).^n ;

if 0,
% plot Qe,Qp
figure(1) ; clf ;
plot(r,Qe,'b-',r,Qp,'g-') ; legend('Qe','Qp') ;
figure(2) ; clf ;
plot(r,Qe-Qp) ; title('Qe-Qp') ;
end ;

% now the densities
if 1,
qe=f*c*n*d*r.^(d-1) .* exp(-f*c*n*r.^d) ;
qp=(r<rmax).*(f*c*d).*r.^(d-1).*n.*(1-f*c*r.^d).^(n-1) ;
figure(1) ; clf ;
plot(r,qe,'b-',r,qp,'g-') ; legend('qe','qp') ;
figure(2) ; clf ;
plot(r,qe-qp) ; title('qe-qp') ;
end ;

% integrand
if 0,
r=0:0.01:rmax/5 ; % for these radii we shall evaluate
qp=r.^d.*(1-f*c*r.^d).^(v-1) ;
qa=r.^d.*(1-(v-1)*f*c*r.^d+(v-1)*(v-2)/2*(f*c*r.^d).^2-(v-1)*(v-2)*(v-3)/6*(f*c*r.^d).^3)
%;
%qa=r.^d.*(1-(v-1)*f*c*r.^d+(v-1)*(v-2)/2*(f*c*r.^d).^2) ;
figure(1) ; clf ; 
plot(r,qp,'g-',r,qa,'b-') ;  
title('integrand') ; legend('poly','approx') ;
end ;

%keyboard

if 1,
% now the expected relative error
r=0:0.1:rmax/5 ; % for these radii we shall evaluate
nr=length(r) ;
ks=[0 1 2 3 ] ;
nk=length(ks) ;
epsw=zeros(nr,nk) ;
epswq=zeros(nr,1) ;

for k=1:nk,
  for i=1:nr,
    epsw(i,k)=expepsw(r(i),ks(k)) ;
  end ;
end ;
for i=1:nr,
    epswq(i)=expepswq(r(i)) ;
end ;

figure(3) ;clf ;
plot(r,epsw(:,1),'b-',r,epsw(:,2),'g-',r,epsw(:,3),'r-',r,epsw(:,4),'c-', ...
     r,epswq(:),'m-') ;
legend('k=0','k=1','k=2','k=3','quad') ;
%qp=(r<rmax).*(f*c*d).*r.^(d-1).*(n-v).*(1-f*c*r.^d).^(n-v-1) ;
%plot(r,epswq(:),'r-',r,qp,'g-',r,epswq(:).*qp(:),'m-') ;
%legend('quad','q','quadq') ; title('expepswq*q') ;
figure(4) ;clf ;
plot(r,epsw(:,1)-epswq(:),'b-',r,epsw(:,2)-epswq(:),'g-',r,epsw(:,3)-epswq(:),'r-',r,epsw(:,4)-epswq(:),'c-')
legend('k=1','k=5','k=10','k=20') ; title('err expepsw') ;
end ;



% verify quadrature
%quad(@(rr)(n-v-1)*(1-f.*c.*rr.^d).^(n-v-2).*f.*c.*d.*rr.^(d-1).*v.*f.*c.* ...
%     d./rr.*(f*c).^(-1/d).*(f*c*d).^(-1).*beta(v,1/d+1),0,rmax)
%
%(n-v-1)*v*beta(v,1/d+1)*beta(n-v-1,1-1/d)

%quad(@(rr)(1-f.*c.*rr.^d).^(n-v-2).*rr.^(d-2),0,rmax)
%
%(f*c*d)^(-1)*(f*c)^(+1/d)*beta(n-v-1,1-1/d)

if 0,
alpha=f*c ;
for k=0:min(v-1,3),
  disp([ 'k=' num2str(k) ]) ;
  (n-v-1)*v*(alpha*d)^2*nchoosek(v-1,k)*(-alpha)^k/(d+k+1)*quad(@(rr)(1-alpha.*rr.^d).^(n-v-2).*rr.^(2*d+k-1),0,rmax)
  (n-v-1)*v*d*nchoosek(v-1,k)*(-alpha)^k*alpha^(-k/d)/(d+k+1)*beta(n-v-1,2+k/d) 
end
end ;

% evaluate the expected relative error as a function of V
if 0,
alpha=f*c ;
vs=1:nc ;
nvs=length(vs) ;
epsf=zeros(1,nvs) ;
epsq=zeros(1,nvs) ;
kmax=5 ;
for i=1:nvs,
  v=vs(i) ;
  %epsq(i)=quadl(@(uw)expepswv(uw,kmax),0,3) ;
  sumk=0 ;
  for k=0:min(v-1,kmax),
    sumk=sumk+nchoosek(v-1,k)*(-1)^k*alpha^(k-k/d)/(d+k+1)*beta(nc-v,2+k/d) ...
         ;
  end ;
  epsf(i)=1+(nc-v)*(-1/nc+v*beta(v,1/d+1)*beta(nc-v,1-1/d)-v*d*sumk) ;
  % epsf(i)=1+(n-v-1)*(-1/(n-1)+v*beta(v,1/d+1)*beta(n-v-1,1-1/d)) ;
end ;
figure(1) ; clf
%plot(vs,epsq,'r-',vs,epsf,'b-') ; legend('quad','form') ;
plot(vs,epsf,'b-') ; legend('quad','form') ;
ylabel('relative error') ; xlabel('v') ;
end ;

%keyboard


function yy=expepswq(uw) 
  yy=quadl(@(rr)rr./min(rr,uw).*v.*(1-f.*c.*rr.^d).^(v-1).*(f*c*d).*rr.^(d-1),0,rmax)  ;
  yy=yy*(uw<rmax).*(f*c*d).*uw.^(d-1).*(n-v).*(1-f*c*uw.^d).^(n-v-1) ;
  
end 


function yy=expepsw2(uw,kmax) ;
% expected value of the relative error given uw = mu_w
yy=1-(1-f*c*uw.^d).^v ;
intr=quad(@(rr)(1-f.*c.*rr.^d).^(v-1).*rr.^d,uw,rmax)  ;
yy=yy+v*f*c*d/uw*intr ;

end ;

function rxx=intgpoly(xx,kmax) ;
rxx= 0 ;
swn=warning ;
warning('off','MATLAB:nchoosek:LargeCoefficient') ;
for kk=min(v-1,kmax):-1:0,
  rxx=rxx+nchoosek(v-1,kk)*(-f*c)^kk/(d+kk+1)*xx.^(d+kk+1) ;
end ;
warning(swn) ;
end ;  

function yy=expepsw(uw,kmax) ;
% expected value of the relative error given uw = mu_w
yy=1-(1-f*c*uw.^d).^v ;
intrmax=(f*c)^(-1/d)*(f*c*d)^(-1)*beta(v,1/d+1) ;
yy=yy+v*f*c*d/uw*(intrmax-intgpoly(uw,kmax)) ;
%yy=yy+v*f*c*d/uw*intrmax ;
yy=yy*(uw<rmax).*(f*c*d).*uw.^(d-1).*(n-v-1).*(1-f*c*uw.^d).^(n-v-2) ;
end ;

function yy=expepswv(uw,kmax) ;
  nn=size(uw,2) ; assert(size(uw,1)==1) ;
  yy=zeros(1,nn) ;
  for iii=1:nn,
    yy(iii)=expepsw(uw(iii),kmax) ;
  end ;
end 


end