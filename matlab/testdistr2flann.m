function testdistr2flann(distr)
% test of the depence on N and a distribution 'distr'
% FLANN only

%clear all;
% nv=[1e4 1e5 1e6] ;
nv=[1e5] ;
%nv=[1e6] ;
dv=[2 5 10 20] ;
tv={distr} ;

%tv={'normal','uniform','corrnormal','corruniform','clustered', ...
%    'subspace','imgneighb'} ;
%;
%tv={'imgneighb'} ;
nn=length(nv) ;
nd=length(dv) ; 
nt=length(tv) ;
nrep=1 ;
bldtm=zeros(nd,nn,nt) ; 
srtm=zeros(nd,nn,nt) ; % search times
bldtmr=zeros(nd,nn,nt,nrep) ; % build times
srtmr=zeros(nd,nn,nt,nrep) ; % search times
annbldtmr=zeros(nd,nn,nt,nrep) ; % build times
annsrtmr=zeros(nd,nn,nt,nrep) ; % search times
annbldtm=zeros(nd,nn,nt) ; 
annsrtm=zeros(nd,nn,nt) ; % search times
flannbldtmr=zeros(nd,nn,nt,nrep) ; % build times
flannsrtmr=zeros(nd,nn,nt,nrep) ; % search times
flannbldtm=zeros(nd,nn,nt) ; 
flannsrtm=zeros(nd,nn,nt) ; % search times

for it=1:nt,
  t=tv{it} ;
  for id=1:nd,
    d=dv(id) ;
    if d<=2, % choose leaf size
        l=50 ;
    elseif d<=18
        l=70 ;
    elseif d<=50
        l=100 ;
    else 
        l=150 ;
    end ;
%    if d<=5,
%      l=20 ;
%    else
%      l=4 ;
%    end ;
    for in=1:nn,
      n=nv(in) ;
      for ir=1:nrep,
        a=gendata(t,n,d) ;
        save('-ascii',sprintf('ddata%s.txt',distr),'a') ;
         disp(sprintf('Testdist ir=%d t=%s d=%d n=%d started\n',ir,t,d,n)) 
         fd=fopen(sprintf('ddep%s.cmd',distr),'w') ;
         fprintf(fd,'set_dim %d\n',d) ;
         fprintf(fd,'set_leaf_size %d\n',l) ;
         fprintf(fd,'read_data_pts ddata%s.txt\n',distr) ;
         % fprintf(fd,'vnucko_build_tree\n') ;
         % fprintf(fd,'vnucko_all_nn_search\n') ;
         % fprintf(fd,'ann_build_tree\n') ;
         % fprintf(fd,'ann_all_nn_search\n') ;
         fprintf(fd,'set_flann_checks %d\n',n) ;
         fprintf(fd,'set_flann_trees %d\n',1) ;
         fprintf(fd,'flann_build_tree\n') ;
         fprintf(fd,'flann_entropy\n') ;

         fprintf(fd,'quit\n') ;
         fclose(fd) ;
         system(sprintf('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <ddep%s.cmd >ddep%s.out',distr,distr)) ;
         r=load(sprintf('ddep%s.out',distr)) ;
         % bldtmr(id,in,it,ir)=r(1) ;
         % srtmr(id,in,it,ir)=r(2) ;
         % annbldtmr(id,in,it,ir)=r(3) ;
         % annsrtmr(id,in,it,ir)=r(4) ;
         flannbldtmr(id,in,it,ir)=r(1) ;
         flannsrtmr(id,in,it,ir)=r(3) ;
      end ; % for ir
      bldtm(id,in,it)=mean(bldtmr(id,in,it,:)) ;
      srtm(id,in,it)=mean(srtmr(id,in,it,:)) ;
      annbldtm(id,in,it)=mean(annbldtmr(id,in,it,:)) ;
      annsrtm(id,in,it)=mean(annsrtmr(id,in,it,:)) ;
      flannbldtm(id,in,it)=mean(flannbldtmr(id,in,it,:)) ;
      flannsrtm(id,in,it)=mean(flannsrtmr(id,in,it,:)) ; 
      save(['testdistr2flann' distr]) ;
    end ;  % for in
end ; % for id
end ; % for it
