% Visualize the results of the testndep experiment

clear all 
close all
load testndep

% visualize for each 'd' separately
for i=1:nd,
  d=dv(i) ;
  disp(['Dimension ' num2str(d)]) ;
  figure ;
  h=loglog(nv,bldtm(i,:)+srtm(i,:),'b-o',nv,annbldtm(i,:)+annsrtm(i,:),'g-o',...
         nv,bldtm(i,:)+nnnsrtm(i,:),'c-o',nv,flannbldtm(i,:)+flannsrtm(i,:),'m-o',nv,brutesrtm(i,:),'r-o') ;
  set(h,'linewidth',2) ;
  set(gca,'FontSize',18) ;
  legend('BBF','ANN','BBF NNN','FLANN','brute','Location','SouthEast') ;
  legend('boxoff') ;
  xlabel('number of points') ; ylabel('Time [s]') ;
  print('-depsc2',[ 'ndepgraphtotd' num2str(d) '.eps' ] ) ; 
  title(['Total times for d=' num2str(d)]) ;

  figure ;
  h=loglog(nv,bldtm(i,:)+srtm(i,:),'b-o',nv,annbldtm(i,:)+annsrtm(i,:),'g-o',...
         nv,bldtm(i,:)+nnnsrtm(i,:),'c-o',nv,flannbldtm(i,:)+flannsrtm(i,:),'m-o') ;
  set(h,'linewidth',2) ;
  set(gca,'FontSize',18) ;
  legend('BBF','ANN','BBF NNN','FLANN','Location','SouthEast') ;
  legend('boxoff') ;
  xlabel('number of points') ; ylabel('Time [s]') ;
  print('-depsc2',[ 'ndepgraphtotnobruted' num2str(d) '.eps' ] ) ; 
  title(['Total times for d=' num2str(d)]) ;

  
  
  % figure ;
  % h=semilogx(nv,(bldtm(i,:)+srtm(i,:))./(annbldtm(i,:)+annsrtm(i,:)),'b-o',...
  %            nv,(bldtm(i,:)+nnnsrtm(i,:))./(annbldtm(i,:)+annsrtm(i,:)),'c-o',...
  %            brutesrtm(i,:),(annbldtm(i,:)+annsrtm(i,:)),'g-o') ; 
  % set(h,'linewidth',2) ;
  % set(gca,'FontSize',18) ;
  % axis([1e2 1e6 0 3]) ;
  % legend('BBF','BBF NNN','Location','SouthEast') ;
  % legend('boxoff') ;
  % xlabel('number of points') ; ylabel('Time rel.') ;
  % print('-depsc2',[ 'ndepgraphreld' num2str(d) '.eps' ] ) ; 
  % title(['Relative total times for d=' num2str(d)]) ;
    
if 0,  
  figure ;
  h=loglog(nv,bldtm(i,:),'b--o',nv,srtm(i,:),'b-.o',...
         nv,nnnsrtm(i,:),'c-.o',nv,annbldtm(i,:),'g--o',nv,annsrtm(i,:),'g-.o') ;
  set(h,'linewidth',2)
  set(gca,'FontSize',18) ;
  legend('BBF build','BBF search','BBF NNN search','ANN build','ANN search','Location','SouthEast') ;
  legend('boxoff') ;
  xlabel('number of points') ; ylabel('Time [s]') ;
  print('-depsc2',[ 'ndepgraphbldsrc' num2str(d) '.eps' ] ) ; 
  title(['Build and search times for d=' num2str(d)]) ;
end ;


end ;

if 0,

figure ;

% The legend to be edited
bldtm=bldtm( [1 3 5 6:8],:) ;
srtm=srtm( [1 3 5 6:8],:) ;
nnnsrtm=nnnsrtm( [1 3 5 6:8],:) ;
annsrtm=annsrtm( [1 3 5 6:8],:) ;
annbldtm=annbldtm( [1 3 5 6:8],:) ;
brutesrtm=brutesrtm( [1 3 5 6:8],:) ;
axis([1e2 1e6 1e-5 1e4]) ;
h=loglog(nv,bldtm+srtm,'-o') ; 
set(h,'linewidth',2)
set(gca,'FontSize',18) ;
legend('d=1','d=3','d=5','d=10','d=15','d=20','Location','SouthEast') ;
print('-depsc2','ndepgraphbbf.eps') ;
title('BBF total times') ;

figure ; 
h=loglog(nv,brutesrtm,'-o') ; 
set(h,'linewidth',2)
set(gca,'FontSize',18) ;
axis([1e2 1e6 1e-5 1e4]) ;
  xlabel('number of points') ; ylabel('Time [s]') ;
legend('d=1','d=3','d=5','d=10','d=15','d=20','Location','SouthEast') ;
  legend('boxoff') ;
print('-depsc2','ndepgraphbrute.eps') ;
title('Brute force total times') ;

figure ;
% The legend to be edited
h=loglog(nv,bldtm+nnnsrtm,'-o') ; 
axis([1e2 1e6 1e-5 1e4]) ;
set(h,'linewidth',2)
set(gca,'FontSize',18) ;
legend('d=1','d=3','d=5','d=10','d=15','d=20','Location','SouthEast') ;
  xlabel('number of points') ; ylabel('Time [s]') ;
print('-depsc2','ndepgraphbbfnnn.eps') ;
title('BBF NNN total times') ;

figure ;
% The legend to be edited
h=loglog(nv,annbldtm+annsrtm,'-o') ; 
axis([1e2 1e6 1e-5 1e4]) ;
set(h,'linewidth',2)
set(gca,'FontSize',18) ;
%legend('d=1','d=2','d=3','d=4','d=5','d=10','d=15','d=20','Location','SouthEast');
  xlabel('number of points') ; ylabel('Time [s]') ;
legend('d=1','d=3','d=5','d=10','d=15','d=20','Location','SouthEast') ;
  legend('boxoff') ;
print('-depsc2','ndepgraphann.eps') ;
title('ANN total times') ;

end

disp('converting') ;
system('for i in ndepgraph*.eps ; do echo $i ; epstopdf $i ; done') ;
system('cp -v ndepgraph*.pdf ~/tex/annsearch/figs/') ;
