function [h,t,leafsize]=bbfbestd(d,maxvisited) ;

ll=10 ; [hl,tl]=bbfwithd(d,ll,maxvisited) ;
rr=200 ;[hr,tr]=bbfwithd(d,rr,maxvisited) ;

[h,t,leafsize]=binarysearch(ll,rr,tl,tr,hl,hr,d,maxvisited) ;

 

% automatically determines the best leafsize

function [h,t,leafsize]=binarysearch(ll,rr,tl,tr,hl,hr,d,maxvisited) 
    while (ll<rr-1)
      mm=ceil((ll+rr)/2) ;
      [hh,tt]=bbfwithd(d,mm,maxvisited) ;
      if tl<=tr,
        rr=mm ; tr=tt ; hr=hh ;
      else
        ll=mm ; tl=tt ; hl=hh ;
      end ;
    end % while
    if tl<=tr,
      leafsize=ll ; t=tl ; h=hl ;
    else
      leafsize=rr ; t=tr ; h=hr ;
    end ;
    

