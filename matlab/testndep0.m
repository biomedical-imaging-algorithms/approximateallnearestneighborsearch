% test of the dependence on N 

clear all
nv=[100 1000 10000 100000 1e6] ;
dv=[ 1 2 3 4 5 10 15 20  ] ; % dimensions
nn=length(nv) ;
nd=length(dv) ; 
nrep=10 ;

bldtm=zeros(nd,nn) ; 
srtm=zeros(nd,nn) ; % search times
bldtmr=zeros(nd,nn,nrep) ; % build times
srtmr=zeros(nd,nn,nrep) ; % search times
nnnsrtmr=zeros(nd,nn,nrep) ; % search times
brutesrtmr=zeros(nd,nn,nrep) ; % search times
annbldtmr=zeros(nd,nn,nrep) ; % build times
annsrtmr=zeros(nd,nn,nrep) ; % search times
annbldtm=zeros(nd,nn) ; 
annsrtm=zeros(nd,nn) ; % search times
nnnsrtm=zeros(nd,nn) ; % search times
brutesrtm=zeros(nd,nn) ; % search times


for id=1:nd,
 d=dv(id) ;
 dobrute=1 ; % perform brute force search unless it takes too long
 if d<=2,
   l=16 ;
 else
   l=1 ;
 end ;
 for in=1:nn,
  n=nv(in) ; 
  for ir=1:nrep,
    a=gendata('normal',n,d) ;
    save('-ascii','ndata.txt','a') ;
    disp(sprintf('Testndep ir=%d d=%d n=%d started\n',ir,d,n)) 
    fd=fopen('ndep.cmd','w') ;
    fprintf(fd,'set_dim %d\n',d) ;
    fprintf(fd,'set_leaf_size %d\n',l) ;
    fprintf(fd,'read_data_pts ndata.txt\n') ;
    fprintf(fd,'vnucko_build_tree\n') ;
    fprintf(fd,'vnucko_all_nn_search\n') ;
    fprintf(fd,'ann_build_tree\n') ;
    fprintf(fd,'ann_all_nn_search\n') ;
    fprintf(fd,'vnucko_nnn_search\n') ;
    if dobrute,
      fprintf(fd,'brute_all_nn_search\n') ;
    end ;
    fprintf(fd,'quit\n') ;
    fclose(fd) ;
    system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <ndep.cmd >ndep.out') ;
    r=load('ndep.out') ;
    bldtmr(id,in,ir)=r(1) ;
    srtmr(id,in,ir)=r(2) ;
    annbldtmr(id,in,ir)=r(3) ;
    annsrtmr(id,in,ir)=r(4) ;
    nnnsrtmr(id,in,ir)=r(5) ;
    if dobrute,
      brutesrtmr(id,in,ir)=r(6) ;
    end ;
    
  end ; % end of for ir
  bldtm(id,in)=mean(bldtmr(id,in,:)) ;
  srtm(id,in)=mean(srtmr(id,in,:)) ;
  annbldtm(id,in)=mean(annbldtmr(id,in,:)) ;
  annsrtm(id,in)=mean(annsrtmr(id,in,:)) ;
  nnnsrtm(id,in)=mean(nnnsrtmr(id,in,:)) ;
  if dobrute,
    brutesrtm(id,in)=mean(brutesrtmr(id,in,:)) ;
  else
    brutesrtm(id,in)=nan ;
  end ;
  disp(sprintf('Testleaf d=%3d n=%d finished. Build %g;  Search %g ANN-build %g ANN-search %g NNN search %g Brute %g\n',d,n,bldtm(id,in),srtm(id,in),annbldtm(id,in),annsrtm(id,in),nnnsrtm(id,in), brutesrtm(id,in))) ;
  if dobrute>0 && brutesrtm(id,in)>10,
    dobrute=0 ;
  end ;
  
  save testndep
 end ;
end ;



