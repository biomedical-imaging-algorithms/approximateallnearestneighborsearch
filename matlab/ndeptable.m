% Create a table from the testndep experiment
clear all 
close all
load testndep

totbbf=bldtm+srtm ; 
totnnn=bldtm+nnnsrtm ;
totann=annbldtm+annsrtm ;
totflann=flannbldtm+flannsrtm ;

totmin=min(min(totflann,totann),min(totbbf,totnnn)) ;

nnv=length(nv) ;
nd=length(dv) ; 
fd=fopen('ndep.tex','w') ;
fprintf(fd,'\\begin{tabular}{ll|*{%d}{r}}\n',nd) ;
fprintf(fd,'$n$ & $d$  ') ;
for d=dv,
  fprintf(fd,'& %d',d) ;
end ;
fprintf(fd,'\\\\ \\hline \n') ;

totmin=

for j=3:nnv,
   fprintf(fd,'\\multirow{3}{*}{$10^%d$} & BBF ', round(log10(nv(j)))) ;
   for i=1:nd,
       d=dv(i) ;
       if (totbbf(i,j)==0.0),
           fprintf(fd,'& --- ') ;
       else
       if (totbbf(i,j)<=totmin(i,j))
           fprintf(fd,'& \\textbf{%4.2f}',totbbf(i,j)) ;
       else
           fprintf(fd,'& %4.2f',totbbf(i,j)) ;
       end ;
       end ;
   end ;
   fprintf(fd,'\\\\ \n') ;
   fprintf(fd,'& NNN BBF ') ;
   for i=1:nd,
       d=dv(i) ;
       if (totnnn(i,j)==0.0),
           fprintf(fd,'& ---') ;
       else
       if (totnnn(i,j)<=totmin(i,j)),
           fprintf(fd,'& \\textbf{%4.2f}',totnnn(i,j)) ;
       else
           fprintf(fd,'& %4.2f',totnnn(i,j)) ;
       end ;
       end ;
   end ;
   fprintf(fd,'\\\\ \n') ;
   fprintf(fd,'& ANN ') ;
   for i=1:nd,
       d=dv(i) ;
       if (totann(i,j)==0.0),
           fprintf(fd,'& ---') ;
       else
       if (totann(i,j)<=totmin(i,j)),
           fprintf(fd,'& \\textbf{%4.2f}',totann(i,j)) ;
       else
           fprintf(fd,'& %4.2f',totann(i,j)) ;
       end ;
       end ;
   end ;
   fprintf(fd,'\\\\ \n') ;
   fprintf(fd,'& FLANN ') ;
   for i=1:nd,
       d=dv(i) ;
       if (totflann(i,j)==0.0),
           fprintf(fd,'& ---') ;
       else
       if (totflann(i,j)<=totmin(i,j)),
           fprintf(fd,'& \\textbf{%4.2f}',totflann(i,j)) ;
       else
           fprintf(fd,'& %4.2f',totflann(i,j)) ;
       end ;
       end ;
   end ;
   fprintf(fd,'\\\\ \\hline \n') ;
end ;

fprintf(fd,'\\end{tabular}\n') ;
fclose(fd) ;
system('cp -v ndep.tex ~/tex/annsearch/') ;

