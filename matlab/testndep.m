% test of the dependence on N 

clear all
%nv=[100 1000 10000 100000 1e6] ;
nv=[1e5] ;
% nv=[10000] ;
%dv=[ 1 2 3 4 5 10 15 20  ] ; % dimensions
dv=[ 5 15  ] ; % dimensions
%dv=[ 5  ] ; % dimensions
nn=length(nv) ;
nd=length(dv) ; 
%nrep=10 ;
 nrep=1 ;

bldtm=zeros(nd,nn) ; 
srtm=zeros(nd,nn) ; % search times
bldtmr=zeros(nd,nn,nrep) ; % build times
srtmr=zeros(nd,nn,nrep) ; % search times
nnnsrtmr=zeros(nd,nn,nrep) ; % search times
brutesrtmr=zeros(nd,nn,nrep) ; % search times
annbldtmr=zeros(nd,nn,nrep) ; % build times
annsrtmr=zeros(nd,nn,nrep) ; % search times
annbldtm=zeros(nd,nn) ; 
annsrtm=zeros(nd,nn) ; % search times
nnnsrtm=zeros(nd,nn) ; % search times
brutesrtm=zeros(nd,nn) ; % search times
flannsrtm=zeros(nd,nn) ; % search times
flannbldtm=zeros(nd,nn) ; % build times
lshrtm=zeros(nd,nn) ; % search times
lshbldtm=zeros(nd,nn) ; % build times
%bbfannbldtmr=zeros(nd,nn,nrep) ; % build times
%bbfannsrtmr=zeros(nd,nn,nrep) ; % search times
flannbldtmr=zeros(nd,nn,nrep) ; % build times
flannsrtmr=zeros(nd,nn,nrep) ; % search times
                               %lshbldtmr=zeros(nd,nn,nrep) ; % build times
                               %lshsrtmr=zeros(nd,nn,nrep) ; % search times
bbftbbbldtmr=zeros(nd,nn,nrep) ; % build times
bbftbbsrtmr=zeros(nd,nn,nrep) ; % search times
bbftbbsrtm=zeros(nd,nn) ; % search times
bbftbbbldtm=zeros(nd,nn) ; % build times


for id=1:nd,
 d=dv(id) ;
 dobrute=1 ; % perform brute force search unless it takes too long
 flanntrees=1 ;
 % lshw=0.25 ;
 annepsilon=0 ;
 if d<=2, % choose leaf size
   l=50 ;
 elseif d<=18
     l=70 ;
 elseif d<=50
     l=100 ;
 else 
   l=150 ;
 end ;
 for in=1:nn,
  n=nv(in) ; 
  flannchecks=n ;
  for ir=1:nrep,
    a=gendata('normal',n,d) ;
    save('-ascii','ndata.txt','a') ;
    disp(sprintf('Testndep ir=%d d=%d n=%d started\n',ir,d,n)) 
    fd=fopen('ndep.cmd','w') ;
    fprintf(fd,'set_dim %d\n',d) ;
    % bbf
    fprintf(fd,'set_leaf_size %d\n',l) ;
    %    fprintf(fd,'set_max_visited %d\n',maxvisited) ;
    fprintf(fd,'read_data_pts ndata.txt\n') ;
    fprintf(fd,'vnucko_build_tree\n') ;
    fprintf(fd,'vnucko_all_nn_search\n') ;
    % bbf-ann
    fprintf(fd,'vnucko_nnn_search\n') ;
    % ann
    fprintf(fd,'set_ann_epsilon %f\n',annepsilon) ;
    fprintf(fd,'ann_build_tree\n') ;
    fprintf(fd,'ann_all_nn_search\n') ;
    % flann
    fprintf(fd,'set_flann_checks %d\n',flannchecks) ;
    fprintf(fd,'set_flann_trees %d\n',flanntrees) ;
    fprintf(fd,'flann_build_tree\n') ;
    fprintf(fd,'flann_entropy\n') ;
    % bbf-tbb
    fprintf(fd,'set_usetbb %d\n',0) ;
    fprintf(fd,'vnucko_build_tree\n') ;
    fprintf(fd,'vnucko_all_nn_search\n') ;
    % if dolsh,
    %   fprintf(fd,'set_nlshfuncs %d\n',nlshfuncs) ;
    %   fprintf(fd,'set_nlshtables %d\n',nlshtables) ;
    %   fprintf(fd,'set_lshw %f\n',lshw) ;
    %   %      fprintf(fd,'lsh_build_tree\n') ;
    %   %      fprintf(fd,'lsh_entropy\n') ;
    % end
    if dobrute,
      fprintf(fd,'brute_all_nn_search\n') ;
    end ;
    fprintf(fd,'quit\n') ;
    fclose(fd) ;
    system('unset LD_LIBRARY_PATH ; nice -19 ../bin/allnn_exp <ndep.cmd >ndep.out') ;
    r=load('ndep.out') ;
    % bbf
    bldtmr(id,in,ir)=r(1) ;
    srtmr(id,in,ir)=r(2) ;
    nnnsrtmr(id,in,ir)=r(3) ;
    annbldtmr(id,in,ir)=r(4) ;
    annsrtmr(id,in,ir)=r(5) ;
    flannbldtmr(id,in,ir)=r(6) ;
    % r(7) is entropy
    flannsrtmr(id,in,ir)=r(8) ;
    bbftbbbldtmr(id,in,ir)=r(9) ;
    bbftbbsrtmr(id,in,ir)=r(10) ;
    % if dolsh,
    %   lshbldtmr(id,in,ir)=r(11) ;
    %   % r(12) is entropy
    %   lshsrtmr(id,in,ir)=r(13) ;
    % end
    if dobrute,
      brutesrtmr(id,in,ir)=r(11) ;
    end ;
    
  end ; % end of for ir
  bldtm(id,in)=mean(bldtmr(id,in,:)) ;
  srtm(id,in)=mean(srtmr(id,in,:)) ;
  annbldtm(id,in)=mean(annbldtmr(id,in,:)) ;
  annsrtm(id,in)=mean(annsrtmr(id,in,:)) ;
  nnnsrtm(id,in)=mean(nnnsrtmr(id,in,:)) ;
  flannbldtm(id,in)=mean(flannbldtmr(id,in,:)) ;
  flannsrtm(id,in)=mean(flannsrtmr(id,in,:)) ;
  bbftbbbldtm(id,in)=mean(bbftbbbldtmr(id,in,:)) ;
  bbftbbsrtm(id,in)=mean(bbftbbsrtmr(id,in,:)) ;
  % if dolsh,
  %   lshbldtm(id,in)=mean(lshbldtmr(id,in,:)) ;
  %   lshsrtm(id,in)=mean(lshsrtmr(id,in,:)) ;
  % else
  %   lshbldtm(id,in)=nan ;
  %   lshsrtm(id,in)=nan ;
  % end ;
  if dobrute,
    brutesrtm(id,in)=mean(brutesrtmr(id,in,:)) ;
  else
    brutesrtm(id,in)=nan ;
  end ;
  disp(sprintf(['Testleaf d=%3d n=%d finished. Build %f;  Search %f ; NNN ' ...
                'search %f BBFTBB build %f ; BBFTBB search %g ; FLANN ' ...
                'build %f ; FLANN search %f ; ANN-build %f ANN-search %f; Brute %f\n'],d,n,bldtm(id,in),srtm(id,in),nnnsrtm(id,in),bbftbbbldtm(id,in),bbftbbsrtm(id,in),flannbldtm(id,in),flannsrtm(id,in),annbldtm(id,in),annsrtm(id,in), brutesrtm(id,in))) ;
  if dobrute>0 && brutesrtm(id,in)>100,
    dobrute=0 ;
  end ;
  
  save testndep
 end ;
end ;



