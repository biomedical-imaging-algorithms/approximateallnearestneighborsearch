function testndep3(method) ;

% test of the dependence on N 

%clear all
nv=[100 1000 10000 100000 1e6] ;
%nv=[1e5] ;
% nv=[10000] ;
%dv=[ 1 2 3 4 5 10 15 20  ] ; % dimensions
dv=[ 5 15  ] ; % dimensions
%dv=[ 5  ] ; % dimensions
nn=length(nv) ;
nd=length(dv) ; 
%nrep=10 ;
 nrep=1 ;

bldtm=zeros(nd,nn) ; 
srtm=zeros(nd,nn) ; % search times
bldtmr=zeros(nd,nn,nrep) ; % build times
srtmr=zeros(nd,nn,nrep) ; % search times


for id=1:nd,
 d=dv(id) ;
 dobrute=1 ; % perform brute force search unless it takes too long
 flanntrees=1 ;
 % lshw=0.25 ;
 annepsilon=0 ;
 if d<=2, % choose leaf size
   l=15 ;
 elseif d<=18
     l=30 ;
 elseif d<=50
     l=100 ;
 else 
   l=150 ;
 end ;
 for in=1:nn,
  n=nv(in) ; 
  flannchecks=n ;
  for ir=1:nrep,
    a=gendata('normal',n,d) ;
    save('-ascii','ndata.txt','a') ;
    disp(sprintf('Testndep ir=%d d=%d n=%d started\n',ir,d,n)) 
    fd=fopen(['ndep' method '.cmd'],'w') ;
    fprintf(fd,'set_dim %d\n',d) ;
    % bbf
    fprintf(fd,'set_leaf_size %d\n',l) ;
    %    fprintf(fd,'set_max_visited %d\n',maxvisited) ;
    fprintf(fd,'read_data_pts ndata.txt\n') ;
    if strcmp(method,'bbf'),
        fprintf(fd,'vnucko_build_tree\n') ;
        fprintf(fd,'vnucko_all_nn_search\n') ;
    % ann
    elseif strcmp(method,'ann'),
        fprintf(fd,'set_ann_epsilon %f\n',annepsilon) ;
        fprintf(fd,'ann_build_tree\n') ;
        fprintf(fd,'ann_all_nn_search\n') ;
    elseif strcmp(method,'flann')
        % flann
        fprintf(fd,'set_flann_checks %d\n',flannchecks) ;
        fprintf(fd,'set_flann_trees %d\n',flanntrees) ;
        fprintf(fd,'flann_build_tree\n') ;
        fprintf(fd,'flann_entropy\n') ;
    elseif strcmp(method,'brute')
        if dobrute,
            fprintf(fd,'brute_all_nn_search\n') ;
        end ;
    else
        error('Unknown method') ;
    end ;
    fprintf(fd,'quit\n') ;
    fclose(fd) ;
    system(sprintf('unset LD_LIBRARY_PATH ; ~/work/increst/bin/allnn_exp <ndep%s.cmd >ndep%s.out',method,method)) ;
    r=load(['ndep' method '.out']) ;
    % bbf
    if strcmp(method,'brute'),
        if dobrute,
            srtmr(id,in,ir)=r(1) ;
        end    
    elseif strcmp(method,'flann'),
        bldtmr(id,in,ir)=r(1) ;
        srtmr(id,in,ir)=r(3) ;
    else
        bldtmr(id,in,ir)=r(1) ;
        srtmr(id,in,ir)=r(2) ;
    end
    
  end ; % end of for ir
  if ~strcmp(method,'brute') || dobrute,
      bldtm(id,in)=mean(bldtmr(id,in,:)) ;
      srtm(id,in)=mean(srtmr(id,in,:)) ;
  else
      srtm(id,in)=nan ;
  end ;
  fprintf('Testleaf d=%3d n=%d finished. Build %f;  Search %f ;\n',d,n,bldtm(id,in),srtm(id,in)) ;
  if dobrute>0 && srtm(id,in)>100,
    dobrute=0 ;
  end ;
  
  save(['testndep3' method],'nv','bldtm','srtm') ;  
 end ;
end ;



