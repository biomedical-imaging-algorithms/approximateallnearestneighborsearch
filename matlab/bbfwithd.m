function [h,t]=bbfwithd(d,leafsize,maxvisited,fname) ;
% Times the BBF method with given parameters, returns entropy and time
%


oldfolder=cd('~/work/increst/tmp') ;
angl=0 ;

fd=fopen('bbfwithd.cmd','w') ;
if nargin<4 || isempty(fname),
    fname=sprintf('imgnb%d-%.2f.txt',d,angl) ;
else
    a=load(fname) ;
    [n,dim]=size(a) ;
end ;

fprintf(fd,'set_dim %d\n',dim) ;
fprintf(fd,'set_leaf_size %d\n',leafsize) ;
fprintf(fd,'read_data_pts %s\n',fname) ;
fprintf(fd,'set_max_visited %d\n',maxvisited) ;
fprintf(fd,'vnucko_build_tree\n') ;
fprintf(fd,'vnucko_entropy\n') ;
fprintf(fd,'quit\n') ;
fclose(fd) ;
system('unset LD_LIBRARY_PATH ; ~/work/increst/bin/allnn_exp <bbfwithd.cmd >bbfwithd.out') ;
r=load('bbfwithd.out')  ;
h=r(2) ;
t=r(1)+r(3); % total time

cd(oldfolder) ;
