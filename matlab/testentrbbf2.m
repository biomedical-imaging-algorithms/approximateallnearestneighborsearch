% test the accuracy of entropy estimation versus time
% for the BBF algorithm

clear all
%n=1e6 ;
%mv=[ 3 10 30 100 300 1000 3000 10000 1e5 1e6] ; % max number of visited points
%dv=[ 2 5 10 20 ] ; % dimensions to test
% n=1e5 ;
% mv=[ 3 5 10 30 100 1000 10000 ] ; % max number of visited points
% dv=[ 3 5 7 9 11 20 ] ; % dimensions to test
% nd=length(dv) ;
% nm=length(mv) ;
% nrep=100 ; 

n=1e5 ;
mv=[ 10 30 100 300 500 1000 3000 ] ;
%dv=[ 2 4 6 ] ;
dv=[ 6 ] ;
nrep = 1 ;
nd=length(dv) ;
nm=length(mv) ;

angl=0 ;


bldtmr=zeros(nd,nm,nrep) ;
srtmr=zeros(nd,nm,nrep) ;
entr=zeros(nd,nm,nrep) ;
bldtm=zeros(nd,nm) ;
srtm=zeros(nd,nm) ;
enterr=zeros(nd,nm) ;

oldfolder=cd('~/work/increst/tmp') ;


for id=1:nd,
  d=dv(id) ; 
%  trueent=d*(0.5+0.5*log(2*pi))
  % find true entropy
  fc=load(['evalrot-' num2str(d) '-brute']) ;
  ind=find(fc.angs==angl) ;
  trueent=fc.mres(1,ind) ;
  %  l=30 
  dim=2*d^2 ;  
 % if dim<=2, % choose leaf size
 %   l=15 ;
 % elseif dim<=18
 %     l=50 ;
 % elseif dim<=50
 %     l=100 ;
 % else 
 %   l=150 ;
 % end ;
 l=30 ;
 
 %  if d<=4, % choose leaf size
 %  l=20 ;
 % elseif d>=10
 %   l=4 ;
 % else
 %   l=round(16-6/5*d)
 % end ;
  for im=1:nm,
    m=mv(im) ;
    for ir=1:nrep,
      %a=gendata('normal',n,d) ;
      fname=sprintf('imgnb%d-%.2f.txt',d,angl) ;
      % a=gendata('imgneighb',n,d) ;
      % save('-ascii','nentdatabbf.txt','a') ;
      disp(sprintf('Testentrbbf ir=%d d=%d m=%d started\n',ir,d,m)) 
      fd=fopen('entrbbf.cmd','w') ;
      fprintf(fd,'set_dim %d\n',dim) ;
      fprintf(fd,'set_leaf_size %d\n',l) ;
      fprintf(fd,'read_data_pts %s\n',fname) ;
      fprintf(fd,'set_max_visited %d\n',m) ;
      fprintf(fd,'vnucko_build_tree\n') ;
      fprintf(fd,'vnucko_entropy\n') ;
      fprintf(fd,'quit\n') ;
      fclose(fd) ;
      system('unset LD_LIBRARY_PATH ; ~/work/increst/bin/allnn_exp <entrbbf.cmd >entrbbf.out') ;
      r=load('entrbbf.out') ;
      bldtmr(id,im,ir)=r(1) ;
      entr(id,im,ir)=r(2) ;
      srtmr(id,im,ir)=r(3) ;
    end ; % end for ir
    bldtm(id,im)=mean(bldtmr(id,im,:)) ;
    srtm(id,im)=mean(srtmr(id,im,:)) ;
    enterr(id,im)=sqrt(mean((entr(id,im,:)-trueent).^2)) ;
    disp(sprintf('Testentrbbf d=%3d m=%d finished. Build %g;  Search %g Enterr %g\n',d,m,bldtm(id,im),srtm(id,im),enterr(id,im))) ;
    save testentrbbf2
  end ; % for im
end ; % for id

cd(oldfolder) ;
