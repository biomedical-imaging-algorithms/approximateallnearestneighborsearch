imgt='sqrs' ;
tt='r' ;

crits={'ssd','cc','mi','nbmi'} ;
colors={'r','g','m','b'} ;

clf ;
for i=1:length(crits),
  d=load(sprintf('../tmp/testcritres-%s-%s-%s.mat',imgt,tt,crits{i})) ;
  if i==1,
    d.y=-d.y ;
  end
  d.y=d.y-min(d.y) ;
  d.y=d.y/max(d.y) ;
  plot(d.tv,d.y,[ colors{i} '-' ],'LineWidth',2) ; hold on ;
end

h1=xlabel('rotation angle [deg]') ;
h2=ylabel('criterion value [rel]') ;
set(gca,'FontSize',12) ;
set(h1,'FontSize',12) ;
set(h2,'FontSize',12) ;

grid on ;
print('-depsc','results/compcritsqrs.eps') ;
