function mi=neighbmi(fimg,gimg,params) ;
% Neighborhood mutual information image similarity criterion
% neighborhood size is params.nbsize, defaults to 3
%
% Jan Kybic, May 2011
  
if nargin<3,
  params=[] ;
end ;

if ~isfield(params,'nbsize'),
  params.nbsize=3 ;
end ;
  
fimg=nbfeatures(fimg,params.nbsize) ;
gimg=nbfeatures(gimg,params.nbsize) ;
mi=mutinf_klflann(fimg,gimg,params) ;  