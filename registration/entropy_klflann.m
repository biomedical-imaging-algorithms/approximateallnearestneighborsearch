function h=entropy_klflann(q,params) ;
% given data q of size (d-by-n) representing n samples in dimension d,
% calculate its entropy using KL estimator with the FLANN library
%
% params (if given) are passed to flann_search.
% params.uncert is the amplitude of the uniform noise to be added to the
% samples to avoid collisions. Alternatively, specify params.eps, as the
% minimum distance to be considered.
% 
  
  
[d,n]=size(q) ;
q=single(q) ; 

if nargin<2,
  params=[] ;
end ;

if ~isfield(params,'algorithm'),
  params.algorithm='kdtree' ;
end ;

if ~isfield(params,'trees'),
  params.trees=1 ;
end ;

if ~isfield(params,'checks'),
  params.checks=150 ;
end ;

if ~isfield(params,'uncert'),
  params.uncert=0 ;
end ;

if ~isfield(params,'eps'),
  params.eps=0 ;
end ;

if params.uncert>0,
  q=q+single(random('uniform',0,params.uncert,d,n))  ;
end ;

% find NN
[~,dists]=flann_search(q,q,2,params) ;
dists=dists(2,:) ;


if params.eps>0,
  eps2=params.eps^2 ;
  ind=dists<eps2 ;
  dists(ind)=eps2 ;
end ;

% calculate entropy
if any(dists<1e-300),
  warning('entropy_klflann: Some distances are negative or zero.') ;
end

logdist=0.5*d*sum(log(dists)) ;
eulergamma=0.577215664901532 ;
h=log(pi^(0.5*d)*(n-1)/gamma(1+d/2))+eulergamma+logdist/n ;