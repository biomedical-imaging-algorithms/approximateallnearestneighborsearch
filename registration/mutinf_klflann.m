function mi=mutinf_klflann(f,g,params) ;
% calculate mutual information between two vector variables f and g
% (must have the same number of columns)
%  
% Jan Kybic, May 2011
  
[fd,n]=size(f) ;
[gd,m]=size(g) ;
assert(m==n) ;

d=fd+gd ;

if nargin<3,
  params=[] ;
end

hf=entropy_klflann(f,params) ;
hg=entropy_klflann(g,params) ;
hfg=entropy_klflann([ f ; g],params) ;
mi=hf+hg-hfg ;