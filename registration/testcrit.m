function [f,g,tv,y]=testcrit(imgt,tt,critt) ;
% Perform one test for one image type, transformation type and
% criterion type. The resulting criterion values are saved into 
% a Matlab file and saved as a graph

fprintf('testcrit %s %s %s\n',imgt,tt,critt) ;  
  
  
[f,g,ofs]=getimgpair(imgt,tt) ;


% vector of parameters to test
switch tt
  case 'r'
    tv=-5:0.1:5 ;
    %tv=-5:1:5 ;
  case 't'  
    %tv=-5:0.1:5 ;
    tv=-5:1:5 ;
  otherwise
    error('Uknown transfom type tt') ;
end

tn=length(tv) ;
y=zeros(1,tn) ;

edg=30 ;

for ti=1:tn,
  t=tv(ti) ;
  ft=transfimg(f,tt,t+ofs) ;
  y(ti)=getcrit(ft(edg+1:end-edg,edg+1:end-edg),g(edg+1:end-edg,edg+1:end-edg),critt) ;
  fprintf('t=%f y=%f\n',t,y(ti)) ;
end ;

save(sprintf('../tmp/testcritres-%s-%s-%s.mat',imgt,tt,critt),'imgt', ...
     'tt','critt','tv','y') ;

figure(3) ;
clf ; plot(tv,y) ; 
print('-depsc',sprintf('../tmp/testcritgraph-%s-%s-%s.eps',imgt,tt,critt)) ...
  ;

switch tt
  case 'r'
    xlabel('angle') ;
  case 't'
    xlabel('shift') ;
  otherwise
    error('unknown tt') ;
end

ylabel(critt) ;
title(imgt) ;