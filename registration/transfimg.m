function [ft,x]=transfimg(f,tt,x) ;
% Transforms image [f] by a transformation [tt] described by parameter [x]
% for similarity criteria testing. [tt] is the type of the
% transformation, either 'r' (rotation, degrees) or 't' (translation,
% pixels). If [x] is not given, choose a default value.
%
% Jan Kybic, May 2011

[ny,nx]=size(f) ;

switch tt
  
  case 'r'
    if nargin<3,
      x=1.35304 ;
    end
    ft=single(imrotate(f,x,'bilinear','crop')) ;

  case 't'
    if nargin<3,
      x=1.7 ;
    end
    tform=maketform('affine',[1 0 0 ; 0 1 0 ; x 0 1 ]) ;
    ft=single(imtransform(f,tform,'bilinear','XData',[ 1 nx],'YData',[1 ...
                        ny],'UData',[1 nx],'VData',[1 ny])) ;
    
    
  otherwise
    error('unknown transformation type') ;
    
 end   
    