% load image
a=imread('cameraman.tif') ;
% calculate edges
gh = imfilter(a,fspecial('sobel') /8,'replicate');
gv = imfilter(a,fspecial('sobel')'/8,'replicate');
g=-(double(gv).^2+double(gh).^2).^0.3 ;
angofs=5 ;
g=imrotate(g,5,'bilinear','crop') ;
figure(1) ; clf ;
imagesc(a) ; colormap(gray) ; colorbar
figure(2) ; clf ;
imagesc(g) ;  colormap(gray) ; colorbar


angs=-5:0.1:5 ;
an=length(angs) ;
y=zeros(1,an) ;
yssd=zeros(1,an) ;
ycc=zeros(1,an) ;

params=[] ;
params.nbsize=3 ;
params.eps=1e-100 ;
parmas.checks=50 ;

g=single(g) ;

for ai=1:an,
  ang=angs(ai) ;
  ar=single(imrotate(a,ang+angofs,'bilinear','crop')) ;
  ssd=sum((single(ar(:))-single(g(:))).^2) ;
  rr=corrcoef(ar(:)-mean(ar(:)),g(:)-mean(g(:)))  ;
  cc=rr(1,2)/sqrt(rr(1,1)*rr(2,2)) ;
  mi=neighbmi(ar,g,params) ;
  % mi=0 ;
  fprintf('ang=%f mi=%f ssd=%f cc=%f\n',ang,mi,ssd,cc) ;
  y(ai)=mi ;
  yssd(ai)=ssd ;
  ycc(ai)=cc ;
end ;

figure(3) ;
clf ; plot(angs,y) ; xlabel('angle') ; ylabel('MI') ;
print('-depsc','results/rotcameramangMI.eps') ;

figure(4) ;
clf ; plot(angs,yssd) ; xlabel('angle') ; ylabel('SSD') ;
print('-depsc','results/rotcameramangSSD.eps') ;

figure(5) ;
clf ; plot(angs,ycc) ; xlabel('angle') ; ylabel('CC') ;
print('-depsc','results/rotcameramangCC.eps') ;


