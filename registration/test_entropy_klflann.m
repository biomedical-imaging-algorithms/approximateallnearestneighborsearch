% test entropy_klflann

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
n=10000 ;
nrep=1 ;
params=[] ;
params.eps=1e-50 ;
for d=1:10,
  for j=1:nrep,
    trueent=d*(0.5*log(2*pi)+0.5) ; 
    q=random('normal',0,1,d,n) ;
    h=entropy_klflann(q,params) ;
    fprintf('d=%d H_KL=%f H_true=%f err=%f\n',d,h,trueent,abs(h-trueent)) ...
        ;
  end
end