function [f,g,t]=getimgpair(imgt,tt)
% Get a pair of images [f,g] for similarity criteria testing
% [t] is the true parameter of the transformation
% [tt] is the type of the transformation, either 'r' (rotation) or 't'
% (translation) 
%
% Possible images are: 
% 'cam' - cameraman, no changes
% 'caml' - cameraman, intensity transformation
% 'camn' - cameraman, nonlinear intensity transformation
% 'came' - cameraman, edge detection
% 'text' - texture
% 'lenae' - 
% 'rice'
% 'sqrs' - squares
%  
% Jan Kybic, May 2011
  
switch imgt
  
  case 'cam'
    f=single(imread('cameraman.tif')) ;
    g=f ;
    
  case 'caml'
    f=single(imread('cameraman.tif')) ;
    g=100+f/2 ;
  
  case 'camn'
    f=single(imread('cameraman.tif')) ;
    g=120-100*sin(f/150*pi) ;
    % g=20+0.1*abs(f-127).^2 ;
    
  case 'came'
    f=single(imread('cameraman.tif')) ;
    % f=single(imread('rice.png')) ;
    % f = imfilter(f,fspecial('gaussian',10,2),'replicate');
    [m,n] = size(f);
    [x,y] = meshgrid( 1:n, 1:m );
    gh = imfilter(f,fspecial('sobel') /8,'replicate');
    gv = imfilter(f,fspecial('sobel')'/8,'replicate');
    g=-(double(gv).^2+double(gh).^2).^0.7 ;
    % f=f+y ;
    f=normimg(f) ;
    f=f>100 ;
    g=normimg(g) ;
    g=g>100 ;
    g=normimg(g) ;
    f=normimg(f) ;
    % g=g>120 ;
    % f=f+1*x ;
    % f=f+random('normal',0,10,m,n) ;
    %g=g+0.5*x ;
    % g=(double(gv).^2+double(gh).^2)<1000 ;
    % h=imhist(uint8(f)) ;

  case 'rice'
    f=single(imread('rice.png')) ;
    % f=single(imread('rice.png')) ;
    % f = imfilter(f,fspecial('gaussian',10,2),'replicate');
    %[m,n] = size(f);
    %[x,y] = meshgrid( 1:n, 1:m );
    gh = imfilter(f,fspecial('sobel') /8,'replicate');
    gv = imfilter(f,fspecial('sobel')'/8,'replicate');
    g=-(double(gv).^2+double(gh).^2).^0.7 ;
    [m,n] = size(f);
    [x,y] = meshgrid( 1:n, 1:m );
    f=f+x ;
    f=normimg(f) ;
    g=normimg(g) ;
    % g=g>120 ;
    % f=f+1*x ;
    % f=f+random('normal',0,10,m,n) ;
    %g=g+0.5*x ;
    % g=(double(gv).^2+double(gh).^2)<1000 ;

    
  case 'sqrs'
    [x,y]=meshgrid(1:256,1:256) ;
    f= xor(mod(fix(x/32),2),mod(fix(y/32),2))*100+x  ;
    gh = imfilter(f,fspecial('sobel') ,'replicate');
    gv = imfilter(f,fspecial('sobel')','replicate');
    g=-(double(gv).^2+double(gh).^2).^0.7 ;
    f=normimg(f) ;
    g=normimg(g) ;

    
  case 'lenae'
    f=single(imread('~/data/images/lena_edges.png')) ;
    g=single(imread('~/data/images/lena_smoothed.png')) ;
    [m,n] = size(f);
    [x,y] = meshgrid( 1:n, 1:m );
    g=g+0.25*y ;
    % g=(double(gv).^2+double(gh).^2)<1000 ;
    % h=imhist(uint8(f)) ;
  
    
  case 'text'
    addpath('~/work/SKHbook/codes/') ;
    cmpviapath('~/work/SKHbook/codes/') ;
    ImageDir='~/work/SKHbook/codes/15Texture/images/' ;

    t1 = normimg( imread([ImageDir 'D112.png']) );
    t2 = normimg( imread([ImageDir 'D17.png']) );
    t3 = normimg( imread([ImageDir 'D4.png']) );
    t4 = normimg( imread([ImageDir 'D95.png']) );
    t5 = normimg( imread([ImageDir 'D3.png']) );
    t6 = normimg( imread([ImageDir 'D101.png']) );
    t7 = normimg( imread([ImageDir 'D16.png']) );
    t8 = normimg( imread([ImageDir 'D110.png']) );
    [m,n] = size(t1);
    [x,y] = meshgrid( 1:n, 1:m );
    mask1 = ( ((x-0.75*n).^2 + (y-0.5*m).^2) < 20000 );
    mask2 = ( x>100 & x<300 & y>50 & y<250 );
    mask3 = ( x>100 & x<450 & y>350 & y<600) & not(mask1);
    mask  = 1 + mask1 + 2*mask2 + 3*mask3;
    
    f = single(t1.*(mask==1) + t2.*(mask==2) + t3.*(mask==3) + t4.*(mask==4));
    g = single(t5.*(mask==1) + t6.*(mask==2) + t7.*(mask==3) + t8.*(mask==4));
    
    
    
  otherwise
    error('unknown image type') ;
    
 end   
  
% f=single(f) ;
 
figure(1) ; clf ;
%imagesc(f,[0 255]) ; colormap(gray) ; colorbar
imagesc(f) ; colormap(gray) ; colorbar
figure(2) ; clf ;
%imagesc(g,[0 255]) ;  colormap(gray) ; colorbar
imagesc(g) ;  colormap(gray) ; colorbar
pause(0.01) ;

[g,t]=transfimg(g,tt) ;
[f,~]=transfimg(f,tt,-t) ;
t=2*t ; 


function f=normimg(f) ;
f=im2double(f) ;
f=f-mean(f(:)) ;
f=f/std(f(:))*50+128 ;