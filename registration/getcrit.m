function y=getcrit(f,g,critt) ;
% Evaluate a similarity criterion between two images of the sames size.
% critt can be 'ssd','cc' (correlation coefficient), 'mi', 'nbmi',
% 'textmi'

params=[] ;
params.nbsize=3 ;
params.eps=1e-100 ;
params.checks=50 ;


switch critt  
  case 'ssd'
    y=sum((f(:)-g(:)).^2) ;
    
  case 'cc'
    y=corrcoef(f(:),g(:)) ;
    y=y(1,2) ;
    
  case 'mi'
    y=mutualinfo(double(f(:)),double(g(:))) ;
    
  case 'nbmi'
    y=neighbmi(single(f),single(g),params) ;
    
  case 'textmi'
    maxlevel=2 ; sigma=20 ;
    fw=waveletsegdescr(f,maxlevel,sigma) ;
    gw=waveletsegdescr(g,maxlevel,sigma) ;
    k=size(fw,1) ; % length of the descriptor vector
    fw=reshape(fw,k,[]) ;
    gw=reshape(gw,k,[]) ;
    y=mutinf_klflann(fw,gw,params) ;
    
  otherwise
    error('criterion type unknown')
end
    