% show bias

n=1e4 ;
params=[] ;
params.nbsize=3 ;
params.eps=1e-50 ;
params.checks=150 ;

alphav=0.1:0.05:1 ;
na=length(alphav) ;
te=zeros(1,na) ;
biask=zeros(1,na) ;
biash=zeros(1,na) ;
nrep=10 ;
for i=1:na,
  alpha=alphav(i) ;
  hmi=0 ; kmi=0 ;
  for j=1:nrep ;
    x=random('normal',0,1,1,n) ;
    y=random('normal',0,1,1,n) ;
    z=alpha*y+(1-alpha)*x ;
    hmi=hmi+mutualinfo(x,z) ;
    kmi=kmi+mutinf_klflann(x,z,params) ;    
  end ;
  hmi=hmi/nrep ; kmi=kmi/nrep ;
  e=exp(1) ;
  tmi=-(log(2.0*pi*e*alpha)-0.5*log(2.0*pi*e*(2*alpha*alpha-2*alpha+1))-0.5*log(2.0*pi*e)) ;
  fprintf('alpha=%f true=%f hist=%f KL=%f\n',alpha,tmi,hmi,kmi) ;
  te(i)=tmi ;
  biash(i)=hmi-tmi ;
  biask(i)=kmi-tmi ;
end ;
plot(te,biash,'g-o',te,biask,'r-o','LineWidth',2) ;
legend('hist','KL','Location','SouthWest') ;
h1=xlabel('true MI') ; h2=ylabel('bias') ;
set(gca,'FontSize',12) ;
set(h1,'FontSize',12) ;
set(h2,'FontSize',12) ;

grid on ;
print('-depsc','results/mibias.eps') ;
