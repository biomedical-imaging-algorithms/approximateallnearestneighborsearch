/**
 *  @file klentropyestim.h
 *  @brief KL entropy estimator declaration.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 14.12.2005
 */

#ifndef KLENTROPYESTIM_H
  #define KLENTROPYESTIM_H

#include <math.h>
#include <assert.h>
#include "config.h"


/** @brief Euler-Mascheroni constant. */
double const EULER_GAMMA = 0.577215664901532;

// calculate factorial
double factorial(int n) {
  double res = 1.0 ; 
  assert (n>0) ;
  for (int i=2;i<=n;i++) res *= (double) i ;
  return res ;
}

// calculate a gamma function of (1+d/2)
double gamma(int d) {
  assert (d>=1) ;
  if (d%2==0) 
    return factorial(d/2) ;
  else {
    int n=(1+d)/2 ;
    return sqrt(M_PI)*(double)factorial(2*n)/(double)factorial(n)/
      pow(4.,n) ;
  }
}


/**
 *  @brief Entropy estimator functionoid, passed as evaluator parameter to
 *  #CBBFTree iteration functions.
 */
class FKLEntropyEstim {
  int sizeOfData; /**< @brief Size (number of points) of the data. */
  DimensionType dim; /**< @brief Dimensionality for which the entropy is estimated. */
  double constPart; /**< @brief Constant part of the estimator. */
  /**
   *  @brief Smallest acceptable distance.
   *
   *  Every distance smaller than epsilon (particularly zero, for multiple
   *  points) is considered epsilon. For multiple points a modification of
   *  the log dist estimator is used.
   *
   */
  double epsilon;
  double const dimLogEpsilon; /**< @brief Dimensionality by log epsilon. */
  double dimLogDist; /**< @brief Cummulates the log distances of NN points. */

public:

  FKLEntropyEstim(int inSizeOfData, DimensionType inDimensionality,
    double inEpsilon) :
  sizeOfData(inSizeOfData),
      dim(inDimensionality),
#if LNORM==0
      constPart(log(pow(2, dim) * (sizeOfData - 1)) + EULER_GAMMA),
#else
        constPart(log(pow(M_PI, 0.5*dim) * (sizeOfData - 1)/gamma(dim)) + 
                  EULER_GAMMA),
#endif
      epsilon(inEpsilon),
      dimLogEpsilon(dim*log(inEpsilon)),
      dimLogDist(0) {
    sizeOfData=0 ; 
    
  };

  void operator()(PointType inPoint, JKDataType multiplicity) {
    dimLogDist += dimLogEpsilon - log(multiplicity);
    sizeOfData+=multiplicity ;
  };

  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType dist) {
      // FIXME: seems to enhance the positive bias
      /*if(dist < epsilon)
        dimLogDist += dimLogEpsilon;
      else*/
#if LNORM==0
        dimLogDist += (dim * log(dist));
#else
        dimLogDist += 0.5 * (dim * log(dist));
#endif
    sizeOfData++ ;
  };

  double getEntropy() {
#if LNORM==0
    constPart=log(pow(2, dim) * (sizeOfData - 1)) + EULER_GAMMA ;
#else
    constPart=log(pow(M_PI, 0.5*dim) * (sizeOfData - 1)/gamma(dim)) + 
                   EULER_GAMMA ;
#endif
    //    cout << "getEntropy n=" << sizeOfData << " sumlog=" << dimLogDist << " const=" << constPart << "\n" ;
    return (dimLogDist / sizeOfData) + constPart;
  };

  void reset() {
    dimLogDist = 0; sizeOfData = 0 ;
  };
};

#endif
