/**
 *  @file bbftreenode.h
 *  @brief Node of the tree, declaration
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 16.01.2006
 */

#ifndef BBFTREENODE_H
  #define BBFTREENODE_H

#include "config.h"
#include "pointset.h"
#include "bbox.h"
#include <vector>
#include <queue>

class CBBFTreeNode;

/**
 *  @brief Type of an element of the node priority queue. It contains
 *  the node pointer, previous node (which pushed it) pointer and this
 *  node's distance value.
 */
typedef threeValues<CBBFTreeNode*, CBBFTreeNode*, JKDataType> QueueElementType;

/**
 *  @brief Functionoid providing the comparison < (the less operator) for
 *  #CBBFTreeNode instances.
 */
struct SNodeComp {
  bool operator()(
    const QueueElementType& n1,
    const QueueElementType& n2) {
    //      return (n1.third < n2.third);
      return (n1.third > n2.third);
  }
};

/**
 *  Priority queue type definition. It's a STL @c priority_queue with
 *  @c vector implementation and #SNodeComp as a @c less operator.
 */
typedef
  std::priority_queue<QueueElementType, std::vector<QueueElementType>,
    SNodeComp>
PQueueType;

/**
 *  @brief Node of the CBBFTree.
 *
 *  Holds his point set, the size of his whole subtree and pointers to parent
 *  and children.
 */
class CBBFTreeNode {

  /** @brief Set of points held by this node. */
  CPointSet* pointSet;

  /**
   *  @brief Loose bounding box - union of halfspaces defined by cuts of
   *  parent nodes.
   */
  SBBox looseBox;

  /** @brief Tight bounding box of all points in the set. */
  SBBox tightBox;

  /** @brief Dimension in which was this node cutted to obtain children. */
  DimensionType cutDimension;

  /** @brief Cutting value. */
  JKDataType cutValue;

  /** @brief Number of points in this node's subtree. */
  int size;

  /** @brief Leaf flag. It's true if this is a leaf. */
  bool leaf;

  /** @brief Pointer to the left son node. */
  CBBFTreeNode* leftSon;
  /** @brief Pointer to the right son node. */
  CBBFTreeNode* rightSon;
  /** @brief Pointer to the parent node. */
  CBBFTreeNode* parent;

  /** @brief Helper set for the update/rebuild operation. */
  CPointSet leftSet;
  /** @brief Helper set for the update/rebuild operation. */
  CPointSet rightSet;

  /**
   *  @brief Helper pointer to the pointSet or rightSet during the
   *  update/rebuild operation.
   */
  CPointSet* actualSet;

  /**
   *  @brief Private constructor used by the cutNode method.
   *
   *  @arg @c inDimRange Dimension range to work on.
   *  @arg @c inParent Parent node.
   *  @arg @c inPointSet Point set of this node.
   *  @arg @c inCutDim Cutting dimension.
   *  @arg @c inCutVal Cutting value.
   *  @arg @c True if it's the left child.
   */
  CBBFTreeNode(DimRangeType inDimRange, CBBFTreeNode* inParent,
    CPointSet* inPointSet, DimensionType inCutDim, JKDataType inCutVal,
    bool left);

  /**
   *  @brief Returns true if input point is in #cutDimension left from
   *  (lower than) #cutValue.
   */
  bool goesToLeft(const PointType inPoint);

    /**
   *  @brief Appends all points of this node's point set to parent's one,
   *  leaving this one empty.
   */
  void releasePoints();


  /**
   *  @brief Pushes given tree node to the queue if it's distance value
   *  is better then current minimal distance.
   *
   *  @arg @c inNode Candidate for queue pushing.
   *  @arg @c inQuery Query point.
   *  @arg @c inPrevNode Node, which tries to push this one.
   *  @arg @c minDist Current minimal distance.
   *  @arg @c inPQueue Priority queue reference.
   *  @arg @c inDimRange Dimension range to work on.
   */
  void pushIfBetter(CBBFTreeNode* inNode, CPointSet::iterator inQuery,
    CBBFTreeNode* inPrevNode, JKDataType minDist, PQueueType& inPQueue,
                    DimRangeType inDimRange, bool useTbb);

  /**
   *  @brief Changes the NN point and distance if found better.
   *
   *  @arg @c inPointNN NN point and distance found in the original leaf set.
   *  @arg @c inQuery The query point for which to find the NN.
   *  @arg @c inDimRange Dimension range to work on.
   *  @arg @c inMaxVisit Maximum number of points to test. Optional
   *  parameter of the tree.
   *  @arg @c exact True if exact nearest neighbor are to be iterated.
   *  It's implemented as ignoring the inMaxVisit parameter.
   *
   *  @return Number of compared points;
   */
  int findLowerDistance(
    twoValues<CPointSet::iterator, JKDataType>& inPointNN,
    const CPointSet::iterator& inQuery, DimRangeType inDimRange, int inMaxVisit,
    bool exact, bool pushroot, bool useTbb);

public:

  /**
   *  @brief Constructor for creating of the root node of the tree.
   *
   *  @arg @c inData Pointer to the input data.
   *  @arg @c inDimOfData Dimensionality of the input data.
   *  @arg @c inSizeOfData Size (number of points) of the input data.
   *  @arg @c inDimRange Dimension range to work on.
   */
  CBBFTreeNode(const JKDataType* inData, DimensionType inDimOfData,
  int inSizeOfData, DimRangeType inDimRange);

  /** @brief Destructor. Deallocates his children and point set. */
  ~CBBFTreeNode();

  /**
   *  @brief Creates the tight bounding box for the points in the set.
   *
   *  @arg @c inDimRange Dimension range to work on.
   */
  void createTightBox(DimRangeType inDimRange);

  /**
   *  @brief Cuts this node, creating two children nodes.
   *
   *  @arg @c inDimRange Dimension range to work on.
   *  @arg @c inLeafSize Maximum size of a leaf. It's one of the optional
   *  parameters of the tree.
   *
   *  @return True if the two children were created, false if this has became
   *  a leaf.
   */
  bool cutNode(DimRangeType inDimRange, int inLeafSize);

  /**
   *  @brief Updates the node. Used by the CBBFTree#Update() method.
   *
   *  If it's a leaf, this method checks the points if they are in this
   *  point set's loose bounding box. Outfalling points are moved to parent's
   *  set (pointSet or rightSet, according to which one is actualSet pointing)
   *  If it's a non-leaf, updateNode() is called for both children and this
   *  node's pointSet is updated afterwards.
   *
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return Number of moved points in this subtree.
   */
  int updateNode(DimRangeType inDimRange);

  /**
   *  @brief Rebuilds this node's subtree if it's unbalanced.
   *
   *  This method is called as a second pass through the tree after update.
   *  Checks for unbalanced subtrees and rebuilds them.
   *
   *  @arg @c inDimRange Dimension range to work on.
   *  @arg @c inLeafSize Maximum size of a leaf. This is one of the optional
   *  parameters of the tree.
   *  @arg @c inDelta Coefficient of the allowed unbalance of a subtree. For
   *  details see CBBFTree#SBBFTreeParams.
   *
   *  @return Number of points rebuild in this node's subtree.
   */
  int rebuildNode(DimRangeType inDimRange, int inLeafSize, float inDelta);

  /**
   *  @brief Iterates through all points in the node's subtree and applies
   *  the evaluator for found NN pairs.
   *
   *  It's a recursive function, which searches the tree. For a leaf it
   *  iterates through all points. Finds NN point in it's set
   *  and tries to find closer point using the priority queue BBF approach
   *  implemented in findLowerDistance() method for each one.
   *
   *  @arg @c eval Evaluator functionoid which is called for each found pair,
   *  for details see CBBFTree#IterateAllApproxNN() method.
   *  @arg @c inDimRange Dimension range to work on.
   *  @arg @c inMaxVisit Maximal number of points to visit by the NN search.
   *  Optional parameter of the tree.
   *  @arg @c exact If true, the search ignores the maxVisit parameter and will
   *  find the exact NN.
   */
  template <typename FProcess>
  int processNode(FProcess& eval, DimRangeType inDimRange, int inMaxVisit,
                  bool exact,bool useTbb) {
      if(leaf) {
        //        cerr << "processNode, leaf: " << tightBox << "\n" ;
        int number = 0;
        if(size > 0) {
          // if it's a leaf node, process it's points
          // degenerated tight box means only one value in set
          CPointSet::iterator setEnd = pointSet->end();
          if(!tightBox.applyFlag && size > 1) {
            for(CPointSet::iterator iter = pointSet->begin();
              iter != setEnd; ++iter) {
                eval(*iter, pointSet->getSize());
            }
          }
          else {
            //            cerr << "processNode: " << tightBox << "\n" ;
            for(CPointSet::iterator iter = pointSet->begin();
              iter != setEnd; ++iter) {
                twoValues<CPointSet::iterator, JKDataType>
                  pointNN = pointSet->findNN(iter, inDimRange);
                number += size;
                // if the point is multiple, evaluate the multiplicity
                if(pointNN.first == pointSet->end() && pointNN.second==0.) {
                  eval(*iter, pointNN.second);
                } else {
                  // start the priority queue optimization search for lower
                  // distance
                  number += findLowerDistance(pointNN, iter, inDimRange,
                                              inMaxVisit - size, exact,false,useTbb);
                  // evaluate on the found NN pair
                  eval(*iter, *(pointNN.first), pointNN.second);
                }
            }
          }
        }
        return number;
      } else {
        // if it's a non-leaf node, process it's children
        int leftNumber = leftSon->processNode<FProcess>(eval,
                                                        inDimRange, inMaxVisit, exact,useTbb);
        int rightNumber = rightSon->processNode<FProcess>(eval,
          inDimRange, inMaxVisit, exact,useTbb);
        return (leftNumber + rightNumber);
      }
  };

  /** approxNN is called from bbftree.h:findApproxNN to find one approximate
      nearest neighbor in the subtree of the current tree.
      It returns the number of visited points. */
  template <typename FProcess>
    int approxNN(FProcess& eval, DimRangeType inDimRange, int inMaxVisit,CPointSet::iterator query,bool useTbb) {
    int number ;
    twoValues<CPointSet::iterator, JKDataType>
      pointNN = twoValues<CPointSet::iterator, JKDataType>(
          pointSet->begin(),INFINITY) ;
    number = findLowerDistance(pointNN,query,inDimRange,inMaxVisit,false,true,useTbb) ;
    // call the evaluator
    eval(*query, *(pointNN.first), pointNN.second);
    return number ;
  }

  /** @brief Returns the size of this node's subtree. */
  int getSize();

  /** @brief Returns true if this node is a leaf. */
  bool isLeaf();

  /** @brief Returns this node's point set reference. */
  CPointSet& getPointSet();

  /** @brief Returns the left son of this node. */
  CBBFTreeNode* getLeftSon();

  /** @brief Returns the right son of this node. */
  CBBFTreeNode* getRightSon();

  /* show the contents of this subtree */
  void show(ostream& out,int level) const ;

#if 0
  // for debugging - verify that none of the nodes in the subtree contains
  // points with NaN coordinates.
  void tree_not_none();
#endif

};

ostream& operator<< (ostream& out, const CBBFTreeNode& p) ;


#endif // ifndef BBFTREENODE_H
