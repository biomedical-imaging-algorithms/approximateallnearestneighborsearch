/**
 *  @file experiments.cpp
 *  @brief Experiments with the IncrEst implementation.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 27.2.2006
 */

#include <time.h>
#include <iostream>
#include <vector>
#include <string>
#include <newran.h>
#include "config.h"
#include "klentropyestim.h"
#include "bbftree.h"

/** @brief Empty evaluator, for iteration the data only. */
class FEmpty {
public:
  void operator()(PointType inPoint, JKDataType multiplicity) {
  };
  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
  };
};

/**
 *  @brief Determining the optimal leaf size.
 */
void experiment1() {

  // parameters
  DimensionType ds[] = {1,2,3,5,10,15,20};
  int n = 100000;
  int ls[] = {5,10,20,30,40,50};
  int seed = 4457894;

  // empty evaluator
  FEmpty* empty = new FEmpty();

  // for dimensions
  for(int d = 0; d < 7; ++d) {

    // data preparation
    int num = n*ds[d];
    JKDataType* data = new JKDataType[num];
    // Data generation
    srand(seed);
    std::vector<Random*> rv;
    for(int i = 0; i < ds[d]; ++i) {
      Normal* randV = new Normal();
      rv.push_back(randV);
      randV->Set(rand()/(double)RAND_MAX);
    }
    for (int i = 0; i < n; i++) {
      for(int j=0;j<ds[d];j++) {
        data[ds[d]*i + j] = rv[j]->Next();
      }
    }

    // for leafsizes
    for(int l = 0; l < 6; ++l) {
      //Build the tree
      std::cout << ds[d] << "\t" << ls[l];
      clock_t start = clock();
      CBBFTree* testTree = new CBBFTree(data, ds[d], n, ls[l], 0, ds[d] - 1,
        0.1, (ds[d] <= 2) ? 100 : 1000);
      testTree->Build();
      clock_t end = clock();
      std::cout << "\t" << double(end - start)/double(CLOCKS_PER_SEC);
      // Iterate
      start = clock();
      testTree->IterateAllApproxNN(*empty);
      end = clock();
      std::cout << "\t" << double(end - start)/double(CLOCKS_PER_SEC);
      std::cout << std::endl;
      delete testTree;
    }

    // deleting the data
    for(int i = 0; i < ds[d]; ++i) {
      delete rv[i];
    }
    delete[] data;
  }

  delete empty;
};

/**
 *  @brief Timings wrt n.
 */
void experiment2() {

  // parameters
  DimensionType ds[] = {1,10};
  int ns[] = {100,1000,10000,100000,1000000};
  int seed = 113241;

  // empty evaluator
  FEmpty* empty = new FEmpty();

  // for dimensions
  for(int d = 0; d < 2; ++d) {
    // for data sizes
    for(int n = 0; n < 5; ++n) {

      // data preparation
      int num = ns[n]*ds[d];
      JKDataType* data = new JKDataType[num];
      // Data generation
      srand(seed);
      std::vector<Random*> rv;

      for(int i = 0; i < ds[d]; ++i) {
        Normal* randV = new Normal();
        rv.push_back(randV);
        randV->Set(rand()/(double)RAND_MAX);
      }
      for (int i = 0; i < ns[n]; i++) {
        for(int j = 0; j < ds[d]; j++) {
          data[ds[d]*i + j] = rv[j]->Next();
        }
      }

      //Build the tree
      std::cout << ds[d] << "\t" << ns[n];
      clock_t start = clock();
      CBBFTree* testTree = new CBBFTree(data, ds[d], ns[n], 2*ds[d] + 9,
        0, ds[d] - 1, 0.1, (ds[d] <= 2) ? 100 : 1000);
      testTree->Build();
      clock_t end = clock();
      std::cout << "\t" << double(end - start)/double(CLOCKS_PER_SEC);
      // Iterate
      start = clock();
      testTree->IterateAllApproxNN(*empty);
      end = clock();
      std::cout << "\t" << double(end - start)/double(CLOCKS_PER_SEC);
      start = clock();
      testTree->IterateAllExactNN(*empty);
      end = clock();
      std::cout << "\t" << double(end - start)/double(CLOCKS_PER_SEC);
      std::cout << std::endl;

      // deleting the data
      delete testTree;
      for(int i = 0; i < ds[d]; ++i) {
        delete rv[i];
      }
      delete[] data;
    }
  }

  delete empty;
};

/**
 *  @brief Choice of delta.
 */
void experiment3() {

  // parameters
  float delta[] = {0.0,0.1,0.2,0.3};
  float hs[] = {0.01, 0.1, 1.0};
  DimensionType d = 5;
  int n = 100000;
  int l = 25;
  int numOfRuns = 10;
  int seed[] = {113241,54654,1321,46789,7987,321321,4445,215154,554798,777488};

  int num = n*d;
  JKDataType* data = new JKDataType[num];
  FEmpty* empty = new FEmpty();

  for(int deltaIndex = 0; deltaIndex < 4; ++deltaIndex) {
    for(int hsIndex = 0; hsIndex < 3; ++hsIndex) {
      double sumUpdate = 0;
      double sumIter = 0;
      double sumIter2 = 0;
      for(int seedIndex = 0; seedIndex < numOfRuns; ++seedIndex) {

        // Data generation
        srand(seed[seedIndex]);
        std::vector<Random*> rv;
        std::vector<Random*> modify;
        for(int i = 0; i < d; ++i) {
          Normal* randV = new Normal();
          Normal* modifyRV = new Normal();
          rv.push_back(randV);
          modify.push_back(modifyRV);
          randV->Set(rand()/(double)RAND_MAX);
          modifyRV->Set(rand()/(double)RAND_MAX);
        }
        for (int i = 0; i < n; i++) {
          for(int j = 0; j < d; j++) {
            data[d*i + j] = rv[j]->Next();
          }
        }

        //Build the tree and modify the data
        CBBFTree* testTree = new CBBFTree(data, d, n, l,
          0, d - 1, delta[deltaIndex], 1000);
        testTree->Build();
        for (int i = 0; i < n; i++) {
          for(int j = 0; j < d; j++) {
            data[d*i + j] += (hs[hsIndex] * (modify[j]->Next()));
          }
        }

        // update
        clock_t start = clock();
        testTree->Update();
        clock_t end = clock();
        sumUpdate += double(end - start)/double(CLOCKS_PER_SEC);

        // iterate
        start = clock();
        testTree->IterateAllApproxNN(*empty);
        end = clock();
        sumIter += double(end - start)/double(CLOCKS_PER_SEC);

        // rebuild
        delete testTree;
        testTree = new CBBFTree(data, d, n, l,
          0, d - 1, delta[deltaIndex], 1000);
        testTree->Build();

        // iterate2
        start = clock();
        testTree->IterateAllApproxNN(*empty);
        end = clock();
        sumIter2 += double(end - start)/double(CLOCKS_PER_SEC);

        delete testTree;
        for(int i = 0; i < d; ++i) {
          delete rv[i];
          delete modify[i];
        }
      }

      std::cout << hs[hsIndex] << "\t" << delta[deltaIndex] << "\t";
      std::cout << sumUpdate/numOfRuns << "\t";
      std::cout << sumIter/numOfRuns << "\t";
      std::cout << sumIter2/numOfRuns << std::endl;
    }
  }

  delete empty;
  delete[] data;
};

/** @brief Entropy estimation error growth for growing quantization. */
void experiment10() {

  // parameters
  // quantization steps
  float qstep[] = {0, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1};
  // dimensionality
  DimensionType ds[] = {1, 3, 10, 25};
  int ns[] = {10000, 100000, 1000000};
  int seed[] = {32154, 1, 1245, 15478, 7879, 44, 69, 5687, 789, 77454};
  int numOfRuns = 10;

  // for data sizes
  for(int n = 0; n < 3; ++n) {
    // for dimensionalities
    for(int d = 0; d < 4; ++d) {
      // for quantizations
      for(int q = 0; q < 7; ++q) {
        // 10 times to obtain average values
        double sumBuildTime = 0;
        double sumApproxIterTime = 0;
        double sumApproxIterEstimError = 0;
        double sumExactIterTime = 0;
        double sumExactIterEstimError = 0;
        double trueEntropy = ds[d]*(0.5 + 0.5*log(2*3.14159265358979323846));
        for(int seedIndex = 0; seedIndex < numOfRuns; ++seedIndex) {
          // data prepare
          int num = ns[n]*ds[d];
          JKDataType* data = new JKDataType[num];
          std::vector<Random*> rv;
          srand(seed[seedIndex]);
          for(int i = 0; i < ds[d]; ++i) {
            Normal* randV = new Normal();
            rv.push_back(randV);
            double ranseed = rand()/(double)RAND_MAX;
            if (ranseed == 1)
              ranseed = 0.999999;
            randV->Set(ranseed);
          }
          if(qstep[q] == 0) {
            for (int i = 0; i < ns[n]; i++) {
              for(int j = 0; j < ds[d]; j++) {
                data[ds[d]*i + j] = rv[j]->Next();
              }
            }
          } else {
            for (int i = 0; i < ns[n]; i++) {
              for(int j = 0; j < ds[d]; j++) {
                int trunc = static_cast<int>(rv[j]->Next() / qstep[q]);
                data[ds[d]*i + j] = trunc * qstep[q];
              }
            }
          }

          // entropy estimator
          FKLEntropyEstim entropy(ns[n], ds[d], 0.001);

          // estimation
          clock_t start = clock();
          CBBFTree* tree = new CBBFTree(data, ds[d], ns[n]);
          tree->Build();
          clock_t end = clock();
          sumBuildTime += double(end - start)/double(CLOCKS_PER_SEC);

          start = clock();
          tree->IterateAllApproxNN(entropy);
          end = clock();
          sumApproxIterTime += double(end - start)/double(CLOCKS_PER_SEC);
          sumApproxIterEstimError += entropy.getEntropy() - trueEntropy;

          entropy.reset();

          start = clock();
          tree->IterateAllExactNN(entropy);
          end = clock();
          sumExactIterTime += double(end - start)/double(CLOCKS_PER_SEC);
          sumExactIterEstimError += entropy.getEntropy() - trueEntropy;

          // deleting the data
          for(int i = 0; i < ds[d]; ++i) {
            delete rv[i];
          }
          delete tree;
          delete[] data;
        }

        // output
        std::cout << ns[n] << "\t" << ds[d] << "\t" << qstep[q] << "\t";
        std::cout << trueEntropy << "\t";
        std::cout << sumBuildTime/numOfRuns << "\t";
        std::cout << sumApproxIterTime/numOfRuns << "\t";
        std::cout << sumApproxIterEstimError/numOfRuns << "\t";
        std::cout << sumExactIterTime/numOfRuns << "\t";
        std::cout << sumExactIterEstimError/numOfRuns << std::endl;
      }
    }
  }
};

/**
 *  @brief Entropy estimation error and speed for 1 000 000 normally
 *  distributed points .
 */
void experiment11() {

  // parameters
  // dimensionality
  DimensionType ds[] = {1, 3, 5, 10};
  int n = 1000000;
  int seed = 32154;

  // for dimensionalities
  for(int d = 0; d < 5; ++d) {
    // 10 times to obtain average values
    double buildTime = 0;
    double approxIterTime = 0;
    double approxIterEstimError = 0;
    double exactIterTime = 0;
    double exactIterEstimError = 0;
    double trueEntropy = ds[d]*(0.5 + 0.5*log(2*3.14159265358979323846));

    // data prepare
    int num = n*ds[d];
    JKDataType* data = new JKDataType[num];
    std::vector<Random*> rv;
    srand(seed);
    for(int i = 0; i < ds[d]; ++i) {
      Normal* randV = new Normal();
      rv.push_back(randV);
      double ranseed = rand()/(double)RAND_MAX;
      if (ranseed == 1)
        ranseed = 0.999999;
      randV->Set(ranseed);
    }
    for (int i = 0; i < n; i++) {
      for(int j = 0; j < ds[d]; j++) {
        data[ds[d]*i + j] = rv[j]->Next();
      }
    }

    // entropy estimator
    FKLEntropyEstim entropy(n, ds[d], 0.001);

    // estimation
    clock_t start = clock();
    CBBFTree* tree = new CBBFTree(data, ds[d], n);
    tree->Build();
    clock_t end = clock();
    buildTime = double(end - start)/double(CLOCKS_PER_SEC);

    start = clock();
    tree->IterateAllApproxNN(entropy);
    end = clock();
    approxIterTime = double(end - start)/double(CLOCKS_PER_SEC);
    approxIterEstimError = entropy.getEntropy() - trueEntropy;

    entropy.reset();

    start = clock();
    tree->IterateAllExactNN(entropy);
    end = clock();
    exactIterTime = double(end - start)/double(CLOCKS_PER_SEC);
    exactIterEstimError = entropy.getEntropy() - trueEntropy;

    // deleting the data
    for(int i = 0; i < ds[d]; ++i) {
      delete rv[i];
    }
    delete tree;
    delete[] data;

    // output
    std::cout << ds[d] << "\t";
    std::cout << trueEntropy << "\t";
    std::cout << buildTime << "\t";
    std::cout << approxIterTime << "\t";
    std::cout << approxIterEstimError << "\t";
    std::cout << exactIterTime << "\t";
    std::cout << exactIterEstimError << std::endl;
  }
};

int main(int argc, char **argv) {
  if(argc < 2) {
    std::cout << "Give the number of experiment (or more)";
    std::cout << " as arguments!" << std::endl;
    return 0;
  }
  int count = 1;
  while(argc != count) {
    if(string(argv[count]) == string("1")) {
      experiment1();
    } else if(string(argv[count]) == string("2")) {
      experiment2();
    } else if(string(argv[count]) == string("3")) {
      experiment3();
    } else if(string(argv[count]) == string("10")) {
      experiment10();
    } else if(string(argv[count]) == string("11")) {
      experiment11();
    } else {
      std::cout << "No experiment number " << string(argv[count]);
      std::cout << " implemented!" << std::endl;
    }
    ++count;
  }
};
