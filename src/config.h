/**
 *  @file config.h
 *  @brief Configuration header for IncrEst project.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko, changed by Jan Kybic
 *  @date 14.12.2005
 */

#ifndef CONFIG_H
  #define CONFIG_H


#define INCOMPLETEDISTANCE 0


  // LNORM==0 for the l_infty norm, LNORM==2 for the l_2 norm
  #define LNORM 2


#if LNORM!=0 & LNORM!=2
  #error LNORM must be 0 or 2
#endif

#include <limits>

/** @brief Data values type. */
typedef double JKDataType;

/** @brief Dimension indexing type. Should be some size of integer. */
typedef short DimensionType;

/** @brief Data type infinity. */
JKDataType const INFINITY = std::numeric_limits<float>::infinity();
JKDataType const NEGATIVE_INFINITY = -INFINITY;

/** @brief Utility structure for holding two objects of arbitrary types. */
template<typename valType1, typename valType2 = valType1>
struct twoValues {
  valType1 first;
  valType2 second;

  twoValues() {};

  twoValues(const valType1& inVal1,const valType2& inVal2) : first(inVal1),
    second(inVal2) {
  };
};

/** @brief Utility structure for holding three objects of arbitrary types. */
template<typename valType1, typename valType2 = valType1,
  typename valType3 = valType2>
struct threeValues {
  valType1 first;
  valType2 second;
  valType3 third;

  threeValues() {};

  threeValues(const valType1& inVal1, const valType2& inVal2,
    const valType3& inVal3) : first(inVal1), second(inVal2), third(inVal3) {
  };
};

/** @brief Data point type. Currently defined as an array of #JKDataType. */
typedef const JKDataType* PointType;

/** @brief Dimension range type. */
typedef twoValues<DimensionType> DimRangeType;

#endif // ifndef CONFIG_H
