/**
 *  @file increst.h
 *  @brief Main include file for the IncrEst project.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 22.03.2006
 */

#include "bbftree.h"
#include "klentropyestim.h"
