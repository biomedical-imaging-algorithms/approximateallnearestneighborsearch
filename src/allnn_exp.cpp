/*
   Experiments for the article on all nearest neighbor search

   Jan Kybic

*/

#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>
#include <limits>

#include <time.h>
#define use_namespace
#include <newran.h>
#include <string.h>

#include "config.h"
#include "klentropyestim.h"
#include "bbftree.h"
#include "bbox.h"

#include "ANN.h"
#include "flann/flann.h"
// #include "onbnn.h"
// #include "lshkit-onbnn/mplsh-custom.h"
#include "ccData.hpp"
#include "ccDistance.hpp"
#include "ccLsh.hpp"


#include "timeit.h"

// Constants

const int STRING_LEN = 500 ;
const double eepsilon = 1e-50 ;

// Global variables

int dim=1 ;
int npoints=0 ; // number of data points
int leaf_size = 16 ; 
int max_visited = std::numeric_limits<int>::max() ;
int verbosity = 3 ;
bool useTbb = true ;
float delta = 0. ;
int nlshfuncs = 5 ;
int nlshtables = 1 ;
JKDataType* data = NULL ;
CBBFTree* tree = NULL ;
ANNbd_tree*	anntree = NULL ;
ANNpointArray pa = NULL ; // point array for ANN
double ann_epsilon = 0. ; // epsilon (error bound) for ANN

// for FLANN
int flann_checks = 128 ; 
int flann_trees = 4 ;
flann_index_t flann_index = NULL ; // index for FLANN
struct FLANNParameters flann_params =  DEFAULT_FLANN_PARAMETERS ;

// for LSH / ONBNN, taken from example1-binary-classification.cpp
//typedef vector<double> Keypoint;
// typedef vector<Keypoint> KeypointSet;
//typedef mplsh_custom::DoubleNnIndex Index;
//KeypointSet* lshdata = NULL ; // points for LSH
//Index* lshindex = NULL ;      // index for LSH

// for LSH from Caltech Large Scale Image Search
Lsh* lshindex = NULL ;
Data<double> *lshdata = NULL ;
double lshW = 0.25 ;

void readPts(string filename, int dim, int &npoints,JKDataType*& data) ;


/** @brief Empty evaluator, for iteration the data only. */
class FEmpty {
public:
  void operator()(PointType inPoint, JKDataType multiplicity) {
  };
  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
  };
};




void printPoint(PointType p) {
    cout << "p" << (p-data)/dim << "=(" ;
    for (int i=0;i<dim;i++)  {
      cout << *(p+i) ;
      if (i<dim-1) cout << ", " ;
    }
    cout << ")" ;
  }

/** @brief Show evaluator, for debugging. */
class FShow {
  PointType start ;
  int dim ; 


public:
  FShow(PointType inStart,int inDim) { start=inStart ; dim=inDim ; } ;

  void operator()(PointType inPoint, JKDataType multiplicity) {
    cout << "FShow: " ;
    printPoint(inPoint) ;
    cout << " multiplicity " << multiplicity  << "\n" ;
  };
  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
    cout << "FShow: " ;
    printPoint(inPoint1) ;
    cout << " " ;
    printPoint(inPoint2) ;
    cout << " dist " << distance << "\n" ;
  };
};




// brute force NN search, returns pointer to a NN and a distance
twoValues<JKDataType *,JKDataType> bruteNNsearch(JKDataType* data,int npoints, int dim, const JKDataType* query)
{
  JKDataType dist = INFINITY ;
  JKDataType *best=NULL ;
  DimRangeType dimRange = twoValues<DimensionType>(0,dim-1) ;

  for (int i=0; i<npoints; i++)
    { JKDataType *p=data+i*dim ;
      if (p!=query) {
        JKDataType d = SBBox::distance(query,p,dimRange) ;
        if (d < dist) {
          best = p ;
          dist = d ;
        }
      }
    }
  return twoValues<JKDataType *,JKDataType>(best,dist) ;
}

/** @brief Check evaluator, for debugging */
class FCheck {

protected:
  JKDataType *data ;
  int dim,npoints ;

  FCheck() {} ;

public:
  

  FCheck(JKDataType *inData, int inDim, int inNpoints) { 
    data=inData ; dim=inDim ; npoints = inNpoints ; } ;


  void operator()(PointType inPoint, JKDataType multiplicity) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint) ;
      if (best.second==0.) {
#if 0
        cout << "FCheck: " ;
        printPoint(inPoint) ;
        cout << " multiplicity " << multiplicity  << "\n" ;
#endif
      } else {
        cout << "FCheck: " ;
        printPoint(inPoint) ;
        cout << " multiplicity " << multiplicity  
             << " true " ; 
        printPoint(best.first) ;
        cout << " dist " << best.second << "\n" ;
      }

  };
  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint1) ;
    if (best.first==inPoint2 && fabs(best.second-distance)<1e-5) {
#if 0
        cout << "FCheck: " ;
        printPoint(inPoint1) ;
        cout << " " ;
        printPoint(inPoint2) ;
        cout << " dist " << distance << " brdist " << best.second <<  "\n" ;
#endif
      } else {
        cout << "FCheck: " ;
        printPoint(inPoint1) ;
        cout << " " ;
        printPoint(inPoint2) ;
        cout << " dist " << distance 
        << " true " ; 
        printPoint(best.first) ;
        cout << " dist " << best.second << "\n" ;
      }
  };
} ;

class FPtrue  {

protected:

   JKDataType *data ;
   int dim,npoints ;
   int ntot,ncorrect ; // counters for the total number of times we are called and the number of correct ones

   FPtrue() {} ;

 public:
  
   FPtrue(JKDataType *inData, int inDim, int inNpoints) { 
     data=inData ; dim=inDim ; npoints = inNpoints ; 
     ntot=0 ; ncorrect=0 ; } ;

  void operator()(PointType inPoint, JKDataType multiplicity) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint) ;
    // multiple points are counted as one since we do not know for sure if [multiplicity] is correct
    ntot++ ; 
    if (best.second==0.) ncorrect++ ;
  }


  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint1) ;
    ntot++ ;
    if (best.first==inPoint2 && fabs(best.second-distance)<1e-5) ncorrect++ ;
  }

  double get_ptrue() {
    return ((double)ncorrect / (double)ntot) ;
  }
};

 class FRelerr  {

protected:

  JKDataType *data ;
  int dim,npoints ;
  int ntot ;      // counters for the total number of times we are called 
  double sumerr ; // sum of relative errors

  FRelerr() {} ;

public:
  
  FRelerr(JKDataType *inData, int inDim, int inNpoints) { 
    data=inData ; dim=inDim ; npoints = inNpoints ; 
    ntot=0 ; sumerr=0. ; } ;

  void operator()(PointType inPoint, JKDataType multiplicity) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint) ;
    // relative error does not make sense for multiple points, so we just
    // test for errors
    if (best.second!=0.) sumerr=INFINITY ;
  }


  void operator()(PointType inPoint1, PointType inPoint2,
    JKDataType distance) {
    twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,inPoint1) ;
    ntot++ ;
    sumerr+= ( distance /  best.second ) - 1. ; ;
  }

  double get_relerr() {
    return (sumerr / (double)ntot) ;
  }

 };



 


class FAnnCheck : public FCheck {

public:
  FAnnCheck(JKDataType *inData, int inDim, int inNpoints) {
    data=inData ; dim=inDim ; npoints = inNpoints ; } ;

  void do_check(ANNbd_tree*	anntree) {
    ANNidx apx_nn_idx[1] ;
    ANNdist apx_dists[1] ;
    annMaxPtsVisit(max_visited) ;
    for (int i=0;i<npoints;i++) {
        anntree->annkPriSearch(pa[i],1,apx_nn_idx,apx_dists,ann_epsilon) ;
        (*this)(data+i*dim,data+apx_nn_idx[0]*dim,apx_dists[0]) ;
      } 
  } 
}  ;


// brute force all-NN search
template <typename T> void bruteAllNNsearch(
  T& evaluator,JKDataType* data,int npoints, int dim)
{ JKDataType *best,*query,dist ;
  twoValues<JKDataType *,JKDataType> bestdist ;
  for (int i=0; i<npoints; i++) {
    query=data+i*dim ;
    bestdist=bruteNNsearch(data,npoints,dim,query) ;
    best=bestdist.first ;
    dist=bestdist.second ;
    evaluator(query,best,dist) ;
  }
} ;

// BBF NNN (N-times NN) search
template <typename T> void repeatedNNsearch(
    T& evaluator,JKDataType* data,int npoints, int dim,CBBFTree *tree)
{ JKDataType *query ;
  DimRangeType dimRange = twoValues<DimensionType>(0,dim-1) ;

  for (int i=0; i<npoints; i++) {
    query=data+i*dim ;
    // cerr << "query=" << query << " " << "data=" << data << "\n" ;
    tree->findApproxNN(evaluator,dimRange,query) ;
  }
}



// The following classes are used for timings

class BuildCBBFTree {
        const JKDataType* data ;
        DimensionType dimOfData ;
        int sizeOfData,leafSize ;
        DimensionType dmin,dmax  ;
        float delta ;
        int maxVisit ;
        CBBFTree *tree ;
        bool useTbb ;

      public:
        BuildCBBFTree(const JKDataType* inData,DimensionType inDimOfData,
  int inSizeOfData, int inLeafSize, DimensionType inDmin, DimensionType inDmax,
                      float inDelta, int inMaxVisit, bool inUseTbb) :
          data(inData), dimOfData(inDimOfData), sizeOfData(inSizeOfData), 
          leafSize(inLeafSize), dmin(inDmin), dmax(inDmax),
          delta(inDelta), maxVisit(inMaxVisit), useTbb(inUseTbb) { tree=NULL ; } ;

        void op() {
          if (tree) delete tree ;
          tree=new CBBFTree(data,dimOfData,sizeOfData,leafSize, dmin, dmax,
                            delta, maxVisit,useTbb) ;
          tree->Build() ;
        }
       
        CBBFTree *result() { return tree ; }
      } ; // end of class BuildCBBFTree



class VnuckoAllNNSearch {
  CBBFTree *tree ;
  FEmpty empty ;

public:
  VnuckoAllNNSearch(CBBFTree *inTree) : tree(inTree) { } ;

  void op() {
      tree->IterateAllApproxNN(empty);
  } ;
} ; // end of class VnuckoAllNNSearch



class VnuckoEntropy {
  CBBFTree *tree ;
  FKLEntropyEstim entropy ;

public:
  VnuckoEntropy(CBBFTree *inTree) : tree(inTree),
            entropy(npoints,dim,eepsilon) { } ;

  void op() {
    entropy.reset() ;
    // entropy(data,data,1.0) ;
    tree->IterateAllApproxNN(entropy);
  } ;

  double get_entropy() { return entropy.getEntropy() ; } ;
} ; // end of class VnuckoEntropy

class VnuckoNNNEntropy {
  CBBFTree *tree ;
  FKLEntropyEstim entropy ;

public:
  VnuckoNNNEntropy(CBBFTree *inTree) : tree(inTree),
            entropy(npoints,dim,eepsilon) { } ;

  void op() {
    entropy.reset() ;
    // entropy(data,data,1.0) ;
    repeatedNNsearch(entropy,data,npoints,dim,tree);
    // tree->IterateAllApproxNN(entropy);
  } ;

  double get_entropy() { return entropy.getEntropy() ; } ;
} ; // end of class VnuckoNNNEntropy



class VnuckoNNNSearch {
  FEmpty empty ;

public:
  VnuckoNNNSearch()  { } ;
  void op() {
    repeatedNNsearch(empty,data,npoints,dim,tree);
  } ;
} ; // end of class VnuckoNNNSearch


class BruteAllNNSearch {
  JKDataType *data ;
  int npoints, dim ; 
  FEmpty empty ;

public:
  BruteAllNNSearch(JKDataType *inData, int inNpoints, int inDim) :
    data(inData), npoints(inNpoints), dim(inDim) {  } ;

  void op() {
    bruteAllNNsearch(empty,data,npoints,dim);
  }

} ; // end of BruteAllNNSearch


class BuildANNTree {
  ANNbd_tree* tree  ;

public:

  BuildANNTree() { tree = NULL ; } ;

  void op() {
    if (tree != NULL) delete tree;	
    tree=new ANNbd_tree(pa,npoints,dim,leaf_size,ANN_KD_SUGGEST,ANN_BD_SUGGEST) ;
  } ;

  ANNbd_tree *result() { return tree ; } ;
} ; // end of BuildANNTree


class BuildFLANNTree {
  flann_index_t index ;
  float speedup ;
 public:

  BuildFLANNTree() { index = NULL ; } ;

  void op() {
    if (index != NULL) flann_free_index(index,&flann_params);	
    //    flann_params.algorithm = FLANN_INDEX_AUTOTUNED;
    // flann_params.target_precision = 0.9 ;
    // flann_params.log_level=FLANN_LOG_INFO ;
    flann_params.algorithm = FLANN_INDEX_KDTREE;
    flann_params.checks = flann_checks ; // 128 ;
    flann_params.trees = flann_trees ; // 4 ;
    index=flann_build_index_double(data,npoints,dim,&speedup,&flann_params) ;
    // cout << "FLANN estimated speedup: " << speedup << "\n" ;
  } ;

  flann_index_t result() { return index ; }
} ;

class BuildLSHTree {
  Lsh* index ;
 public:

  BuildLSHTree() { index = NULL ; } ;

  void op() {
    if (index != NULL) delete index;	
    LshOptions opt ;
    opt.htype = LSH_HASH_TYPE_L2 ;
    opt.dist = DISTANCE_L2 ;
    opt.norm = true ;
    opt.ndims = dim ;
    opt.w = lshW ;
    opt.nfuncs = nlshfuncs ;
    opt.ntables = nlshtables ;
    opt.tablesize = npoints  ; 
    index=new Lsh(opt) ;
    index->createFuncs() ;
    insertPoints(*index,*lshdata,0) ;
    // index->build(*lshdata) ;
    // for (int i=0;i<npoints;i++)
      
  } ;

  Lsh* result() { return index ; }
} ;


class SearchANNTree {

public:
  void op() {
      ANNidx apx_nn_idx[1] ;
      ANNdist apx_dists[1] ;
      annMaxPtsVisit(max_visited) ;
      for (int i=0;i<npoints;i++) {
        anntree->annkPriSearch(pa[i],1,apx_nn_idx,apx_dists,ann_epsilon) ;
      } 

  } ;

} ; // end of SearchANNTree

class ANNEntropy {
  FKLEntropyEstim entropy ;

public:

  ANNEntropy() : entropy(npoints,dim,eepsilon) {} ;

  void op() {
      ANNidx apx_nn_idx[1] ;
      ANNdist apx_dists[1] ;
      entropy.reset() ;
      annMaxPtsVisit(max_visited) ;
      for (int i=0;i<npoints;i++) {
        anntree->annkPriSearch(pa[i],1,apx_nn_idx,apx_dists,ann_epsilon) ;
        //        twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,data+i*dim) ;
        JKDataType dist= /* sqrt */ (apx_dists[0]) ;
        // if (best.first!=data+apx_nn_idx[0]*dim (* || best.second!=apx_dists[0] *) ) {
        //   cout << "q=" ;
        //   printPoint(data+i*dim) ;
        //   cout << " ANN=" ;
        //   printPoint(data+apx_nn_idx[0]*dim) ;
        //   cout << " d=" << apx_dists[0] << " true=" ;
        //   printPoint(best.first) ;
        //   cout << " d=" << best.second << "\n" ;
        // }
        entropy(data+i*dim,data+apx_nn_idx[0]*dim,dist) ;
        // entropy(data+i*dim,best.first,best.second) ;
      } 

  } ;
  
  double get_entropy() { return entropy.getEntropy() ; } ;

} ;


class BruteEntropy {
  FKLEntropyEstim entropy ;

public:

  BruteEntropy() : entropy(npoints,dim,eepsilon) {} ;

  void op() {
      entropy.reset() ;
      // entropy(data,data,1.0) ;
#if 1
      for (int i=0;i<npoints;i++) {
        twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,data+i*dim) ;
         entropy(data+i*dim,best.first,best.second) ;
      } 
#endif
  } ;
  
  double get_entropy() { return entropy.getEntropy() ; } ;

} ;



class FLANNEntropy {
  FKLEntropyEstim entropy ;

public:

  FLANNEntropy() : entropy(npoints,dim,eepsilon) {} ;


  void op() {
    // the first NN is the query point itself
      int idx[2] ;
      JKDataType dists[2] ;
      entropy.reset() ;
      for (int i=0;i<npoints;i++) {
        //        twoValues<JKDataType *,JKDataType> best=bruteNNsearch(data,npoints,dim,data+i*dim) ;
        int res=flann_find_nearest_neighbors_index_double(flann_index,data+i*dim,1,idx,dists,2,&flann_params) ;
        JKDataType dist=/* sqrt */ (dists[1]) ;
        // if ( best.first!=data+idx[1]*dim /* || best.second!=dists[1] */ ) {
        //    cout << "q=" ;
        //    printPoint(data+i*dim) ;
        //    cout << " FLANN=" ;
        //    cout << " idx=" << idx[0] << " " << idx[1] << " " ;
        //    printPoint(data+idx[1]*dim) ;
        // cout << "i=" << i << " d=" << dist << "\n" ; // true=" ;
        //    printPoint(best.first) ;
        //    cout << " d=" << best.second << "\n" ;
        // }
        assert (res==0) ;
        entropy(data+i*dim,data+idx[1]*dim,dist) ;
        // entropy(data+i*dim,best.first,best.second) ;
      } 

  } ;
  
  double get_entropy() { return entropy.getEntropy() ; } ;

} ;


class LSHEntropy {
  FKLEntropyEstim entropy ;

public:

  LSHEntropy() : entropy(npoints,dim,eepsilon) {} ;


  void op() {
    int npnf=0 ;
    entropy.reset() ;
    for (int i=0;i<npoints;i++) {
        Data<JKDataType> inp ;
        JKDataType dists[2] ;
        uint idx[2] ;
        inp.type=DATA_FIXED ;
        inp.npoints=1 ;
        inp.ndims=dim ;
        inp.setFixed(data+i*dim) ;
        getKnn(*lshindex,*lshdata,inp,2,DISTANCE_L2,idx,dists) ;
        if (dists[1]<1e50)
          entropy(data+i*dim,data+idx[1]*dim,dists[1]*dists[1]) ;
        else
          npnf++ ;
        //entropy(data+i*dim,best.first,best.second ) ;
      } 
    if (npnf>0) 
      cerr << npnf << " points not found.\n" ;
  } ;
  
  double get_entropy() { return entropy.getEntropy() ; } ;

} ;




int main(int argc, char **argv)
{
  string cmd ;
  string arg ;

  if (verbosity>0)
    cerr << "allnn_exp started.\n" ;

  while (1) {
    if (verbosity>=3)
      cerr << "Ready.\n" ;
    cmd="" ;
    cin >>  cmd ; 
    if (verbosity>=3)
      cerr << "Read command '" << cmd << "'.\n" ;

    if (cmd == "set_dim") {
      cin >> dim ;
      if (verbosity>=3) 
        cerr << "Dimension set to: " << dim << "\n" ;
    }

    else if (cmd == "quit") break ;

    else if (cmd == "set_leaf_size") {
      cin >> leaf_size ;
      if (verbosity>=3) 
        cerr << "Leaf size set to: " << leaf_size << "\n" ;
    }

    else if (cmd == "verbose") {
      cin >> verbosity ;
      if (verbosity>=3) 
        cerr << "Verbosity set to: " << verbosity << "\n" ;
    }

    else if (cmd == "set_delta") {
      cin >> delta ;
      if (verbosity>=3) 
        cerr << "Delta set to: " << delta << "\n" ;
    }

    else if (cmd == "set_usetbb") {
      cin >> useTbb ;
      if (verbosity>=3) 
        cerr << "UseTBB set to: " << useTbb << "\n" ;
    }

    else if (cmd == "set_lshw") {
      cin >> lshW ;
      if (verbosity>=3) 
        cerr << "LshW set to: " << lshW << "\n" ;
    }


    else if (cmd == "set_ann_epsilon") {
      cin >> ann_epsilon ;
      if (verbosity>=3) 
        cerr << "ANN epsilon set to: " << ann_epsilon << "\n" ;
    }

    else if (cmd == "set_max_visited") {
      cin >> max_visited ;
      if (verbosity>=3) 
        cerr << "Max_visited set to: " << max_visited << "\n" ;
    }

    else if (cmd == "set_delta") {
      cin >> delta ;
      if (verbosity>=3) 
        cerr << "delta set to: " << delta << "\n" ;
    }

    else if (cmd == "set_nlshfuncs") {
      cin >> nlshfuncs ;
      if (verbosity>=3) 
        cerr << "nlshfuncs set to: " << nlshfuncs << "\n" ;
    }

    else if (cmd == "set_nlshtables") {
      cin >> nlshtables ;
      if (verbosity>=3) 
        cerr << "nlshtables set to: " << nlshtables << "\n" ;
    }

    else if (cmd == "set_flann_checks") {
      cin >> flann_checks ;
      if (verbosity>=3) 
        cerr << "flann_checks set to: " << flann_checks << "\n" ;
    }

    else if (cmd == "set_flann_trees") {
      cin >> flann_trees ;
      if (verbosity>=3) 
        cerr << "flann_trees set to: " << flann_trees << "\n" ;
    }




    else if (cmd == "read_data_pts") {
      if (data && verbosity >= 3) {
        cerr << "Data will be overwritten.\n" ;  
      }
      // data = NULL ;
      cin >> arg ;
      if (verbosity>=3)
        cerr << "Reading data points from file '" << arg << "'\n" ;
      readPts(arg,dim,npoints,data) ;
      if (verbosity>=3)
        cerr << "Read " << npoints << " points.\n" ;
    }

    else if ( cmd == "vnucko_build_tree") {

      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }
      
      if (tree) {
        delete tree ; 
        tree = NULL ;
      }

      if (verbosity>=3)
        cerr << "Starting to build the tree.\n" ;
      
      BuildCBBFTree buildtree(data,dim,npoints,leaf_size,0,dim-1,delta,max_visited,useTbb) ;
      double t=timeit(buildtree) ;
      tree=buildtree.result() ;
      // tree->tree_not_none() ;

      if (verbosity>=3)
        cerr << "Building the tree took " << t << " seconds.\n" ;
      cout << t << "\n" ;
    } // end of vnucko_build_tree if

    else if ( cmd == "vnucko_update_tree") {

      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }
      
      if (!tree) {
        cerr << "No tree built.\n"  ;
        continue ;
      }

      if (verbosity>=3)
        cerr << "Starting to update the tree.\n" ;

      // tree->tree_not_none() ;
      clock_t start = clock () ;
      twoValues<int> r=tree->Update(data) ;
      clock_t end = clock () ;
      double t=((double)end-(double)start)/double(CLOCKS_PER_SEC) ;

      if (verbosity>=3) {
        cerr << "Updating the tree took " << t << " seconds. " ;
        cerr << "Update returned: moved=" << r.first << " rebuilt=" << r.second << "\n" ;
        cerr << "update summary: delta=" << delta << " moved=" <<
          static_cast<int>( (100*r.first) / npoints ) << 
          "% rebuilt=" <<          static_cast<int>( (100*r.second) / npoints ) << "%.\n" ;
      }
      cout << t << "\n" ;
    } // end of vnucko_update_tree if
    else if ( cmd == "vnucko_all_nn_search" ) {

      if (verbosity>=3)
        cerr << "Starting Vnucko All-NN search.\n" ;

      if (!tree) {
        cerr << "No Vnucko tree built.\n" ;
        continue ;
      }

      VnuckoAllNNSearch searchtree(tree) ;
      double t=timeit(searchtree) ;
      if (verbosity>=3)
        cerr << "Vnucko All-NN search took " << t << " seconds.\n" ;
      cout << t << "\n" ;
    } // end of vnucko_all_nn_search if

    else if ( cmd == "vnucko_nnn_search" ) {

       if (!tree) {
         cerr << "No tree built.\n" ;
         continue ;
       }

       if (verbosity>=3)
         cerr << "Starting repeated BBF NN search.\n" ;

      VnuckoNNNSearch nnnsearch ;
      double t=timeit(nnnsearch) ;
      if (verbosity>=3)
        cerr << "Repeated BBF NN search took " << t << " seconds.\n" ;
      cout << t << "\n" ;
    } // end of vnucko_nnn_search

    else if ( cmd == "brute_all_nn_search" ) {

      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }

      if (verbosity>=3)
        cerr << "Starting brute force NN search.\n" ;

      BruteAllNNSearch brutesearch(data,npoints,dim) ;
      double t = timeit(brutesearch) ;
      if (verbosity>=3)
        cerr << "Brute force NN search took " << t << "seconds.\n" ;
      cout << t << "\n" ;
    } // end of brute_all_nn_search if


    else if ( cmd == "ann_build_tree" ) {
      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }
      
      cerr << "Copying data points for ANN.\n" ;
      if (pa) annDeallocPts(pa) ; 
      pa = annAllocPts(npoints, dim);
      for (int i = 0 ; i < npoints ; i++)
        for (int j = 0 ; j < dim ; j++)
          pa[i][j]=data[i*dim+j] ;

      if (anntree != NULL) delete anntree;	
      cerr << "Building ANN tree.\n" ;
      
      BuildANNTree buildanntree ;
      double t = timeit(buildanntree) ;
      anntree = buildanntree.result() ;
      if (verbosity>=3)
        cerr << "All NN build tree took " << t << " seconds.\n" ;
      cout << t << "\n" ;


    } // end of ann_build_tree

    else if ( cmd == "ann_all_nn_search" ) {

      if (!anntree) {
        cerr << "No ANN tree built.\n" ;
        continue ;
      }
      cerr << "ANN all NN search.\n" ;
      SearchANNTree searchanntree ;
      double t=timeit(searchanntree) ;
      if (verbosity>=3)
        cerr << "All NN search took " << t << " seconds.\n" ;
      cout << t << "\n" ;

    } // end of ann_all_nn_search

    else if ( cmd == "show_ann") {
      
      FShow* show = new FShow(data,dim) ;

      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }

      if (!anntree) {
        cerr << "No ANN tree built.\n" ;
        continue ;
      }

      if (!tree) {
        cerr << "No Vnucko tree built.\n" ;
        continue ;
      }

      cout << "Brute force results.\n" ;
      bruteAllNNsearch(*show,data,npoints,dim) ;

      cout << "\nBBF results.\n" ;
      tree->IterateAllApproxNN(*show) ;

      cout << "\nANN results.\n" ;
      {
      ANNidx apx_nn_idx[1] ;
      ANNdist apx_dists[1] ;
      annMaxPtsVisit(max_visited) ;
      for (int i=0;i<npoints;i++) {
        anntree->annkPriSearch(pa[i],1,apx_nn_idx,apx_dists) ;
        (*show)(data+i*dim,data+apx_nn_idx[0]*dim,apx_dists[0]) ;
        } 
      }

      cout << "\nBBF NNN results.\n" ;
      repeatedNNsearch(*show,data,npoints,dim,tree) ;
      cout << "\n" ;

    } // end of show_ann
    else if ( cmd == "vnucko_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting Vnucko entropy calculations.\n" ;

      if (!tree) {
        cerr << "No Vnucko tree built.\n" ;
        continue ;
      }

      VnuckoEntropy vnentropy(tree) ;
      double t=timeit(vnentropy) ;
      if (verbosity>=3)
        cerr << "BBF entropy=" << vnentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << vnentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of vnucko_entropy if
    else if ( cmd == "vnucko_nnn_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting Vnucko NNN entropy calculations.\n" ;

      if (!tree) {
        cerr << "No Vnucko tree built.\n" ;
        continue ;
      }

      VnuckoNNNEntropy vnentropy(tree) ;
      double t=timeit(vnentropy) ;
      if (verbosity>=3)
        cerr << "BBF NNN entropy=" << vnentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << vnentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of vnucko_nnn_entropy if
    else if ( cmd == "brute_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting brute force entropy calculations.\n" ;

      BruteEntropy brentropy ;
      double t=timeit(brentropy) ;
      if (verbosity>=3)
        cerr << "Brute force entropy=" << brentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << brentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of brute_entropy if

    else if ( cmd == "ann_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting ANN entropy calculations.\n" ;

      if (!anntree) {
        cerr << "No ANN tree built.\n" ;
        continue ;
      }

      ANNEntropy annentropy ;
      double t=timeit(annentropy) ;
      if (verbosity>=3)
        cerr << "ANN entropy=" << annentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << annentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of ann_entropy if
    else if ( cmd == "vnucko_check" ) {
      FCheck check = FCheck(data,dim,npoints) ;

      if (verbosity>=3)
        cerr << "Starting Vnucko check.\n" ;

      if (!tree) {
        cerr << "No vnucko tree built.\n" ;
        continue ;
      }
      tree->IterateAllApproxNN(check) ;
    } // end of vnucko_check
    if ( cmd == "vnucko_ptrue" ) {
      FPtrue check = FPtrue(data,dim,npoints) ;

      if (verbosity>=3)
        cerr << "Starting Vnucko ptrue.\n" ;

      if (!tree) {
        cerr << "No vnucko tree built.\n" ;
        continue ;
      }
      tree->IterateAllApproxNN(check) ;
      if (verbosity>=3)
        cerr << "Vnucko ptrue=" << check.get_ptrue() << "\n" ;
      cout << check.get_ptrue()  << "\n" ;

    } // end of vnucko_ptrue
    if ( cmd == "vnucko_relerr" ) {
      FRelerr check = FRelerr(data,dim,npoints) ;

      if (verbosity>=3)
        cerr << "Starting Vnucko relerr.\n" ;

      if (!tree) {
        cerr << "No vnucko tree built.\n" ;
        continue ;
      }
      tree->IterateAllApproxNN(check) ;
      if (verbosity>=3)
        cerr << "Vnucko relerr=" << check.get_relerr() << "\n" ;
      cout << check.get_relerr() << "\n"  ;

    } // end of vnucko_relerr
    else if ( cmd == "ann_check" ) {
      FAnnCheck check = FAnnCheck(data,dim,npoints) ;
      if (verbosity>=3)
        cerr << "Starting ANN check.\n" ;

      if (!anntree) {
        cerr << "No ANN tree built.\n" ;
        continue ; }
      
      check.do_check(anntree) ; 
    } // end of ann_check
    else if ( cmd == "flann_build_tree" ) {
      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }
      
      if (flann_index != NULL) flann_free_index(flann_index,&flann_params) ;	
      cerr << "Building FLANN tree (index).\n" ;
      
      BuildFLANNTree buildflanntree ;
      // cerr << "Calling buildflanntree\n" ;
      double t = timeit(buildflanntree) ;
      flann_index = buildflanntree.result() ;
      if (verbosity>=3)
        cerr << "FLANN build tree took " << t << " seconds.\n" ;
      cout << t << "\n" ;
    } // end of flann_build_tree
    else if ( cmd == "flann_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting FLANN entropy calculations.\n" ;

      if (!flann_index) {
        cerr << "No FLANN tree built.\n" ;
        continue ;
      }

      FLANNEntropy flannentropy ;
      double t=timeit(flannentropy) ;
      if (verbosity>=3)
        cerr << "FLANN entropy=" << flannentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << flannentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of flann_entropy if
    else if ( cmd == "lsh_build_tree" ) {
      if (!data) {
        cerr << "No data points loaded.\n" ;
        continue ;
      }
      
      cerr << "Copying data points for LSH.\n" ;
      if (lshdata) delete lshdata ;
      lshdata = new Data<JKDataType> ;
      lshdata -> type = DATA_FIXED ;
      lshdata -> npoints = npoints ;
      lshdata -> ndims = dim ;
      lshdata -> setFixed(data) ;

      if (lshindex) delete lshindex ;	
      cerr << "Building LSH index.\n" ;
      BuildLSHTree buildlshtree ;
      double t = timeit(buildlshtree) ;
      lshindex = buildlshtree.result() ;
      if (verbosity>=3)
        cerr << "LSH build index took " << t << " seconds.\n" ;
      cout << t << "\n" ;
    } // end of lsh_build_tree
    else if ( cmd == "lsh_entropy" ) {

      if (verbosity>=3)
        cerr << "Starting LSH entropy calculations.\n" ;

      if (!lshindex) {
        cerr << "No LSH index built.\n" ;
        continue ;
      }

      LSHEntropy lshentropy ;
      double t=timeit(lshentropy) ;
      if (verbosity>=3)
        cerr << "LSH entropy=" << lshentropy.get_entropy() << ", time=" << t << " seconds.\n" ;
      cout << lshentropy.get_entropy() << "\n" << t << "\n" ;
    } // end of lsh_entropy if
    else 
      { cerr << "Unknown command '" << cmd << "'.\n" ;
      }
    
  } // end while
    if (verbosity>0) {
      cerr << "Good bye.\n" ;
    }




} // end of main

void readPts(string filename,int dim, int &npoints, JKDataType*& data)
{
  ifstream fd(filename.c_str());	
  std::vector<JKDataType> v ;
  int npointsn ;


  if (!fd) {
    cerr << "Cannot open input file '" << filename << "'.\n" ;
    return ;
  }

  // first read all data into a vector
  while (true) {
    JKDataType d ;
    if (!(fd >> d)) break ;
    v.push_back(d) ;
  }


  if (v.size() % dim != 0) {
    cerr << "Read " << v.size() << " points. Not a multiple of dim=" <<
      dim << ".\n" ;
    return  ;
  }
    
  npointsn = v.size() / dim ;
  if (verbosity>=3) {
    cerr << "Read " << npointsn << " points from the file.\n" ;
  }

  if (data && npointsn!=npoints) {
    cerr << "Error: The number of points changed.\n" ;
    return ;
  }
  npoints = npointsn ;


  if (!data)
    data = new JKDataType[npoints*dim] ;

  // copy data from the vector
  for (int i=0;i<npoints*dim;i++)
    data[i]=v[i] ;

  return ;
}


// ------------- end of allnn_exp.cpp ------------------- 

