/**
 *  @file bbftree.cpp
 *  @brief Data structure bbftree (Best Bin First k-D tree) implementation.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 14.12.2005
 */

#include "bbftree.h"

#include <fstream>

CBBFTree::CBBFTree(const JKDataType* inData, DimensionType inDimOfData,
  int inSizeOfData) :
    data(inData), dimOfData(inDimOfData), sizeOfData(inSizeOfData),
    params(inDimOfData),
    root(inData, inDimOfData, inSizeOfData, params.dimRange),
    built(false) {
};

CBBFTree::CBBFTree(const JKDataType* inData, DimensionType inDimOfData,
  int inSizeOfData, int leafSize, DimensionType dmin, DimensionType dmax,
                   float delta, int maxVisit, bool inUseTbb) :
    data(inData), dimOfData(inDimOfData), sizeOfData(inSizeOfData),
    params(inDimOfData, leafSize, dmin, dmax, delta, maxVisit,inUseTbb),
    root(inData, inDimOfData, inSizeOfData, DimRangeType(dmin, dmax)),
    built(false) {
};

CBBFTree& CBBFTree::Build() {
  if(built) {
    delete root.getLeftSon();
    delete root.getRightSon();
  }
  // cerr << "Build tree called.\n" ;
  if(root.getSize() > params.leafSize)
    root.cutNode(params.dimRange, params.leafSize);
  built = true;
  //  fstream fd("bld.txt",ios::out);
  // fd << root ;
  // fd.close() ;
  return *this;
};

twoValues<int> CBBFTree::Update(const JKDataType* inData) {
  int upd = -1;
  int reb = -1;
  if(built) {
    if(inData)
      data = inData;
    // root.tree_not_none() ;
    upd = root.updateNode(params.dimRange);
    // root.tree_not_none() ;
    reb = root.rebuildNode(params.dimRange, params.leafSize, params.delta);
    // fstream fd("upd.txt",ios::out);
    // fd << root ;
    // fd.close() ;
  }
  return twoValues<int>(upd, reb);
};
