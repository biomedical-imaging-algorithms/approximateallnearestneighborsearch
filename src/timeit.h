// Automatic timing of an operation
// 
// Jan Kybic
//
//
// Usage: 
//   Define a class possibly deriving from Timed with methods: op()
//  Calling timeit() will repeat { op() } as many times
//  as required for accurate measurement and will return the time it takes to
//  run 'op()' in seconds.

#include <time.h>


/* class Timed { */

/*  public: */

/*   void op() { } ; */

/* } */

template <class T> double timeit_n(T& t, int n)
{
  clock_t start = clock () ;
  for (int i=0;i<n;i++)    t.op();
  clock_t end = clock () ;
  return ((double)end-(double)start)/double(CLOCKS_PER_SEC) ;
} 


template <class T> double timeit(T& t)
{
  int n = 1 ;
  for(;;) {
    // cerr << "Timing started \n" ;
    double e = timeit_n(t,n) ;
    // cerr << "Timing returned " << e << " s\n" ;
    if (e>=  0.  ) return e / double(n) ;
    if (n>=1000000) {
      cerr << "WARNING: Timing requires too many repetitions.\n" ;
      return e / double(n) ;
    }
    n=10*n ;
  }

} 



// end of timeit.h
