/**
 *  @file bbftreenode.cpp
 *  @brief Node of the tree, implementation.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 16.01.2006
 */

#include "bbftreenode.h"

#include <iostream> 

// #define DEBUGNNN 1

using namespace std ;

CBBFTreeNode::CBBFTreeNode(DimRangeType inDimRange, CBBFTreeNode* inParent,
  CPointSet* inPointSet, DimensionType inCutDim, JKDataType inCutVal,
  bool left) :
    pointSet(inPointSet),
    looseBox(inParent->looseBox, inCutDim, inCutVal, left),
    tightBox(*pointSet, looseBox.dimensionality, inDimRange),
    cutDimension(-1),
    cutValue(0),
    size(inPointSet->getSize()),
    leaf(true),
    leftSon(0), rightSon(0), parent(inParent),
    leftSet(), rightSet(), actualSet(pointSet) {
};

CBBFTreeNode::CBBFTreeNode(const JKDataType* inData, DimensionType inDimOfData,
  int inSizeOfData, DimRangeType inDimRange) :
    pointSet(new CPointSet(inData, inDimOfData, inSizeOfData)),
    looseBox(inDimOfData),
    tightBox(*pointSet, inDimOfData, inDimRange),
    cutDimension(-1),
    cutValue(0),
    size(inSizeOfData),
    leaf(true),
    leftSon(0), rightSon(0), parent(0),
    leftSet(), rightSet(), actualSet(pointSet) {
};

CBBFTreeNode::~CBBFTreeNode() {
  delete pointSet;
  delete leftSon;
  delete rightSon;
};

void CBBFTreeNode::createTightBox(DimRangeType inDimRange) {
  tightBox.reset();
  tightBox.addPoints(*pointSet, inDimRange);
};

bool inline CBBFTreeNode::goesToLeft(const PointType inPoint) {
  return inPoint[cutDimension] <= cutValue;
};

bool CBBFTreeNode::cutNode(DimRangeType inDimRange, int inLeafSize) {
  bool created = false;
  CPointSet* leftSet = 0;
  CPointSet* rightSet = 0;
  // try to split the point set
  DimensionType lDim = tightBox.longestDim(inDimRange);
  if(lDim >= 0) {
    // pointSet->assert_not_none() ;
    // if (size<=20)
    //   cerr << "points: " << *pointSet << "\n" ;
    JKDataType cutVal = pointSet->split(lDim, leftSet, rightSet);
    //    cerr << "cutNode: size=" << size << " dim=" << lDim << " val=" << cutVal << "\n" ; 
//     if (size<=20) {
//       if (leftSet)
//         cerr << "leftSet: " << *leftSet << "\n" ;
//       if (rightSet)
//         cerr << "rightSet: " << *rightSet << "\n" ;
//     }
    // if the childsets were created, create the nodes for them

    if(leftSet != 0) {
      // create the two child nodes and return
      leftSon = new CBBFTreeNode(inDimRange, this,
        leftSet, lDim, cutVal, true);
      rightSon = new CBBFTreeNode(inDimRange, this,
        rightSet, lDim, cutVal, false);
      created = true;
      leaf = false;
      cutDimension = lDim;
      cutValue = cutVal;
      if(leftSon->getSize() > inLeafSize) {
        leftSon->cutNode(inDimRange, inLeafSize);
      }
      if(rightSon->getSize() > inLeafSize) {
        rightSon->cutNode(inDimRange, inLeafSize);
      }
    } else
      leaf = true;
  }
  return created;
};

int CBBFTreeNode::updateNode(DimRangeType inDimRange) {
  int moved = 0;
  if(leaf && (parent != 0)) {
    // if it's a non-root leaf, check the points if they are in their BBoxes,
    // if there was a point, which was outside, balancing is needed
    moved = pointSet->update(looseBox, inDimRange, *parent->actualSet);
    size -= moved;
    createTightBox(inDimRange) ; // added by JK
  } else {
    // if it's a non-leaf, run update for children
    if (leftSon) {
      actualSet = &leftSet;
      moved = leftSon->updateNode(inDimRange);
    }
    if (rightSon) {
      actualSet = &rightSet;
      moved += rightSon->updateNode(inDimRange);
    }
    // move points obtained from children to parent if needed
    if(parent != 0) {
      int up = 0;
      if(!leftSet.empty())
        up = leftSet.update(looseBox, inDimRange, *(parent->actualSet));
      if(!rightSet.empty())
        up += rightSet.update(looseBox, inDimRange, *(parent->actualSet));
      size -= up;
    }
  }
  return moved;
};

int CBBFTreeNode::rebuildNode(DimRangeType inDimRange, int inLeafSize,
  float inDelta) {
  //  tree_not_none() ;
  // pointSet->assert_not_none() ;
    if((!leaf) && (size > 0)) {
      // points which came from it's parent need to be divided among children
      if(!(pointSet->empty())) {
        CPointSet::iterator endSet = pointSet->end();
        CPointSet::iterator iter = pointSet->begin();
        while(iter != endSet) {
          if(goesToLeft(*iter)) {
            iter = pointSet->move(iter, *(leftSon->pointSet));
            ++(leftSon->size);
          } else {
            iter = pointSet->move(iter, *(rightSon->pointSet));
            ++(rightSon->size);
          }
        }
      }
      // tree_not_none() ;
      // pointSet->assert_not_none() ;
      // calculate the size limit for children
      int limit = static_cast<int>((inDelta + 0.5) * size) + 1;
      // check both children sizes and possibly rebuild the whole node
      int numLeft = leftSon->getSize();
      int numRight = rightSon->getSize();
      // add remaining not assigned point numbers;
      numLeft = numLeft + leftSet.getSize();
      numRight = numRight + rightSet.getSize();
      // check the children sizes
      if((numLeft > limit) || (numRight > limit) ||
        (numLeft == 0) || (numRight == 0)) {
        //                cerr << "delta: " << inDelta << " limit=" << limit << " size=" << size << " numL=" << numLeft << " numR=" << numRight << "\n" ;
        //         cerr << "  rebuilding.\n" ;
          // delete the subtree of this node
          leftSon->releasePoints();
          delete leftSon;
          leftSon = 0;
          // tree_not_none() ;
          // pointSet->assert_not_none() ;
          pointSet->splice(rightSet);
          pointSet->splice(leftSet);
          // pointSet->assert_not_none() ;
          rightSon->releasePoints();
          // pointSet->assert_not_none() ;
          delete rightSon;
          // pointSet->assert_not_none() ;
          rightSon = 0;
          leaf = true;
          // build new subtree
          // pointSet->assert_not_none() ;
          // tree_not_none() ;
          createTightBox(inDimRange); // added by JK
          if(size > inLeafSize) // was: 'limit'
            cutNode(inDimRange, inLeafSize);
          return size;
      } else {
        // if there is no need to rebuild, move remaining points to children
        if(!leftSet.empty()) {
          CPointSet& right = rightSon->getPointSet();
          rightSon->size += leftSet.getSize();
          right.splice(leftSet);
        }
        if(!rightSet.empty()) {
          CPointSet& left = leftSon->getPointSet();
          leftSon->size += rightSet.getSize();
          left.splice(rightSet);
        }
        int num = leftSon->rebuildNode(inDimRange, inLeafSize, inDelta);
        num += rightSon->rebuildNode(inDimRange, inLeafSize, inDelta);
        tightBox.createUnion(leftSon->tightBox, rightSon->tightBox,
          inDimRange);
        return num;
      }
    } else {
      createTightBox(inDimRange);
      return 0;
    }
};

void CBBFTreeNode::releasePoints() {
  // pointSet->assert_not_none() ;
  if(!leaf) {
    CPointSet& parentSet = *parent->pointSet;
    if(!leftSet.empty())
      parentSet.splice(leftSet);
    if(!rightSet.empty())
      parentSet.splice(rightSet);
    leftSon->releasePoints();
    rightSon->releasePoints();
  }
  if(parent != 0) {
    CPointSet& parentSet = *parent->pointSet;
    if(!pointSet->empty())
      parentSet.splice(*pointSet);
  }
  // pointSet->assert_not_none() ;
};

// #define INCOMPLETEDISTANCE 1

inline void CBBFTreeNode::pushIfBetter(CBBFTreeNode* inNode,
  CPointSet::iterator inQuery, CBBFTreeNode* inPrevNode,
                                       JKDataType minDist, PQueueType& inPQueue, DimRangeType inDimRange,bool useTbb) {
    JKDataType dist;
    if(inPrevNode->parent == inNode) {
      dist = inPrevNode->looseBox.extDistance(*inQuery, inDimRange);
    } else {
#if INCOMPLETEDISTANCE
      //#if 0
      if (useTbb) 
        dist = inNode->tightBox.distanceLim(*inQuery, inDimRange,minDist);
      else
        dist = inNode->looseBox.distanceLim(*inQuery, inDimRange,minDist);
#else
      if (useTbb) 
        dist = inNode->tightBox.distance(*inQuery, inDimRange);
      else
        dist = inNode->looseBox.distance(*inQuery, inDimRange);
#endif
    }
#ifdef DEBUGNNN
        cout << "  pushIfBetter distance="  << dist << "\n" ;
#endif
    if(dist < minDist) {
#ifdef DEBUGNNN
        cout << "  pushed to the queue.\n" ;
#endif
      inPQueue.push(QueueElementType(inNode, inPrevNode, dist));
    }
};

int CBBFTreeNode::findLowerDistance(
  twoValues<CPointSet::iterator, JKDataType>& inPointNN,
  const CPointSet::iterator& inQuery, DimRangeType inDimRange, int inMaxVisit,
  bool exact,bool pushroot,bool useTbb) {
    int toVisit = inMaxVisit;
    PQueueType pQueue;
    if(parent != 0) { // parent exists
      JKDataType dist = looseBox.extDistance(*inQuery, inDimRange);
      if(dist < inPointNN.second)
        pQueue.push(QueueElementType(parent, this, dist));
    }
    if (pushroot) {
        pQueue.push(QueueElementType(this, NULL, 0.));
    }

    QueueElementType actElement;
    while((exact || (toVisit > 0)) && !pQueue.empty()) {
     // get the "best" node from the queue
     actElement = pQueue.top();
     pQueue.pop();
     CBBFTreeNode* node = actElement.first;
     // only continue if the node can contain a NN closer than a current minimum
#ifdef DEBUGNNN
     if (inPointNN.first!=NULL)
       cerr << "Current best point x=" << *inPointNN.first.toPointer()->content << 
         " d=" << inPointNN.second << "\n" ;
     else
       cerr << "No best point so far.\n" ;
     if (node->leaf)
       cerr << "Popped a leaf, tightbox=(" <<
         *actElement.first->tightBox.lowLim << " " <<
         *actElement.first->tightBox.highLim << ") distance=" <<
         actElement.third << "\n" ;
     else
       cerr << "Popped a non-leaf, cutvalue=" << actElement.first->cutValue
            << " tightbox=(" <<
         *actElement.first->tightBox.lowLim << " " <<
         *actElement.first->tightBox.highLim << ") distance=" <<
         actElement.third << "\n" ;
#endif
     if (actElement.third < inPointNN.second) {
#ifdef DEBUGNNN
       cerr << "  It is a potentially better point.\n" ;
#endif
     CBBFTreeNode* prevNode = actElement.second;
     CPointSet& pointSet = node->getPointSet();
     if(node->leaf) {
        // if this node is a leaf, search it for a nearer point to inQuery
        twoValues<CPointSet::iterator, JKDataType>
          newNN = pointSet.findNN(inQuery, inDimRange);
        toVisit -= node->size;
#ifdef DEBUGNNN
        if (newNN.first!=NULL)
                    cerr << "  Leaf searched found NN x=" <<
            *newNN.first.toPointer()->content <<
            " d=" << newNN.second << "\n" ;
          else
            cerr << "  Leaf searched no NN found.\n" ;
#endif

        // if found NN is better, update the actual one
        if((newNN.first != pointSet.end()) &&
          (newNN.second < inPointNN.second)) {
            inPointNN.first = newNN.first;
            inPointNN.second = newNN.second;
#ifdef DEBUGNNN
            cerr << "  Best so far NN updated.\n" ;
#endif
        }
     } else { // node is not a leaf
        // try to push both children and parent node to the queue
        if(node->leftSon != prevNode) {
#ifdef DEBUGNNN
                   cerr << "  Trying to push the left son. tightbox=(" << 
                     *node->leftSon->tightBox.lowLim << " " <<
                     *node->leftSon->tightBox.highLim << ")\n" ;
#endif
          pushIfBetter(node->leftSon, inQuery, node, inPointNN.second, pQueue,
                       inDimRange,useTbb); }
        if(node->rightSon != prevNode) {
#ifdef DEBUGNNN
          cerr << "  Trying to push the right son. tightbox=(" <<
            *node->rightSon->tightBox.lowLim << " " <<
            *node->rightSon->tightBox.highLim << ")\n" ;
#endif
          pushIfBetter(node->rightSon, inQuery, node, inPointNN.second, pQueue,
                       inDimRange,useTbb); }
        if((node->parent != 0) && (node->parent != prevNode)) {
#ifdef DEBUGNNN
          cerr << "  Trying to push the parent. tightbox=(" <<
            *node->parent->tightBox.lowLim << " " <<
            *node->parent->tightBox.highLim << ")\n" ;
#endif
          pushIfBetter(node->parent, inQuery, node, inPointNN.second, pQueue,
                       inDimRange,useTbb);}
     } // end if node is leaf
     } // end if node is better
    } // end while
#ifdef DEBUGNNN
     cout << "Finished x=" << *inPointNN.first.toPointer()->content <<
       " d=" << inPointNN.second << "\n" ;
#endif
    return inMaxVisit - toVisit;
};

int CBBFTreeNode::getSize() {
  return size;
};

bool CBBFTreeNode::isLeaf() {
  return leaf;
};

CPointSet& CBBFTreeNode::getPointSet() {
  return (*pointSet);
};

CBBFTreeNode* CBBFTreeNode::getLeftSon() {
  return leftSon;
};

CBBFTreeNode* CBBFTreeNode::getRightSon() {
  return rightSon;
};

void indent(ostream& out, int level) {
  for (int i=0;i<level;i++)
    out << "  " ;
}

void CBBFTreeNode::show(ostream& out, int level) const {
  indent(out,level) ; 
  out << "(" << "tightBox=" << tightBox << ",\n" ;
  indent(out,level) ; 
  out << "(" << "looseBox=" << looseBox << ",\n" ;
  if (leaf) {
     indent(out,level) ; 
     out << "points=" << *pointSet << ")\n" ;
  } else {
    indent(out,level) ;
    out << "size=" << size << ",cutDim=" << cutDimension << ", cutValue=" << cutValue
        << ",left=\n" ; 
    leftSon->show(out,level+1) ;
    indent(out,level) ;
    out << "rightSon=\n" ; 
    rightSon->show(out,level+1) ;
    indent(out,level) ;
    out << ")\n" ; }
}

#if 0
void CBBFTreeNode::tree_not_none() {
  pointSet->assert_not_none() ;
  if (!leaf) {
    leftSon->tree_not_none() ;
    rightSon->tree_not_none() ;
  }
}


#endif 

ostream& operator<< (ostream& out,const CBBFTreeNode& p) 
{
  p.show(out,0) ;
  return  out ;
}
