/**
 *  @file bbox.h
 *  @brief Bounding box structure declaration.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 11.01.2006
 */

#ifndef BBOX_H
  #define BBOX_H

#include "config.h"
#include <math.h>
#include <iostream> 

using namespace std ;

class CPointSet;

/** @brief  Structure representing the bounding box for point sets. */
struct SBBox {

  /**
   *  @brief Relevancy flag. If it's false, this bounding box is unlimited
   *  in all dimensions (loose bbox) or degenerated to one point (tight bbox).
   */
  bool applyFlag;

  /** @brief Dimensionality of the data. */
  DimensionType dimensionality;

  /** @brief Lower limiting point. */
  JKDataType* lowLim;


  /** @brief Higher limiting point. */
  JKDataType* highLim;

  /**
   *  @brief Constructor creates an unlimited bounding box.
   *
   *  @arg @c inDimOfData Dimensionality of the data.
   */
  SBBox(DimensionType inDimOfData);

  /**
   *  @brief Costructor for creating loose bounding box cuts.
   *
   *  @arg @c inBox Surrounding bounding box of which will this one be a cut.
   *  @arg @c inCutDim Cutting dimension.
   *  @arg @c inCutVal Cutting value.
   *  @arg @c lower True if this is the lower (or left) cut.
   */
  SBBox(const SBBox& inBox, DimensionType inCutDim, JKDataType inCutVal,
    bool lower);

  /**
   *  @brief Constructor used in new tight box creation.
   *
   *  @arg @c inDimOfData Dimensionality of the data.
   *  @arg @c inSet #CPointSet for which to create the tight box.
   *  @arg @c inDimRange Dimensionality range to work on.
   */
  SBBox(CPointSet& inSet, DimensionType inDimOfData, DimRangeType inDimRange);

  /** @brief Copy constructor. */
  SBBox(const SBBox& inBox);

  /** @brief Destructor. */
  ~SBBox();

  /** @brief Resets the tight box to empty one. */
  void reset();

  /**
   *  @brief Copies input bbox.
   *
   *  @arg @c inBox Input bounding box, which this one will be the copy of.
   *
   *  @warning In current implementation this cannot be used for boxes with
   *  different dimensionality.
   */
  void copy(const SBBox& inBox);

  /**
   *  @brief Returns the longest dimension.
   *
   *  Find the dimension for which is the difference of high and low limit
   *  value maximal. It's used for tight bounding boxes.
   *
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return The longest dimension or -1 if the longest can not be found.
   */
  DimensionType longestDim(DimRangeType inDimRange);

  /**
   *  @brief Tests the point for being outside of this box.
   *
   *  @arg @c inPoint Point to be tested.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return The first dimension which makes the point fall out or -1
   *  if the point lies inside of this bounding box.
   */
  DimensionType isOutside(const PointType inPoint, DimRangeType inDimRange);

  /**
   *  @brief Changes the bounding box to contain given point.
   *
   *  @arg @c inPoint Input point which to add.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return True if the bounding box was changed (expanded).
   */
  bool addPoint(const PointType inPoint, DimRangeType inDimRange);

  /**
   *  @brief Changes the bounding box to contain given set of points.
   *
   *  @arg @c inSet #CPointSet with points to add.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return True if the bounding box was changed (expanded).
   */
  bool addPoints(CPointSet& inSet, DimRangeType inDimRange);

  /**
   *  @brief L_inf distance of two points.
   *
   *  @arg @c inPoint1,inPoint2 Input points.
   *  @arg @c inDimRange Dimension range to work on.
   */
    static JKDataType distance(const PointType inPoint1, const PointType inPoint2,
   DimRangeType inDimRange);

    static JKDataType distanceLim(const PointType inPoint1, const PointType inPoint2,
                                DimRangeType inDimRange, JKDataType lim);



  /**
   *  @brief L_inf distance of point to this bounding box.
   *
   *  @arg @c inPoint Input point for which to calculate the distance.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return The distance or 0 if the point is inside of the box.
   */
  JKDataType distance(const PointType inPoint, DimRangeType inDimRange);


  // JK: distanceLim seems to be like distance, except that if we know that the distance
  // is larger than 'lim', we return 'lim' without going through the other dimensions,
  // which is faster
/*   JKDataType distanceLim(const PointType inPoint, DimRangeType inDimRange,JKDataType lim) { */
/*   JKDataType result = 0; */
/*   if(applyFlag) { */
/*     for(int i = inDimRange.first; i <= inDimRange.second; ++i) { */
/*       JKDataType ddist = (inPoint[i] > highLim[i]) ? */
/*        inPoint[i] - highLim[i] : */
/*         ((inPoint[i] < lowLim[i]) ? lowLim[i] - inPoint[i] : 0); */
/*       if(ddist > result) { */
/*         result = ddist; */
/*         if (ddist>lim) return result ; */
/*       } */
/*     } */
/*   } else if(lowLim != 0) { */
/*     // if the tight bbox is degenerated only one of the limit points is to */
/*     // check for distance */
/*     for(int i = inDimRange.first; i <= inDimRange.second; ++i) { */
/*       JKDataType ddist = fabs(inPoint[i] - lowLim[i]); */
/*       if(ddist > result) */
/*         result = ddist; */
/*         if (ddist>lim) return result ; */
/*     } */
/*   } else */
/*     return INFINITY; */
/*   return result; */
/* }; */

JKDataType distanceLim(const PointType inPoint, DimRangeType inDimRange,JKDataType lim) {
  JKDataType result = 0;
#if LNORM==0
  // linf norem
  if(applyFlag) {
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = (inPoint[i] > highLim[i]) ?
        inPoint[i] - highLim[i] :
        ((inPoint[i] < lowLim[i]) ? lowLim[i] - inPoint[i] : 0);
      if(ddist > result)
        result = ddist;
      if (ddist>lim) return result ; 
    }
  } else if(lowLim != 0) {
    // if the tight bbox is degenerated only one of the limit points is to
    // be checked for distance
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = fabs(inPoint[i] - lowLim[i]);
      if(ddist > result)
        result = ddist;
      if (ddist>lim) return result ; 
    }
  } else
    return INFINITY;
  return result;
#else // l2 norm
  // JKDataType lim2=lim*lim ;
  if (applyFlag) { // non degenerate case
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      if (inPoint[i] > highLim[i]) { 
        JKDataType ddist=inPoint[i] - highLim[i] ; 
        result += ddist * ddist ;
        if (result>lim) break ;
      }
      else if ( inPoint[i] < lowLim[i]) {
        JKDataType ddist= lowLim[i] - inPoint[i]  ; 
        result += ddist * ddist ;
        if (result>lim) break ;
      } 
    }
    return /* sqrt */ (result) ;
  } else if(lowLim != 0) { // degenerate case (box==point)
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
        JKDataType ddist= lowLim[i] - inPoint[i]  ; 
        result += ddist * ddist ;
        if (result>lim) break ;
    }
    return /* sqrt */ (result) ;
  } else // degenerate case, no point
    return INFINITY ;
#endif
};




  /**
   *  @brief L_inf distance of point to the exterior of this bounding box.
   *
   *  @arg @c inPoint Input point for which to calculate the distance.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return The distance or 0 if the point is outside of the box.
   */
  JKDataType extDistance(const PointType inPoint, DimRangeType inDimRange);

  /**
   *  @brief Resets this box and creates it as a union of two input boxes.
   *
   *  @arg @c inBox1,inBox2 Input boxes.
   *  @arg @c inDimRange Dimension range to work on.
   */
  void createUnion(const SBBox& inBox1, const SBBox& inBox2,
    DimRangeType inDimRange);

  /**
   *  @brief Overlap test for two tight bounding boxes.
   *
   *  @arg @c inBox Input tight box, tested for overlap with this one.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return @c True if they have overlap, @c false if not.
   */
  bool haveOverlap(const SBBox& inBox, DimRangeType inDimRange);

};

ostream& operator<< (ostream& out, const SBBox& p) ;

#endif // ifndef BBOX_H
