/**
 *  @file bbox.cpp
 *  @brief Bounding box structure implementation.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 11.01.2006
 */

#include "bbox.h"
#include "pointset.h"

SBBox::SBBox(DimensionType inDimOfData) :
  applyFlag(false),
  dimensionality(inDimOfData),
  lowLim(new JKDataType[inDimOfData]),
  highLim(new JKDataType[inDimOfData]) {
    for(int i = 0; i < inDimOfData; ++i) {
      lowLim[i] = NEGATIVE_INFINITY;
      highLim[i] = INFINITY;
    }
};

SBBox::SBBox(const SBBox& inBox, DimensionType inCutDim, JKDataType inCutVal,
  bool lower):
    applyFlag(true),
    dimensionality(inBox.dimensionality),
    lowLim(new JKDataType[inBox.dimensionality]),
    highLim(new JKDataType[inBox.dimensionality]) {
      for(int i = 0; i < inBox.dimensionality; ++i) {
        lowLim[i] = inBox.lowLim[i];
        highLim[i] = inBox.highLim[i];
      };
      if(lower) {
        highLim[inCutDim] = inCutVal;
      } else {
        lowLim[inCutDim] = inCutVal;
      }
};

SBBox::SBBox(CPointSet& inSet,
  DimensionType inDimOfData, DimRangeType inDimRange) :
    applyFlag(false),
    dimensionality(inDimOfData),
    lowLim(0),
    highLim(0) {
      addPoints(inSet, inDimRange);
};

SBBox::SBBox(const SBBox& inBox) :
  applyFlag(inBox.applyFlag),
  dimensionality(inBox.dimensionality),
  lowLim(new JKDataType[dimensionality]),
  highLim(new JKDataType[dimensionality]) {
    for(int i = 0; i < dimensionality; ++i) {
      lowLim[i] = inBox.lowLim[i];
      highLim[i] = inBox.highLim[i];
    }
};

SBBox::~SBBox() {
  delete[] lowLim;
  delete[] highLim;
};

void SBBox::reset() {
  delete[] lowLim;
  delete[] highLim;
  lowLim = 0;
  highLim = 0;
  applyFlag = false;
};

void SBBox::copy(const SBBox& inBox) {
  applyFlag = inBox.applyFlag;
  for(DimensionType i = 0; i < inBox.dimensionality; ++i) {
    lowLim[i] = inBox.lowLim[i];
    highLim[i] = inBox.highLim[i];
  }
};

// is called only for tight boxes
DimensionType SBBox::longestDim(DimRangeType inDimRange) {
  DimensionType maxInd = -1;
  if(applyFlag) {
    JKDataType max = 0;
    JKDataType tmp;
    for(DimensionType i = inDimRange.first; i <= inDimRange.second; i++) {
      if((tmp = highLim[i] - lowLim[i]) > max) {
        maxInd = i;
        max = tmp;
      }
    }
  }
  return maxInd;
};

DimensionType SBBox::isOutside(const PointType inPoint,
  DimRangeType inDimRange) {
    DimensionType result = -1;
    if(applyFlag) {
      DimensionType index = inDimRange.first;
      while((index <= inDimRange.second) && (result < 0)) {
        if((inPoint[index] > highLim[index]) ||
          (inPoint[index] < lowLim[index]))
            result = index;
        ++index;
      }
    }
    return result;
};

bool SBBox::addPoint(const PointType inPoint, DimRangeType inDimRange) {
  bool changed = false;
  if(lowLim == 0) {
    lowLim = new JKDataType[dimensionality];
    highLim = new JKDataType[dimensionality];
    for(DimensionType i = 0; i < dimensionality; ++i) {
      lowLim[i] = inPoint[i];
      highLim[i] = inPoint[i];
    }
    changed = true;
  } else {
    for(DimensionType i = inDimRange.first; i <= inDimRange.second; ++i) {
      if(inPoint[i] > highLim[i]) {
        highLim[i] = inPoint[i];
        changed = true;
        applyFlag = true;
      } else if(inPoint[i] < lowLim[i]) {
        lowLim[i] = inPoint[i];
        changed = true;
        applyFlag = true;
      }
    }
  }
  return changed;
};

bool SBBox::addPoints(CPointSet& inSet, DimRangeType inDimRange) {
  bool changed = false;
  if(inSet.getSize() > 0) {
    CPointSet::iterator iter = inSet.begin();
    if(lowLim == 0) {
      lowLim = new JKDataType[dimensionality];
      highLim = new JKDataType[dimensionality];
      for(DimensionType i = 0; i < dimensionality; ++i) {
        lowLim[i] = (*iter)[i];
        highLim[i] = (*iter)[i];
      }
      ++iter;
      changed = true;
    }
    for(; iter != inSet.end(); ++iter){
      for(DimensionType i = inDimRange.first; i <= inDimRange.second; i++) {
        if((*iter)[i] > highLim[i]) {
          highLim[i] = (*iter)[i];
          changed = true;
          applyFlag = true;
        } else if((*iter)[i] < lowLim[i]) {
          lowLim[i] = (*iter)[i];
          changed = true;
          applyFlag = true;
        }
      }
    }
  }
  return changed;
};

JKDataType SBBox::distance(const PointType inPoint1, const PointType inPoint2,
  DimRangeType inDimRange) {
    JKDataType result = 0;
#if LNORM==0
    // linf norm
    // cerr << "linf distance\n" ;
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = fabs(inPoint1[i] - inPoint2[i]);
      if(ddist > result)
        result = ddist;
    }
    return result;
#else
    // l2 norm
    // cerr << "l2 distance\n" ;
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = inPoint1[i] - inPoint2[i];
      result += ddist * ddist ;
    }
    // return sqrt(result) ;
    return result ;
#endif
};

JKDataType SBBox::distanceLim(const PointType inPoint1, const PointType inPoint2,
                            DimRangeType inDimRange, JKDataType lim) {
    JKDataType result = 0;
#if LNORM==0
    // linf norm
    // cerr << "linf distance\n" ;
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = fabs(inPoint1[i] - inPoint2[i]);
      if(ddist > result)
        result = ddist;
      if (ddist>lim) return result ; 
    }
    return result;
#else
    // l2 norm
    // cerr << "l2 distance\n" ;
    // JKDataType lim2 = lim * lim ;
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = inPoint1[i] - inPoint2[i];
      result += ddist * ddist ;
      if (result>lim) break ;
    }
    // return sqrt(result) ;
    return result ;
#endif
};



// called only for tight boxes
JKDataType SBBox::distance(const PointType inPoint, DimRangeType inDimRange) {
  JKDataType result = 0;
#if LNORM==0
  // linf norem
  if(applyFlag) {
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = (inPoint[i] > highLim[i]) ?
        inPoint[i] - highLim[i] :
        ((inPoint[i] < lowLim[i]) ? lowLim[i] - inPoint[i] : 0);
      if(ddist > result)
        result = ddist;
    }
  } else if(lowLim != 0) {
    // if the tight bbox is degenerated only one of the limit points is to
    // be checked for distance
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      JKDataType ddist = fabs(inPoint[i] - lowLim[i]);
      if(ddist > result)
        result = ddist;
    }
  } else
    return INFINITY;
  return result;
#else // l2 norm
  if (applyFlag) { // non degenerate case
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
      if (inPoint[i] > highLim[i]) { 
        JKDataType ddist=inPoint[i] - highLim[i] ; 
        result += ddist * ddist ; }
      else if ( inPoint[i] < lowLim[i]) {
        JKDataType ddist= lowLim[i] - inPoint[i]  ; 
        result += ddist * ddist ; }
    }
    return /* sqrt */ (result) ;
  } else if(lowLim != 0) { // degenerate case (box==point)
    for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
        JKDataType ddist= lowLim[i] - inPoint[i]  ; 
        result += ddist * ddist ;
    }
    return /* sqrt */ (result) ;
  } else // degenerate case, no point
    return INFINITY ;
#endif
};


// extDistance is the same for l2 and linf
JKDataType SBBox::extDistance(const PointType inPoint, DimRangeType inDimRange){
  JKDataType result = INFINITY;
  if(applyFlag) {
    for(int i = inDimRange.first; i <= inDimRange.second; i++) {
      JKDataType ldist = INFINITY;
      JKDataType hdist = INFINITY;
      if((inPoint[i] < lowLim[i]) || (inPoint[i] > highLim[i]))
        return 0;
      if(highLim[i] != INFINITY)
        hdist = highLim[i] - inPoint[i];
      if(lowLim[i] != NEGATIVE_INFINITY)
        ldist = inPoint[i] - lowLim[i];
      if(ldist > hdist)
        ldist = hdist;
      if(ldist < result)
         result = ldist;
    }
  }
#if LNORM==0
  return result;
#else
  return result * result ;
#endif
};

// called only for tight boxes
void SBBox::createUnion(const SBBox& inBox1, const SBBox& inBox2,
  DimRangeType inDimRange) {
    reset();
    if(inBox1.lowLim != 0) {
      addPoint(inBox1.lowLim, inDimRange);
      addPoint(inBox1.highLim, inDimRange);
    }
    if(inBox2.lowLim != 0) {
      addPoint(inBox2.lowLim, inDimRange);
      addPoint(inBox2.highLim, inDimRange);
    }
};

// called only for tight boxes
bool SBBox::haveOverlap(const SBBox& inBox, DimRangeType inDimRange) {
    bool result = true;
    for(DimensionType i = inDimRange.first; i <= inDimRange.second; ++i) {
      if((lowLim[i] > inBox.highLim[i]) ||
        (inBox.lowLim[i] > highLim[i])) {
          result = false;
          break;
      }
    }
    return result;
}

ostream& operator<< (ostream& out, const SBBox& b) {
    out << "[(" ;
    for (int i=0;i<b.dimensionality;i++) {
      out << b.lowLim[i] ;
      if (i<b.dimensionality-1 ) out << "," ;
    }
    out << ")-(" ;
    for (int i=0;i<b.dimensionality;i++) {
      out << b.highLim[i] ;
      if (i<b.dimensionality-1 ) out << "," ;
    }
  out << ")]" ;
  return out ;
};

