/**
 *  @file pointset.h
 *  @brief Set of points, used as a content of an tree node, declaration.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko, changes by Jan Kybic
 *  @date 17.01.2006
 */

#ifndef POINTSET_H
  #define POINTSET_H

#include <iostream>

#include "config.h"

#include <math.h> 
#include <stdlib.h>

using namespace std ;
struct SBBox;



/**
 *  @brief Data structure for holding the pointers to the data points.
 *
 *  Is implemented as a double-linked list of pointers. The whole list
 *  is created in one array to optimize the memory management. The allocation
 *  and deallocation of this array does only the initial set created for the
 *  root node of the tree with the CPointSet(const #JKDataType* inData,
 *  #DimensionType inDimOfData, int inSizeOfData, #DimRangeType inDimRange)
 *  constructor.
 */
class CPointSet {

  /** @brief List node structure holding the element and neighbor pointers. */
  struct SListNode {
    SListNode* previous; /**< @brief Previous node in the list. */
    PointType content; /**< @brief Content of this element. */
    SListNode* next; /**< @brief Next node in the list. */
  };
  SListNode* first; /**< @brief First list node pointer. */
  SListNode* last; /**< @brief Last list node pointer. */


  /**
   *  @brief Memory pool for the whole list.
   *
   *  This is allocated only in the root node, creating all needed
   *  SListNode instances. It's deallocated in #~CPointSet().
   */
  SListNode* memPool;

  /** @brief Size of the set. */
  int size;


public:

  /**
   *  @brief Iterator class for the CPointSet.
   *  @note The iterator is bidirectional. Both increment and decrement
   *  operators can be used.
   */
  class iterator {
  public:

    /** @brief Constructor used in iterator functions. */
    iterator(SListNode* p) : node(p) {};

    ~iterator() {}; /**< @brief Empty destructor. */

    /** @brief Assignment operator. */
    iterator& operator=(const iterator& other) {
      node = other.node;
      return(*this);
    };

    /** @brief Equality operator. */
    bool operator==(const iterator& other) {
      return(node == other.node);
    };

    /** @brief Non-equality operator. */
    bool operator!=(const iterator& other) {
      return(node != other.node);
    };

    /** @brief Prefix increment operator. */
    iterator& operator++() {
      if(node != 0) {
        node = node->next;
      }
      return(*this);
    };

    /** @brief Postfix increment operator. */
    iterator operator++(int) {
      iterator tmp(*this);
      node = node->next;
      return tmp;
    };

    /** @brief Prefix decrement operator. */
    iterator& operator--() {
      if(node != 0) {
        node = node->previous;
      }
      return(*this);
    };

    /** @brief Postfix decrement operator. */
    iterator operator--(int) {
      iterator tmp(*this);
      node = node->previous;
      return tmp;
    };

    /** @brief Dereference operator. */
    PointType& operator*() const {
      return(node->content);
    };

    /** @brief Returns the pointer to element, to which is pointing. */
    SListNode* toPointer() {
      return node;
    };

  private:

    /** @brief Pointer to the element, to which is pointing. */
    SListNode* node;
  };

  /** @brief Returns the first element iterator. */
  iterator begin() const {
    return iterator(first);
  };

  /** @brief Returns the last element iterator. */
  iterator rbegin() {
    return iterator(last);
  };

  /**
   *  @brief Returns end iterator.
   *
   *  This iterator is used to check other iterators for reaching the end of
   *  list by incrementing. It's the same functionality as by STL iterators -
   *  if an iterator is equal (==) to end iterator, it's no more pointing to
   *  an element of the list but behind the last one.
   */
  iterator end() const {
    return iterator(0);
  };

  /**
   *  @brief Returns reverse end iterator.
   *
   *  This iterator is used to check other iterators for reaching the begining
   *  of the list by decrementing. If an iterator is equal (==) to rend
   *  iterator, it's no more pointing to an element of the list but before
   *  the first one.
   */
  iterator rend() {
    return iterator(0);
  };

  /** @brief Default constructor creates an empty set. */
  CPointSet();

  /**
   *  @brief Constructor used to create the set of pointers to data.
   *
   *  @arg @c inData Pointer to the data in memory.
   *  @arg @c inDimOfData Dimensionality of the data.
   *  @arg @c inSizeOfData Size (number of points) of the data.
   */
  CPointSet(const JKDataType* inData, DimensionType inDimOfData,
    int inSizeOfData);

  /**
   *  @brief Constructor for creating child sets by cutting the parent set.
   *
   *  @arg @c inFirst Pointer to the first element of this new set.
   *  @arg @c inLast Pointer to the last element of this new set.
   *  @arg @c inSize Size (number of points) of this set.
   *
   *  @warning This constructor does not check the elements, it only sets
   *  it's arguments to newly created set. If the elements from inFirst to
   *  inLast are not chained in a list, this set becomes inconsistent!
   */
  CPointSet(SListNode* inFirst, SListNode* inLast, int inSize);

  /** @brief Destructor. Deallocates memory pool if set. */
  ~CPointSet();

  /**
   *  @brief Moves element from this set to the end of target set.
   *
   *  This operation uses 3 pointer assignments and one append() call
   *  to move the element.
   *
   *  @arg @c inElement Iterator to element, which to move.
   *  @arg @c inTarget Target set to move the element to.
   *
   *  @return Iterator to the element just after the moved one.
   *
   *  @warning There are possible problems by iterating through the set within
   *  a for loop and moving. If you, based on some predicate, move current
   *  element then the returned iterator, which is the next element is
   *  incremented by the for loop and the one element never gets evaluated.
   *  Use the while loop instead.
   */
  iterator move(iterator inElement, CPointSet& inTarget);

private:
  /**
   *  @brief Appends the element to the end of this set.
   *
   *  This operation is should be inlined and private. Only for calling
   *  from the move() method, because it does not remove the element from
   *  it's source set.
   *
   *  @arg @c inElement Iterator to element, which to append.
   */
  void append(iterator inElement);

  /**
   *  @brief  Swaps the values of two elements.
   *
   *  @arg @c inElem1,inElem2 Input elements, which values to swap.
   */
  void swapValues(iterator& inElem1, iterator& inElem2);


public:

  /**
   *  @brief Appends the elements of inSet to the end of this one.
   *
   *  This operation requires only constant number of assignments.
   *  Leaves the inList empty.
   *
   *  @arg @c inSet Input set, from which to take the elements.
   */
  void splice(CPointSet& inSet);

  /** @brief Returns the middle element iterator. */
  iterator findMiddle();

  /**
   *  @brief Checks the points if they are inside the bounding box.
   *
   *  'outfallers' are moved to the inTarget set, and a new tight bbox
   *  for this set is calculated, calling createTightBox() method.
   *
   *  @arg @c inBBox Bounding box, which all points should lie inside.
   *  @arg @c inDimRange Dimension range to work with.
   *  @arg @c inTarget Target set to which 'outfalling' points are moved.
   *
   *  @return The number of points moved to the target set.
   */
  int update(SBBox& inBBox, DimRangeType inDimRange,
    CPointSet& inTarget);

  /**
   *  @brief Finds the median in given dimension and rearranges the points
   *  around it.
   *
   *  It's an adapted implementation of quicksort-like algorithm to find
   *  kth largest element from 'Numerical recipes in C', sample found on
   *  @a http://www.library.cornell.edu/nr/bookcpdf/c8-5.pdf, the book's
   *  homepage is @a http://www.library.cornell.edu/nr/bookcpdf.html. After
   *  the algorithm the elements are rearranged so, that elements smaller as
   *  the median are before it and bigger are after it in the list.
   *
   *  @arg @c inDim Dimension in which to find the median element.
   *
   *  @return The median element iterator, the number of elements smaller
   *  than the median (including) and the number of point bigger than the
   *  median. If the median could not be found, returned iterator will be
   *  equal to end().
   */
  threeValues<iterator, int, int> median(DimensionType inDim);

  /**
   *  @brief Splits the set and returns the two children, leaving this one
   *  empty.
   *
   *  Operation finds the longest dimension of it's tight box by calling
   *  SBBox#longestDim(), in this dimension finds the median by calling
   *  median() and creates the two children sets by cutting this one in the
   *  longest dimension and median value. This set is emptied.
   *
   *  @arg @c inDimRange Dimension range to work with.
   *  @arg @c outLeftSon Reference to pointer, which should be set to the
   *  newly created left child set.
   *  @arg @c outRightSon Reference to pointer, which should be set to the
   *  newly created right child set.
   *
   *  @return Cutting value. If the set could not be
   *  properly cut, the children pointers will be 0.
   *
   *  @warning The caller needs to deallocate this two sets.
   */
  JKDataType split(DimensionType inDim,
    CPointSet*& outLeftSon, CPointSet*& outRightSon);

  /**
   *  @brief For query point finds it's nearest neighbor in this set.
   *
   *  @arg @c inQuery Iterator to the query point.
   *  @arg @c inDimRange Dimension range to work on.
   *
   *  @return Iterator to found NN and the NN distance. If the query point
   *  is multiple, CPointSet#iterator#end() iterator is the
   *  first value and the second is the multiplicity of the query point.
   */
  twoValues<iterator, JKDataType> findNN(iterator inQuery,
    DimRangeType inDimRange);

  /** @brief Returns true if this set is empty. */
  bool empty() {
    return (size == 0);
  };

  /** @brief Sets this set empty. Does not make any deallocations. */
  void setEmpty() {
    size = 0;
    first = 0;
    last = 0;
  };

  /** @brief Return the actual size of this set. */
  int getSize() {
    return size;
  };

#if 0
  void assert_not_none() const { 
    int dim=1 ;
    for (CPointSet::iterator it=begin();it!=end();++it) {
      for (int i=0;i<dim;i++) {
        if (isnan((*it)[i])) {
          cerr << "CPointSet contains a NaN, exiting.\n" ;
          // cerr << *this << "\n" ;
          exit(-1) ;
        }
      }
    }
  }
#endif

} ;

ostream& operator<< (ostream& out, const CPointSet& p) ;



#endif // ifndef POINTSET_H
