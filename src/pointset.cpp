/**
 *  @file pointset.cpp
 *  @brief Set of points, used as a content of an tree node, implementation.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 17.01.2006
 */

#include <assert.h>

#include "pointset.h"
#include "bbox.h"

#include <iostream>

using namespace std ;

CPointSet::CPointSet() : first(0), last(0), memPool(0), size(0) {
};

CPointSet::CPointSet(const JKDataType* inData, DimensionType inDimOfData,
  int inSizeOfData) : first(0), last(0), memPool(new SListNode[inSizeOfData]),
                      size(inSizeOfData) {
  assert (inSizeOfData>=1) ;
    

    // create list
    memPool[0].previous = 0;
    memPool[0].content = inData;
    memPool[0].next = &memPool[1];
    first = memPool;
    int offset = inDimOfData;
    for(int i = 1; i < inSizeOfData - 1; ++i) {
      memPool[i].previous = &memPool[i - 1];
      memPool[i].content = inData + offset;
      memPool[i].next = &memPool[i + 1];
      offset += inDimOfData;
    }
    if (inSizeOfData>1) {
      memPool[inSizeOfData - 1].previous = &memPool[inSizeOfData - 2];
      memPool[inSizeOfData - 1].content = inData + offset;
      memPool[inSizeOfData - 1].next = 0;
    } else {
      memPool[0].next = 0;
    }
    last = &memPool[inSizeOfData - 1];
};

CPointSet::CPointSet(SListNode* inFirst, SListNode* inLast, int inSize) :
    first(inFirst), last(inLast), memPool(0), size(inSize) {
      inLast->next = 0;
      inFirst->previous = 0;
};

CPointSet::~CPointSet() {
  delete[] memPool;
};

CPointSet::iterator CPointSet::move(iterator inElement,
  CPointSet& inTarget) {
    iterator afterMoved = inElement;
    ++afterMoved;
    SListNode* toMove = inElement.toPointer();
    SListNode* prevLN = toMove->previous;
    SListNode* nextLN = toMove->next;
    if(prevLN != 0)
      prevLN->next = nextLN;
    if(nextLN != 0)
      nextLN->previous = prevLN;
    if(first == toMove) {
      first = nextLN;
    }
    if(last == toMove)
      last = prevLN;
    --size;
    inTarget.append(inElement);
    return afterMoved;
};

inline void CPointSet::append(iterator inElement) {
  SListNode* toAppend = inElement.toPointer();
  toAppend->previous = last;
  toAppend->next = 0;
  if(last != 0)
    last->next = toAppend;
  last = toAppend;
  if(first == 0)
    first = toAppend;
  ++size;
};

inline void CPointSet::swapValues(iterator& inElem1, iterator& inElem2) {
  PointType tmp = *inElem1;
  *inElem1 = *inElem2;
  *inElem2 = tmp;
};

void CPointSet::splice(CPointSet& inSet) {
  if(inSet.size > 0) {
    if(size > 0) {
      last->next = inSet.first;
      inSet.first->previous = last;
      last = inSet.last;
    } else {
      first = inSet.first;
      last = inSet.last;
    }
    size += inSet.size;
    inSet.setEmpty();
  }
};

CPointSet::iterator CPointSet::findMiddle() {
  if(size == 0) {
    return end();
  } else if(size < 3) {
    return begin();
  } else if(size == 3) {
    return ++begin();
  } else {
    iterator middle = begin();
    int diff = size >> 1;
    while(diff--)
      ++middle;
    return middle;
  }
};

int CPointSet::update(SBBox& inBBox, DimRangeType inDimRange,
  CPointSet& inTarget) {
    int counter = 0;
    if((inBBox.applyFlag) && (size > 0)) {
      iterator iter = begin();
      while(iter != end()) {
        if(inBBox.isOutside(*iter, inDimRange) >= 0) {
          iter = move(iter, inTarget);
          ++counter;
        } else
          ++iter;
      }
    }
    return counter;
};

threeValues<CPointSet::iterator, int, int> CPointSet::median(
  DimensionType inDim) {

  // assert_not_none() ;

  if(size > 1) {
      // initializing local variables
      int halfIndex = (size >> 1)  /* + 1 */ ;
      iterator from = begin();
      int fromIndex = 1;
      iterator afterFrom = from;
      ++afterFrom;
      iterator mid = findMiddle();
      iterator to = rbegin();
      iterator setEnd = end();
      iterator setREnd = rend();
      iterator setBegin = begin();
      int toIndex = size;

      // main loop
      for (;;) {
        //        cerr << "fromIndex=" << fromIndex << " toIndex=" << toIndex << "\n" ;

        // active partition contains 1 or 2 elements, finish the algorithm
        if (toIndex <= fromIndex + 1) {
          // case of 2 elements
          if ((toIndex == fromIndex + 1) && ((*from)[inDim] > (*to)[inDim])) {
            swapValues(from, to);
          }
          // find the median
          // iterator median = to;
          // int medianIndex = toIndex;
          iterator median = from;
          int medianIndex = fromIndex;
          // if there's a continuous partition of the same value,
          // choose as the median last of this elements
          JKDataType medianVal = (*median)[inDim];
          while((median != setEnd) && (*median)[inDim] == medianVal){
            ++median;
            ++medianIndex;
          }
          // if the median-equal values are to the end of the set, try to go
          // down
          if(median == setEnd) {
            median = to;
            medianIndex = toIndex;
            while((median != setBegin) &&
              ((*median)[inDim] == medianVal)) {
                --median;
                --medianIndex;
            }
            // if the whole set contains only one value (should not happen,
            // because the median is called only if non-zero longest dimension
            // is found) return zero result
            if(median == setBegin)
              return threeValues<iterator, int, int>(setEnd,0,0);
            else {
              // run to the beginning of the set and find and move all elements
              // equal to the former median
              iterator iter = median;
              while((--iter) != setREnd) {
                if((*iter)[inDim] == medianVal) {
                  swapValues(iter, median);
                  --median;
                  --medianIndex;
                }
              }
            }
          } else {
            --median;
            --medianIndex;
          }
          return threeValues<iterator, int, int>
            (median, medianIndex, size - medianIndex);

        // the partitioning
        } else {
          // choose median of left, center, and right elements as pivot
          swapValues(mid, afterFrom);
          if ((*from)[inDim] > (*to)[inDim]) {
            swapValues(from, to);
          }
          if ((*(afterFrom))[inDim] > (*to)[inDim]) {
            swapValues(afterFrom, to);
          }
          if ((*from)[inDim] > (*(afterFrom))[inDim]) {
            swapValues(from, afterFrom);
          }
          JKDataType pivotVal = (*(afterFrom))[inDim];

          // initialize pointers for iterating the partition
          iterator i = from;
          int iIndex = fromIndex;
          iterator j = to;
          int jIndex = toIndex;
          iterator leftMid = from;
          iterator rightMid = to;

          // if end of partition has the same value as pivot, try to
          // find other end element
          bool endMedian = false;
          iterator newTo = to;
          int newToIndex = toIndex;
          while((*to)[inDim] == pivotVal) {
            while((newTo != from) && ((*newTo)[inDim] == pivotVal)) {
              --newTo;
              --newToIndex;
            }
            // all points have the same value
            if(newTo == from) {
              from = to;
              fromIndex = toIndex;
              endMedian = true;
              break;
            // set new from, pivot and to values
            } else {
              if((*newTo)[inDim] > pivotVal) {
                swapValues(to, newTo);
                j = to;
                jIndex = toIndex;
              } else {
                swapValues(newTo, afterFrom);
                j = newTo;
                jIndex = newToIndex;
                if((*afterFrom)[inDim] < (*from)[inDim])
                  swapValues(from, afterFrom);
                pivotVal = (*afterFrom)[inDim];
              }
            }
          }

          if(!endMedian) {
            // iterate to cross
            // cout << "iterate to cross\n" ;
            // cout << "pivotVal=" << pivotVal << "\n" ;
            for (;;) {
              // scan up to find element > pivot
              while ((i != to) && ((*i)[inDim] <= pivotVal)) {
                ++i;
                ++iIndex;
                if(iIndex % 2)
                  ++leftMid;
              };
              // scan down to find element <= pivot
              while ((*j)[inDim] > pivotVal) {
                --j;
                --jIndex;
                if(jIndex % 2)
                  --rightMid;
              };
              if(jIndex <= iIndex) {
                // pointers crossed, partitioning complete
                break;
              }
              swapValues(i, j);
            };
            // cerr << "iIndex=" << iIndex << " jIndex=" << jIndex << " halfIndex=" << halfIndex << "\n" ;
            // create new partition
            swapValues(afterFrom, j);
            if(jIndex > halfIndex) {
                to = j;
                toIndex = jIndex;
                mid = leftMid;
            } 
            else if(jIndex < halfIndex) {
                from = i;
                fromIndex = iIndex;
                afterFrom = from;
                ++afterFrom;
                mid = rightMid;
              }
            else { // jIndex==halfIndex
              to = j;
              toIndex = jIndex;
              from = j;
              fromIndex = jIndex;
            } 
          }
        }
      }
    } else {
      return threeValues<iterator, int, int>(end(),0,0);
    }
};

JKDataType CPointSet::split(DimensionType inDim,
  CPointSet*& outLeftSon, CPointSet*& outRightSon) {
    // find the median and rearange the points around it
    threeValues<iterator, int, int> cutParams = median(inDim);
    // create the two cuts
    if(cutParams.first != end()) {
      iterator med = cutParams.first;
      JKDataType cutValue = (*med)[inDim];
      // cerr << " median returned cutValue=" << cutValue << " size1=" << cutParams.second << " size2=" << cutParams.third << "\n" ;

      SListNode* leftLast = med.toPointer();
      ++med;
      SListNode* rightFirst = med.toPointer();
      outLeftSon =
        new CPointSet(first, leftLast, cutParams.second);
      outRightSon =
        new CPointSet(rightFirst, last, cutParams.third);
      // empty this one
      setEmpty();
      return cutValue;
    } else {
      // cerr << " median failed.\n" ;
      return 0; }
};

// #define INCOMPLETEDISTANCE 1

twoValues<CPointSet::iterator, JKDataType>
  CPointSet::findNN(iterator inQuery, DimRangeType inDimRange) {
      iterator actualNN = end();
      JKDataType actualDistance = INFINITY;
      int multiplicity = 1;
      iterator setEnd = end();
      for(iterator iter = begin(); iter != setEnd; ++iter) {
        if(*iter != *inQuery) {
#if INCOMPLETEDISTANCE
          JKDataType dist = SBBox::distanceLim(*inQuery, *iter, inDimRange,actualDistance);

          // JKDataType dist = 0. ;
          // PointType inPoint1=*inQuery ;
          // PointType inPoint2=*iter ;
          // for(int i = inDimRange.first; i <= inDimRange.second; ++i) {
          //   JKDataType ddist = fabs(inPoint1[i] - inPoint2[i]);
          //   if(ddist > dist) {
          //     dist = ddist;
          //     // this point cannot be the closest one
          //     if (ddist > actualDistance)
          //       goto no_good_point ;
          //   }          
          // }
#else
          JKDataType dist = SBBox::distance(*inQuery, *iter, inDimRange);
#endif
          if(dist <= 0.) {
            ++multiplicity;
          } else {
            if(dist < actualDistance) {
              actualDistance = dist;
              actualNN = iter;
            }
          }
        }
#if INCOMPLETEDISTANCE
        // no_good_point: ;
#endif
      } // end of the for cycle
      if(multiplicity > 1)
        return twoValues<iterator, JKDataType>(end(), multiplicity);
      else
        return twoValues<iterator, JKDataType>(actualNN, actualDistance);
};

ostream& operator<< (ostream& out, const CPointSet& p) {
  out << "[" ;
  int dim=5 ;
  for (CPointSet::iterator it=p.begin();it!=p.end();++it) {
    out << "(" ;
    for (int i=0;i<dim;i++) {
      out << (*it)[i] ;
      if (i<dim-1 ) out << "," ;
    }
    out << ")" ;
  }
  out << "]" ;
  return out ;
};
