/**
 *  @file bbftree.h
 *  @brief Data structure bbftree (Best Bin First k-D tree) declaration.
 *  @note Part of the IncrEst (Incremental Estimator) project.
 *  @author Ivan Vnucko
 *  @date 14.12.2005
 */

/**
 *  @mainpage IncrEst - Incremental Entropy Estimator, version 1.0
 *
 *  @section Intoduction
 *
 *  This project implements the Kozachenko - Leonenko Nearest
 *  Neighbor Entropy Estimator (see "L. Kozachenko and N. Leonenko.
 *  On statistical estimation of entropy of random vector. Problems
 *  Infor. Transmiss., 23(2):95�101, 1987" and "Nonparametric
 *  entropy estimation: An overview. Beirlant, J. Dudewicz, E. J.
 *  Gyoerfi, L. Van der Meulen, E. C., International Journal of
 *  Mathematical and Statistical Sciences, 1997, Vol. 6, Numb. 1,
 *  pages 17-40" for overview).
 *  It uses a kD tree data structure with Best Bin First searching
 *  strategy for All Nearest Neighbor search.
 *
 *  @section Compilation
 *
 *  Project was succesfully compiled with GCC 3.4.2 on Win32
 *  platform using MinGW (www.mingw.org) and Microsoft Visual C++
 *  Toolkit 2003 (msdn.microsoft.com/visualc/vctoolkit2003) and
 *  is in authors best opinion platform independent. Uses only
 *  vector and queue classes from STL and math unit from C standard
 *  library, which should be shipped with each decent C++
 *  compiler. To compile and run the experiments (experiments.cpp)
 *  the newran02 library (www.robertnz.net/nr02doc.htm) is needed,
 *  however this is not a necessary component to use the estimator.
 *  Project can be compiled as a static library and linked to the
 *  application with the headers bbftree.h and klentropyestim.h
 *  included. Both headers are needed, because the components are
 *  independent. Actually the KLEntropyEstim object is only defined
 *  in the header klentropyestim.h for it's simple implementation.
 *
 *  @section Usage
 *
 *  There are two main components, the BBF kD tree data structure
 *  implementation (declared in bbftree.h) and the KL entropy
 *  estimator (only the definition in klentropyestim.h). The
 *  standard usecase is to prepare the data in one block of memory,
 *  properly aligned (see the .\\docs documentation for CBBFTree
 *  class constructor), construct the CBBFTree object with pointer
 *  to the data and its size and dimensionality (and optional
 *  parameters), then run the Build() method to build the data
 *  structure. Entropy is estimated by calling one of the IterAll...
 *  methods with a instance of the FKLEntropyEstim class as a
 *  parameter. After the IterAll.. method run, the FKLEntropyEstim
 *  holds the estimated entropy value. It can be asked for with
 *  the FKLEntropyEstim#getEntropy() method or cleared for reuse
 *  with the FKLEntropyEstim#reset()
 *  method. For small changes of data, during an optimization
 *  process for example, the CBBFTree#Update() method should run 
 *  faster than rebuilding the whole tree with CBBFTree#Build().
 */

#ifndef BBFTREE_H
  #define BBFTREE_H

#include <iostream>
#include "config.h"
#include "bbftreenode.h"

using namespace std ;

/**
 *  @brief The kD-tree with BBF approach for fast approximate all-NN search.
 *
 *  The basic class providing all-NN searches on given input data. Application
 *  should contruct one instance for input data, and call the
 *  Build() method to build the structure. For small changes in
 *  input data (during an optimization process for example)
 *  call the Update() method, which should be faster then a complete build
 *  in such case.
 *
 *  @see CBBFTree#CBBFTree(const #JKDataType* inData,
 *  #DimensionType inDimOfData, int inSizeOfData)
 *  or CBBFTree#CBBFTree(const #JKDataType* inData,
 *  #DimensionType inDimOfData, int inSizeOfData, int leafSize,
 *  #DimensionType dmin, DimensionType dmax, float delta, int maxVisit)
 *  for input data layout requirements.
 *
 *  @warning The constructors of this class assume proper layout of the input
 *  data and DO NOT make any checks. If the data is not in the right form the
 *  program may crash!
 */
class CBBFTree {

  /** @brief Pointer to the memory block containing the data.*/
  const JKDataType* data;
  DimensionType dimOfData; /**< @brief Dimensionality of the data. */
  int sizeOfData; /**< @brief Size (number of points) of the input data. */

  /**
   *  @brief Structure holding all optional tree parameters.
   */
  struct SBBFTreeParams {

    /**
     *  @brief How many points should each leaf of the tree hold.
     *
     *  It's not exact,
     *  the tree will typically hold between [leafSize/2] and
     *  [leafSize] leafs.
     */
    int leafSize;

    /** @brief Range of dimensions, to take into account. */
    DimRangeType dimRange;

    /**
     *  @brief Allowed imbalance in this node, before his subtree is rebuild.
     *
     *  Used in the Update operation - neither of the 2 children
     *  can contain more than ((0 <= delta < 0.5) + 0.5) * points_in_subtree
     *  points.
     */
    float delta;

    /** @brief Maximum number of points that are visited by the NN search. */
    int maxVisit;

    bool useTbb ;

    /**
     *  @brief Full constructor.
     *
     *  Private for the #CBBFTree,
     *  used in his constructor.
     */
    SBBFTreeParams(DimensionType inDimOfData, int inLeafSize,
      DimensionType inDmin, DimensionType inDmax,
                   float inDelta, int inMaxVisit,bool inUseTbb) :
        leafSize(inLeafSize),
        dimRange((inDmin >= 0 && inDmin < inDimOfData) ? inDmin : 0,
          ((inDmax >= 0) && (inDmax < inDimOfData) && (inDmax >= inDmin) ?
          inDmax : inDimOfData - 1)),
        delta(inDelta),
          maxVisit(inMaxVisit),
          useTbb(inUseTbb) {
    };

    /**
     *  @brief Dimensionality constructor.
     *
     *  Heuristics for the tree parameter values.
     *  Private for the #CBBFTree, used in his constructor.
     */
    SBBFTreeParams(DimensionType inDimOfData) :
      leafSize((40 > (2 * inDimOfData + 9) ? 40 : (2 * inDimOfData + 9))),
      dimRange(0, inDimOfData - 1), delta(0.3),
        maxVisit((inDimOfData <= 2) ? 100 : ((inDimOfData <= 10) ? 1000 : 10000)),   
        useTbb(true)
    {};

  };

  /** @brief All optional parameters of the tree. */
  SBBFTreeParams params;

  /** @brief Root node of the tree. */
  CBBFTreeNode root;

  /** @brief True if the tree is already built with the #Build() call. */
  bool built;

public:

  /**
   *  @brief Tree constructor, arguments describe the input data.
   *
   *  The input data should be aligned in one block of memory,
   *  the elements of type #JKDataType of this block are read as
   *  array of arrays of data points. So the first point are elements
   *  0 - (inDimOfData - 1), the second inDimOfData - (2*inDimOfData - 1)
   *  and so on.
   *
   *  @arg @c inData Pointer to the memory block containing the input data.
   *  @arg @c inDimOfData Dimensionality of the input data.
   *  @arg @c inSizeOfData Size (number of points) of the input data.
   */
  CBBFTree(const JKDataType* inData, DimensionType inDimOfData,
           int inSizeOfData);

  /**
   *  @brief Tree constructor, arguments describe the input data.
   *
   *  The input data should be aligned in one block of memory,
   *  the elements of type #JKDataType of this block are read as
   *  array of arrays of data points. So the first point are elements
   *  0 - (inDimOfData - 1), the second inDimOfData - (2*inDimOfData - 1)
   *  and so on. Additional arguments allow to set the optional parameters
   *  of the tree.
   *
   *  @arg @c inData Pointer to the memory block containing the input data.
   *  @arg @c inDimOfData Dimensionality of the input data.
   *  @arg @c inSizeOfData Size (number of points) of the input data.
   *  @arg @c leafSize Maximal number of points to be held in the leafs of
   *  the tree.
   *  @arg @c dmin,dmax Dimension range to work with.
   *  @arg @c delta Allowed imbalance coefficient.
   *  @arg @c maxVisit Maximum nodes to visit by the all-NN search.
   *
   *  @see CBBFTree#SBBFTreeParams for detailed tree parameters description.
   */
  CBBFTree(const JKDataType* inData, DimensionType inDimOfData,
    int inSizeOfData, int leafSize, DimensionType dmin, DimensionType dmax,
           float delta, int maxVisit,bool useTbb);

  /** @brief Empty destructor. */
  ~CBBFTree() {};

  /**
   *  @brief Builds the tree.
   *
   *  Root node with the initial point set is
   *  created in constructor call, this method starts the recursive process
   *  of splitting the tree nodes.
   */
  CBBFTree& Build();

  /**
   *  @brief Updates the tree.
   *
   *  If the data changed not much, this method should be
   *  faster then the whole build. This method also introduces some tolerance
   *  for inbalance of the tree, given by the delta parameter in the #params
   *  structure.
   *
   *  @arg @c inData New data pointer. Allows to use new location for the data
   *  but the dimensionality and size MUST NOT change.
   *
   *  @return The number of points which were moved and the number of points
   *  in subtrees that were rebuild. If run on an unbuilt tree, -1's are
   *  returned.
   */
  twoValues<int> Update(const JKDataType* inData = 0);

  /**
   *  @brief Iterates through all approximate NN pairs, evaluating FProcess
   *  functionoid on each.
   *
   *  @arg @c evaluator On each found pair the FProcess evaluator is called.
   *  Class used as evaluator must implement the <tt>
   *  operator()(PointType queryPoint, int multiplicity)</tt>,
   *  called for points with higher multiplicity as 1
   *  and <tt> operator()(PointType queryPoint, PointType NNpoint,
   *  JKDataType distance) </tt> for evaluating on NN pairs.
   *
   *  @return Total number of comparisons by the all-NN search. If run on an
   *  unbuilt tree, -1 is returned.
   */
  template <typename FProcess>
  int IterateAllApproxNN(FProcess& evaluator) {
    if(built)
      return root.processNode<FProcess>(evaluator, params.dimRange,
                                        params.maxVisit, false,params.useTbb);
    else
      return -1;
  };

  /**
   * findApproxNN will find a NN for a given query, calling FProcess evaluator on it. It returns the number of nodes visited or -1 on error.
   */

  template <typename FProcess>
    int findApproxNN(FProcess& evaluator, DimRangeType inDimRange,PointType query) {
    if(built) {
      if((inDimRange.first < params.dimRange.first) ||
        (inDimRange.second > params.dimRange.second))
          return -1;
      else {
        // cerr << "query=" << query << " " << *query << "\n" ;
        // cerr << "dimOfData=" << dimOfData << "\n" ;
        CPointSet pset=CPointSet(query,dimOfData,1) ;
        return root.approxNN<FProcess>(evaluator, inDimRange,
                                       params.maxVisit, pset.begin(),params.useTbb);
      }
    } else
      return -1;
  };


  /**
   *  @brief Iterates through all approximatly NN pairs, evaluating FProcess
   *  functionoid on each.
   *
   *  @arg @c evaluator On each found pair the FProcess evaluator is called.
   *  Class used as evaluator must implement the <tt>
   *  operator()(PointType queryPoint, int multiplicity)</tt>,
   *  called for points with higher multiplicity as 1
   *  and <tt> operator()(PointType queryPoint, PointType NNpoint,
   *  JKDataType distance) </tt> for evaluating on NN pairs.
   *  @arg @c inDimRange Dimension range to look the NN pairs on.
   *
   *  @return Number of comparisons. If the inDimRange parameter is outside of
   *  range used to build the tree or the tree is not built yet,
   *  no iteration is performed and -1 is returned.
   */
  template <typename FProcess>
  int IterateAllApproxNN(FProcess& evaluator, DimRangeType inDimRange) {
    if(built) {
      if((inDimRange.first < params.dimRange.first) ||
        (inDimRange.second > params.dimRange.second))
          return -1;
      else
        return root.processNode<FProcess>(evaluator, inDimRange,
                                          params.maxVisit, false,params.useTbb);
    } else
      return -1;
  };

 /**
   *  @brief Iterates through all NN pairs, evaluating FProcess
   *  functionoid on each. This version in contrast to IterateAllApproxNN()
   *  ignores the maxVisit parameter.
   *
   *  @arg @c evaluator On each found pair the FProcess evaluator is called.
   *  Class used as evaluator must implement the <tt>
   *  operator()(PointType queryPoint, int multiplicity)</tt>,
   *  called for points with higher multiplicity as 1
   *  and <tt> operator()(PointType queryPoint, PointType NNpoint,
   *  JKDataType distance) </tt> for evaluating on NN pairs.
   *  @arg @c inDimRange Dimension range to look the NN pairs on.
   *
   *  @return Total number of comparisons by the all-NN search. If the tree
   *  is unbuilt, -1 is returned.
   */
  template <typename FProcess>
  int IterateAllExactNN(FProcess& evaluator) {
    if(built)
      return root.processNode<FProcess>(evaluator, params.dimRange,
                                        params.maxVisit, true,params.useTbb);
    else
      return -1;
  };

  /**
   *  @brief Iterates through all NN pairs, evaluating FProcess
   *  functionoid on each. This version in contrast to IterateAllApproxNN()
   *  ignores the maxVisit parameter.
   *
   *  @arg @c evaluator On each found pair the FProcess evaluator is called.
   *  Class used as evaluator must implement the <tt>
   *  operator()(PointType queryPoint, int multiplicity)</tt>,
   *  called for points with higher multiplicity as 1
   *  and <tt> operator()(PointType queryPoint, PointType NNpoint,
   *  JKDataType distance) </tt> for evaluating on NN pairs.
   *  @arg @c inDimRange Dimension range to look the NN pairs on.
   *
   *  @return Number of comparisons. If the inDimRange parameter is outside of
   *  range used to build the tree or the tree is not built yet,
   *  no iteration is performed and -1 is returned.
   */
  template <typename FProcess>
  int IterateAllExactNN(FProcess& evaluator, DimRangeType inDimRange) {
    if(built) {
      if((inDimRange.first < params.dimRange.first) ||
        (inDimRange.second > params.dimRange.second))
          return -1;
      else
        return root.processNode<FProcess>(evaluator, inDimRange,
                                          params.maxVisit, true,params.useTbb);
    } else
      return -1;
  };


#if 0
  void tree_not_none() {
    root.tree_not_none() ;
  }
#endif

};

#endif // #ifndef BBFTREE_H
