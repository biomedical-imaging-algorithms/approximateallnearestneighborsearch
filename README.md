# Approximate All Nearest Neighbor Search

This repository contains code for the article

Kybic, Vnučko: [Approximate all nearest neighbor search for high dimensional entropy estimation for image registration.](https://cmp.felk.cvut.cz/ftp/articles/kybic/Kybic-SigProc2011.pdf)]. Signal Processing 92 (2012), pp.1302-1316

To compile it, you will need the [Newran02](http://www.robertnz.net/nr02doc.htm) C++ library.

It is provided in the hope that it will be useful. It may or may not work for you. I cannot provide any support. Enjoy.

[Jan Kybic](http://cmp.felk.cvut.cz/~kybic)

