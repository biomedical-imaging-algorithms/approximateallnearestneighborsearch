* Links

Locality sensitive hashing
http://lshkit.sourceforge.net/

Fast Library for Approximate Nearest Neighbors
http://www.cs.ubc.ca/~mariusm/index.php/FLANN/FLANN

On the Estimation of Differential Entropy From Data Located on Embedded Manifolds,
Mattias Nilsson, W. Bastiaan Kleijn,
IEEE Transactions on Information Theory, Vol. 53, No. 7, July 2007.

incomplete beta function
http://www.enotes.com/topic/Beta_function
http://www.jstor.org/pss/2346797
http://dlmf.nist.gov/5.12
http://dlmf.nist.gov/8.17

ASYMPTOTIC EXPANSION OF THE
INCOMPLETE BETA FUNCTION FOR
LARGE VALUES OF THE FIRST
PARAMETER Jos´e L. L´OPEZ y and Javier SESMA

* Log

** <2009-07-10 Fri>

I have initialized a bzr repository

I will go over the code, trying to understand and to spot problems.

** <2009-08-19 Wed>

bzr revision 1

Compilation fails with:

    [kybic.d620-jk ~/work/students/vnucko/increst-1.01]> make
    make[1]: Entering directory `/local/kybic_nonessential/students/vnucko/increst-1.01/src'
    g++ -O3 -Wall -c bbftree.cpp
    In file included from bbftree.cpp:9:
    bbftree.h:210: error: extra qualification 'CBBFTree::' on member 'CBBFTree'
    make[1]: *** [bbftree.o] Error 1
    make[1]: Leaving directory `/local/kybic_nonessential/students/vnucko/increst-1.01/src'
    make: *** [lib] Error 2

After correction, 'make' compiles (bzr revision 2) but 
'make experiments' fails with:

    g++  experiments.o ../lib/libincrest.a ../newran02/lib/libnewran02.a -o ../bin/increst_exp
    experiments.o: In function `experiment1()':
    experiments.cpp:(.text+0x138): undefined reference to `Random::Set(double)'
    experiments.cpp:(.text+0x15c): undefined reference to `Normal::Normal()'
    etc...

I am trying to recompile the newrand02 library
(http://www.robertnz.net), but it fails with:

    include.h:138:28: error: iostream.h: No such file or directory
    include.h:139:27: error: iomanip.h: No such file or directory

I solved with by modifying include.h

   + "using namespace std"
   - taking out ".h" in some included names, such as iostream


Now the following compiles "make -f nr_gnu.mak" and executable
./tryrand runs

Creating libnewran02.a library using

ar -rvu libnewran02.a extreal.o newran.o myexcept.o

Now "make experiments" in increst-1.01/ compiles and
and bin/experiments runs.

bzr revision 3

** <2009-08-20 Thu>

Creating ~/tex/annsearch
Creating ~/work/increst

I probably do not have the data for the experiments so I will have to
reimplement the experiments completely. 

Datasets:
        - uniform (d=1,2,3,5,10,15,20,30,40,50)
        - normal  (d=1,2,3,5,10,15,20,30,40,50)
        - linear (d=2,5,10,15,20,30,40,50)
        - Lena  (d=1,9,25)


Proposed experiments:

      - dependence of the construction and all-NN search times
        for N=10^5 and varying 'd' and 'L'
        Each experiment will be an average of 'R=10' runs.
        testleaf.m + tableleaf.m

      - times for the build and all-NN search in comparison to a
        brute-force search as a function of N=10^2..10^6

      - effect of the number of visited points M=10,10^2,..10^5

      - effect of searching for all-NN instead of repeating N times
        a one-NN search
        
      - effect of the update operation, as a function of the
        perturbation amplitude and allowed imbalance. Show the time
        gain

      - comparison with ANN for exact search

      - comparison with ANN for approximate search, times verus error
        for ANN.

We could send the paper to:

   Computational Geometry: Theory and Applications (Elsevier, impact
   0.89)

   Discrete and computational geometry (Springer, impact 0.754)

   SIAM Journal on Scientific Computing??
   http://epubs.siam.org/SISC/

   Pattern Analysis and Applications, Springer (1.367)
   http://www.springer.com/computer/computer+imaging/journal/10044

 	
   International Journal of Computational Geometry and Applications 
   http://www.worldscinet.com/ijcga/mkt/archive.shtml?2009&19

** <2009-08-21 Fri>

I will make a file allnn_exp.cpp to allow interactive evaluation of the
a-NN search. It will read from standard input the following commands
(partially inspired by ann_test.cpp from the ANN library.)

set_dim <num>            - dimensionality of the data
set_leaf_size <num>      - the size of the leaves, defaults to 10
set_max_visited <num>    - set the maximum number of visited points
set_delta <float>        - set the maximum unbalance
verbose <num>            - 0..9, verbosity=0, print only timings
read_data_pts <file>     - read points from a file
vnucko_build_tree        - builds the tree, reports time in seconds
vnucko_all_nn_search     - find all nearest neighbors, report time in seconds
brute_all_nn_search     - find all nearest neighbors, report time in seconds
quit

Not yet implemented:

check_all_nn_search      - check the all NN search using brute force,
                           report error(s).
vnucko_repeated_nn_search  - find all nearest neighbors by
                        repeatedly searching for one of them, report time in seconds


Makefile target allnn_exp.

** <2009-09-01 Tue>

Maybe considering only a range of dimensions slows things down? It is
useful for entropy calculation.

** <2009-09-02 Wed>

alnn_exp::vnuckoOneNNsearch might have to be rewritten, as
findLowerPoint will probably terminate immediately when called from the root
node

Basic skeleton written, bzr revision 3

** <2009-09-03 Thu>

allnn_exp now compiles, bzr revision 6


** <2009-09-04 Fri>

*** Experiments to test the NN search

in Matlab:`

    a=random('normal',0,1,100,2) ;
    save -ascii normal100.txt a

created test.cmd:

    set_dim 2
    read_data_pts normal100.txt
    vnucko_build_tree
    vnucko_all_nn_search
    brute_all_nn_search
    quit


*** Adding commands for ANN

I have built ANN using linux-g++. Had to add 
 #include <stdlib.h>
 #include <string.h>
to ANN.h

I have also compiled with ANN_ALLOW_SELF_MATCH (in ANN.h) set to false.
I have changed the norm to L_inf.

I will add the following commands

ann_build_tree
ann_all_nn_search

The code compiles but I get Segmentation fault (bzr version 9)
in ann_all_nn_search, but only if I try to with 10000 points. With
1000 it works...

The problem seems that there were too many identical, zero points due
to an error in the code. This is now fixed and the experiments run.

bzr revision 10

** <2009-09-07 Mon>

*** DONE : better time measurements (repeat several times if needed)

Better timings implemented in timeit.h, class BuildCBBFTree in
allnn_exp.cpp
bzr revision 11


*** DONE : experiments in Matlab, several different data sets.

gendata.m - data generation implemented

** <2009-09-10 Thu>

Writing testleaf.m, to measure dependence on the leaf size.

bzr revision 14.

There is some problem with the system() call in Matlab.
It seems to have been solved by saying
    system('unset LD_LIBRARY_PATH ; ../bin/allnn_exp <leaf.cmd >leaf.out') ;
http://www.mathworks.com/matlabcentral/newsreader/view_thread/241634


** <2009-09-11 Fri>

testleaf.m experiment

dv=[ 1 2 3 4 5 10 15 20  ] ; % dimensions

For the normal random data, small leaf sizes seem to work well, except
for dimensions 1,2,3, where bigger leaf sizes might be useful.

>> [t,i]=min(srtm+bldtm) ; lv(i)   

ans =

    16    32    16     2     1     2     1     2

The building is always faster for larger leaf size:

>> [t,i]=min(bldtm) ; lv(i)     

ans =

    32    32    32    32    32    32    32    32

For searching alone, small leaf sizes work well:

>> [t,i]=min(srtm) ; lv(i) 

ans =

     8    16     1     2     1     2     1     2
    
** <2009-09-14 Mon>

The times measured are very variable. We will need to repeat the
experiments several times to counter this. I have started with 10
repetitions in testleaf.m 
bzr revision 15

Preparing a LaTeX table - tableleaf.m

tableleaf.tex includes leaf.tex generated by tableleaf.m

bzr revision 17

A table is generated but there is no place for the standard
deviations, I am taking it out.
    
** <2009-09-15 Tue>

*** Running testleaf on cmpgrid-64 with:

    nohup sh -c 'echo testleaf | matlab -nodesktop -nojvm'  &

The dependence of the search time on L is complicated.

*** DONE : Experiment 1: Evaluate testleaf results (when it finishes)

    Results look reasonable even though they are not monotoneous.

*** DONE : Experiment 2: times for the build and all-NN search in comparison
    with a brute-force search as a function of N=10^2..10^6
    
    testndep.m

    bzr revision 18

    Starting testndep on cmpgrid-71 with:
    
    nohup sh -c 'echo testndep | matlab -nodesktop -nojvm 2>&1 >ndep.log' &

    It ended with segmentation fault for N=10^6 for ann_all_nn_search 

    It seems that the problem was too large arrays as automatic
    (stack) variables in SearchANNTree::op() in allnn_exp.

    bzr revision 19.

    Restarting on cmpgrid-71

    In the current testndep1.mat, the search value srtm(8,5) is zero - ERROR.




*** DONE : Experiment 4: All-NN versus NNN search for different N.

  I will implement CBBFTree::findApproxNN, to find one approximate
  nearest neighbor and apply FProcess on the pair found.
  This will be used in allnn_exp: repeatedNNsearch
  

  New allnn_exp command - show_ann - to show all NN calculated via the
  different methods.

  Implemented class FShow. Does not compile yet. 
  bzr revision 20

  it compiles now, bzr revision 23

  creating stest.cmd
  brute force=BBF=ANN results
  However, we get segmentation fault for BBF NNN 

  There was an error in pointset.cpp, the constructor CPointSet
  did not work correctly for size 1. bzr revision 24

  BBF NNN now runs but the results are incorrect, bzr revision 25.

  The problem seem to have been that the iterator for the CPointSet
  was not copy-constructed properly when passed to findLowerDistance 
  from approxNN (in bbftreenode.h). I solved this by passing the iterator
  by reference. Also, the test in findNN (pointset.cpp) for identity
  of points was changed so that the pointers are compared, not the iterators.
  Hopefuly this will not add that much overhead.

  bzr revision 26

  The experiment will be part of Experiment 2.  


** <2009-10-01 Thu>

*** I will make BBF NNN search measurable (the time it takes).
    Command vnucko_nnn_search

*** BBF NNN works but is relatively slow. I have made on optimization,
    skipping nodes from the queue that are worse than the current
    minimum. But it did not help much.

    I also had to correct processNode in bbftreenode.h, since it did
    not handle correctly leafs with one point.

    bzr revision 28

    For N=1000    BBF NNN 0.0195       0.00139 for AllNN.
        N=10000   BBF NNN 0.768 versus 0.0142  for AllNN 
        N=100000  BBF NNN 47s          0.152   for AllNN

    The disadvantage of BBF NNN disappears for larger d, i.e. d=15,
    N=1000, where however ANN is also fast.

    Vnucko All-NN search took 0.0509 seconds
    Repeated BBF NN search took 0.0784 seconds
    Brute force NN search took 0.0944seconds.
    ANN search took 0.0149 seconds.

    From the ndep experiments, ANN and BBF are very close, there is an
    crossover for some moderate N, so that is fine.

** <2009-10-02 Fri>

*** Extending testndep to BBF NNN search, editing testndep.m, ndepgraph.m,
    
    Note that the grid computers are not equal. Let's stick to cmpgrid-71.  
   
    Starting testndep on cmpgrid-71

    NNN search takes about 15h...

    The improvements between BBF and ANN are much larger in lower
    dimensions and get progressively less significant in higher
    dimensions. Maybe we should try on other distributions or
    just to show the relative difference.

**** DONE : evaluate results when finished


*** DONE : Experiment 3: Entropy estimation error versus time for ANN and our method (KNN). 
  by changing the number of visited leafs and epsilon for ANN

  First the number of visited leafs.

  New command "vnucko_entropy" - report time and entropy.

  New command "set_ann_epsilon" for ANN.
  
  New command "ann_entropy" - report time and entropy.

  Script entropy.cmd. In dimension 2, the entropy should be 
  2*(0.5+0.5*log(2*pi)=2.8379

  The code seems to work, bzr revision 31

  Matlab scripts testentrbbf.m testentrann.m will test for single N=10000 and 
  several d=2,5,10,20, showing the time versus error accuracy

  Starting both testentrbbf and testentrann on cmpgrid-71

**** DONE : See the results

  They have finished and I copied the mat files to the laptop.

  Visualize the results using entrgraph.m

** <2009-10-05 Mon>

*** I do not understand why NNN search takes so long.
    So I am adding debugging prints to find out.
    Uses ntest.cmd

    The queue seems to be sorted in the incorrect order!!!
    Corrected in bbftreenode.h
    bzr revision 34

    Restarting testndep on cmpgrid-71
    Restarting testentrbbf and testentrnnn on cmpgrid-71
    
*** For experiment 3, ANN outperforms BBF for N=1e4. I will try to
    make the experiment for 1e5, later perhaps for 1e6.

**** DONE : See if it works better.

    For 1e5 BBF does not work better except for d=2, d=5.
    Saving to testentrbbf1e5.mat testentrann1e5.mat

    I will try for 1e6.

    Saving to testentrbbf1e6.mat testentrann1e6.mat
    
    Unfortunatelly, ANN seems to be faster for the same precision for
    d=10 etc.    Only at very high accuracy is BBF better. Moreover the curves are
    not monotoneous.

    Probably N=1e4 results were just fine.
 
    Try with different distributions.  
      




*** DONE : Experiment 5: Effect of the different distributions.

    Compare the total time (build+search) for the different distributions
    and different N for BBF and ANN. Show in a table format.

    Matlab script testdistr.m

    starting on cmpgrid-71 with output to matlab/ddistr.log

    bzr revision 35

** <2009-10-06 Tue>

*** DONE : Script tabledistr.m to process testdistr results.

    There was an error in testdistr.m, so that "srtm" will have to be
    recalculated from srtmr.

    The results look promissing, BBF is almost always faster than ANN.

*** DONE : The results do not seem to be consistent with the ndep experiment,
    Running them again. Saved to testdistr4.mat
    It is better but ANN seems to be better in
    several cases, so I will try with n=1e6.

    Done, saved to testdistr5.mat

*** DONE : The results are not bad. I will rerun the experiment 10 times to
    get more smoothed results.

    Starting on February 12, results expected on Monday February 15.


** <2009-10-15 Thu>

*** DONE : Find out why ANN is better for approximate searches?

    Trying on 'imgneighb' data. ANN segment faults.

    Preparing gdb version of ANN with "make linux-noopt"
    The segmentation error is somewhere where the ANN tree is built...
    Trying again with the "normal" dataset.

    Restarting for d=5,10, N=1e5

    Restarting on cmpgrid-71 with nrep=100

*** TODO: Experiment 6: Effect of the update operation

    Modifying allnn_exp to allow update.

    New command "set_delta", "vnucko_update_tree"

    bzr revision 37
   
    We have a problem, as the tree is destroyed by updating.
    Therefore, we will not repeat tree update.
    bzr revision 38

    Matlab test script testupdate.m

    I am getting a Segmentation fault while updating the tree.

    Corrected. bzr version 40

    Starting testupdate on cmpgrid-71

    I suspect that bbftreenode:rebuildNode is wrong, to be checked.
    It seems never to finish.
    In pointset.cpp looking for a median, when pivotVal=NaN, we have an
    infinite loop.
    
    Added an automatic check for Nan to median().

    The problem seems to be that the "data" should not be deleted when
    new data is read. Adding command "keep_data" that will simply
    forget the pointer to the data block without releasing it.
    This is a memory leak but since we shall use it only once
    per experiment, it is not a problem.

    bzr revision 44

    Starting testupdate.log on cmpgrid-71 

    Saved results to testupdate1.mat

    tableupdate.m to show a table of the results of testupdate.m

    restarting testupdate with N=1e5, d=5

    storing to testupdate3.mat

    Results not bad but there is not much difference between search
    times for different delta. Try again with N=1e6. 

    There seems to be an error somewhere as rebuild is never performed.

    Error corrected - the data points need to be changed in place for
    update in allnn_exp. bzr revision 46

    Restarting testupdate.m on cmpgrid-71

    Results to testupdate4.mat

    The search on an update tree seems to take longer than on the
    original tree, even for delta=0. This is strange. I will try to
    measure the tree building and search after the update.

    Restarting on cmpgrid-71

    Results saved to testupdate5.mat.  

    Doubts - why are there rebuilts when delta=0.5???
              - maybe when some of the subtrees is empty.  

           - why is a search on a rebuilt tree faster when delta=0?,
             i.e. the tree is rebuilt completely

    I will try do two independent runs of allnn_exp in testupdate.m
    to avoid cache effects. Does not help.

    Median search does not work for size=2, making changes in pointset.cpp
 
    Rebuilding and fresh building does not give two identical trees -> investigate.

    Changed the median implementation.

    It seems that the tight box was not updated properly when
    rebuilding the tree. 

    I have checked the search for 100 points, the search seem to be
    exactly the same. Perhaps the differences are just because of
    memory layout/fragmentation.

    Rerun results on cmpgrid-71. Results look good. bzr revision 52.

*** DONE : Rerun on cmpgrid-65, complete separable all_nn_exp invocations.
    Check results

*** DONE : redo distdistr

    saved old testdistr.mat to testdistr1.mat

    restarting testdistr na cmpgrid-71

    It takes a lot of time. I will limit myself to N=1e5

    storing testdistr2.mat, restarting
  
    storing to testdistr3.mat

    ANN seems to have a problem with imgneighb, maybe because of
    repeated values. I will add a small noise.

    Restarting testdistr.m on cmpgrid-71
 
    Results look good, BBF works best in all cases.

** <2009-10-29 Thu>

*** Will redo testleaf. Saved old results to testleafres1.mat. 
    Starting testleaf on cmpgrid-71.

**** DONE : Evaluate the results

    Saved to testleafres2.mat. The results are almost OK, except at 
    dimensions 1,2 probably because of random fluctuations L=15 seems
    to work better than L=20, which would be the expected L.

    The recommended values are therefore:
  if d<=4, % choose leaf size
   l=20 ;
  elseif d>=10
    l=4 ;
  else
    l=round(16-6/5*d)
  end ;

** <2009-10-30 Fri>

*** Restarting testentrann testentrbbf on cmpgrid-71 with improved choice of L
    and n=1e6 and nrep=100.

*** DONE : Evaluate restults when finished (entrngraph.m)

    For large N, ANN fails.

    We might try to show BBF results in one graph.

    Rerunning with N=1e5 and dimensions 3,5,7,9,11.

    I have added a graph to entrgraph to show the results for all 'd'
    in a single graph. Another obervation - ANN cannot go to very
    short times.

    bzr revision 53

    Results OK, however I will rerun with nrep=100 and up to d=20 and
    a larger range of epsilon. running on cmpgrid-71

    bzr revision 54

**** DONE : Analyze results of testentrbbf testentrann
    
    Results look useful, even though a little cluttered.
    Saved to testentrbbf2.mat testentrann2.mat

    BBF works better for lower dimensions, but not for higher
    dimensions.

** <2009-11-04 Wed>

*** Maybe partial evaluation of the distances would help?

    Trying to find the hotspot through profiling, for n=1e5, d=20,
    max_visited=1000, leaf_size=4

    According to gpc flat profile, the most time is spent in
    SBBox:distance (68%), then std::__adjust_heap

    In the call graph analysis, most time is spent in
    findLowerDistance, which itself calls SBBox::distance

    The time taken for the search without profiling and with "-O2" is 
    404s.

    First attempt - make the distance function inline. Took 421s :-(
    
    To accelerate the testing, try N=1e4, time is 11.33s
    Chaning SBBox::distance to use fabs makes no difference.

    Try to do incomplete distance calculation, not much time
    difference (in findNN)

    What seems to take time is SBBox::distance(const PointType inPoint, DimRangeType inDimRange)
    calculating the distance to a tight box.

    Incomplete distance calculation reduces the time only to 10.46s.

    Profiling confirms that most time is spent in distanceLim
    called from pushIfBetter.

    However, using leaf_size=20 reduces the time to 2.12s!
    and for n=1e6 to 66s!

    bzr revision 58

**** DONE : Rerunning testleaf.m. Evaluate when finished.

    The results (copied to testleafres3.mat) look usable but still
    there for d=20 we get optimal L=4.

** <2009-11-05 Thu>

*** Rerunning testentrbbf with L=30, saving to testentrbbf1e5l30

    Results (entrgraph.m) look OK, presentable (after I skipped some
    of the 'd' that made non-monotoneous errors).


** <2011-03-28 Mon>

*** DONE read the article, see experiments we have, propose new experiments


** <2011-03-30 Wed>

*** Compiling newran02 with

~/work/increst/newran02$ make -f nr_gnu.mak 

Creating libnewran02.a library using

ar -rvu libnewran02.a extreal.o newran.o myexcept.o


*** Installing ANN 1.1.2

I have  compiled with ANN_ALLOW_SELF_MATCH (in ANN.h) set to false.

Using the l2 norm now.

*** Compiling allnn_exp

mkdir bin lib

make allnn_exp


*** Test NN search

in Matlab:

    a=random('normal',0,1,100,2) ;
    save -ascii normal100.txt a

created test.cmd:

    set_dim 2
    read_data_pts normal100.txt
    vnucko_build_tree
    vnucko_all_nn_search
    brute_all_nn_search
    quit

timings are: 

Building the tree took 1.64e-05 seconds.
Vnucko All-NN search took 3.3e-05 seconds.
Brute force NN search took 6.83e-05seconds.

to check if the results are correct, I use test.cmd

set_dim 2
read_data_pts normal100.txt
vnucko_build_tree
vnucko_all_nn_search
brute_all_nn_search
ann_build_tree
ann_all_nn_search
show_ann
quit

and command bin/allnn_exp <test.cmd >test.txt


or 

set_dim 2
read_data_pts normal100.txt
vnucko_build_tree
vnucko_entropy
ann_build_tree
ann_entropy
quit

the true entropy should be 2.8379


** <2011-04-05 Tue>

*** DONE make sure l2 NN search works, make experiments

The only changes should be the calculation of \eta and of a distance

This seems to be in bbox.h, distance, distanceLim, extDistance

Define LNORM in config.h to choose the norm. Later on we can also make it selectable at run time.

extDistance (distance from a point to an exterior of a box is the same
for l2 and linf because the closest point is always an orthogonal projection on one of the faces.

Run like 

~/work/increst/tmp]> ../bin/allnn_exp <../src/test.cmd

with test.cmd:

set_dim 2
read_data_pts normal100.txt
vnucko_build_tree
vnucko_check
quit

I found that I was actually using the l_inf norm.

Corrected, now it seems to work. Bzr revision 71

** <2011-04-06 Wed>

*** DONE implement vnucko_check, ann_check

vnucko_check seems to work

~/work/increst/tmp]> ../bin/allnn_exp <../src/test.cmd

ann_check compiles and seems to run. bzr revision 73

** <2011-04-06 Wed>

** <2011-04-08 Fri>

*** DONE brute force entropy

ann_entropy and vnucko_entropy does not give the same results

../bin/allnn_exp <../src/testentropy.cmd 

Brute force gives correct entropy in ANNEntropy

the distance seems to be incorrect

OK, it works now, ANN was giving squared distances.
BBF is faster:

ANN entropy=2.95788, time=6.1e-05 seconds
BBF entropy=2.95788, time=4.9e-05 seconds.

true value is log((2 pi e)^k det(Sigma)) = 2.837877

for N=1e5, BBF is still faster:

All NN build tree took 0.037 seconds
ANN entropy=2.83028, time=0.15 seconds
Building the tree took 0.049 seconds.
BBF entropy=2.83028, time=0.064 seconds


** <2011-04-13 Wed>

*** DONE install FLANN 

installed FLANN

automatic tuning takes a lot of time (27s), replaced by fixed parameters

FLANN does not return indexes of the NN (only 0).  bzr rev 76.
I am using the C interface
I will ignore that for the moment and just take the (squared) distance returned.

FLANN works but takes 2s. bzr revision 77

*** TODO install LSH

installing lshkit-0.2.1

installing oNBNN (http://www.minutebutterfly.de/pro)

compiling as described in the README

added code to allnn_exp, new commands lsh_build_tree and lsh_entropy

**** changed mplsh_nn_dist in mplsh-custom.h to skip the first NN (the
query point itself)

  template<class D, class A, class M> 
  float mplsh_nn_dist(const D& query, mplsh_custom::MultiProbeLshIndex<D,A,M>& index, int T)
  {
    float dist = -1;
    int n = 2; // changed by JK
    // while(dist < 0)
    //{
      lshkit::Topk<unsigned> topk;
      topk.reset((unsigned)(n));
      index.query( &query, &topk, (unsigned)T );
      // if( topk[n-1].dist < 3.4e38 )
        dist = topk[n-1].dist;
      // n++;
      //}
    return dist;
  }

**** LSH is often right but sometimes it is wrong and when it is wrong, it is very wrong...

We will remove onbnn, bzr revision 80 (before)


*** DONE try Caltech large scale image search toolbox

http://www.vision.caltech.edu/malaa/software/research/image-search/

working on the integration, bzr rev 81

compiles, bzr rev 82

looks like it is working but extremely long.
After setting ntables=1, it is faster but not by much.

with LSH, sometimes the NN is not found...

this is improved by using several tables

** <2011-04-18 Mon>

new command brute_entropy

commands set_nlshfuncs, set_nlshtables

testentropy.cmd:

brute force 365s, H=2.83028
ANN, 0.13+0.44=0.57s
BBF, 0.16+0.23=0.39s
FLANN, 0.47+7.24s=7.71s
LSH, 0.97+48.96s=49.93s (bad accuracy)
LSH,            =       (good accuracy)
 
** <2011-04-18 Mon>

preparerotdata.m - prepare rotational data

evalrotdata.m - evaluates the entropy. For neighborhood size 1 it
takes about 30s.

evalrot calculated up to d=5, angle=-3.8


** <2011-04-19 Tue>

showrot - shows the dependence of the entropy on the rotation angle

rotate the initial image to avoid high entropy at 0 angle. 
Maybe smooth the image. 

Implement vnucko estimation for quick experiments

I will now be using a named pipe

new methods implemented, to be tested

** <2011-04-20 Wed>

There is something wrong with vnucko_entropy...
Corrected.

bbf-upd, d=1 takes 3.931s
bbf, d=1 takes 3.854s

brute, d=3 takes 366.4 s, H=38.4368 at +0deg
bbf, d=3 takes 13.1 s, H=38.4368 at +5deg (rel err negligible)
bbf, d=3 takes 4.18 s, rel err 0.64%
bbf-ann, d=3 takes 13.82s

ann, d=3 takes 9.12s, H=38.4369 (epsilon=0.1)
ann, d=3 takes 3.06s, relerr=0.5% (epsilon=0.9)

flann, d=3 takes 7.53s, H=38.4862 (0.1% error, checks 128, trees 4)





*** DONE we get segmentation fault for FLANN, correct

now calcentr starts its own allnn_exp process

bzr revision 91

*** DONE prepare parallel version

making a script startallnn that will start allnn_exp in the background

** <2011-04-21 Thu>


*** DONE implement other methods

*** Making evalbrute.sh


*** Determine good parameters

For 10% error, one shot

Note that dim=2 d^2

brute, d=1, H=1.60612, t=113.2s
ann, d=1, H=1.610640, t=0.27s (epsilon=0.9)
bbf, d=1, H=1.713, t=0.27s (maxvisited=20,leafsize=10)
lsh, d=1, H=1.674, t=137s (nlshfuncs=40,nlshtables=1)
flann, d=1, H=1.72848, t=0.59 (flannchecks=20, flanntrees=1)

brute, d=3, H=35.692, t=340.9s



--------------------
for 1% error (times on the laptop)
d=1, dim=2
brute, d=1, H=1.60612, t=113.2s
ann, d=1, H=1.607530, t=0.26 (epsilon=0.5)
bbf, d=1, H=1.607270, t=0.26 (maxvisited=60,leafsize=15) 
flann, d=1, H=1.6168, t=0.55 (flannchecks=100,flanntrees=1)
lsh, d=1, H=1.743,t=210s (lshfuncs=300,lshtables=4)
lsh, d=1, H=1.691,t=144s (lshfuncs=50,lshtables=1)
lsh, d=1, H=1.659,t=144s (lshfuncs=30,lshtables=1)

d=3, dim=18
brute, d=3, H=35.692, t=340.9s
ann, d=3, H=35.7015, t=5.12s (epsilon=0.5)
bbf, d=3, H=35.902, t=4.11s (maxvisited=300,leafsize=50)
flann, d=3, H=35.744, t=5.25s (flannchecks=150,flanntrees=1)
lsh, d=3, H=35.46, t=51.95 (nlshfuncs=500, nlshtables=4)

d=5, dim=50
brute,  d=5, dim=50, H=109.971000, time 673.290000 (on cmpgrid-72)
ann, d=5, H=109.995, time=24s (epsilon=0.9)
bbf, d=5, H=111.07, time=7.32s (maxvisited=400, leafsize=100)
flann, d=5, H=110.57, time=8.12s (flannchecks=150,flanntrees=1)
lsh, d=5, H=126, time=264 (nlshfuncs=500,nlshtables=10)

d=7, dim=98
brute, H=227.858, time 1335s (22min)
ann, H=227.893, time 81.99s (epsilon=0.9)
flann, H=229.39, time 10.19s (flannchecks=150, flanntrees=1)

bbf, H=229.7, time 15.33s (maxvisited=500, leafsize=100)
bbf, H=229.7, time 15.33s (maxvisited=400, leafsize=100)
bbf, H=231, time 13s (maxvisited=200, leafsize=50)
bbf, H=234, time 7.6s (maxvisited=100, leafsize=30)
bbf, H=230.319, time=12,72 (maxvisited=400, leafsize=100)
bbf, H=230.6, time=11.49 (maxvisited=500, leafsize=150)
bbf, H=229.7, time=15.1 (maxvisited=500, leafsize=100)

** <2011-04-22 Fri>

*** moved #define INCOMPLETEDISTANCE to config.h

*** TODO implement bbf-tbb


added parameter useTbb to CBBFTree
bzr revision 94

run bbf-tbb for all d
started, to be checked

*** TODO find good parameters for LSH, d=1

maybe we should change w. No, no improvement

tune parameters and run experiments for d=5,7,9 for lsh
run lsh for d=5 7 9

started on cmpgrid-72, to be checked

*** DONE check results of evalbrute, evalbrute2, evalfast, evallsh on cmpgrid-72
   (nohup.out, nohup[234].out)

Results look good. We should show the image
showrot(3,'brute')

and compare times of ann,bbf,bbf-ann,flann,lsh

Recalculate 1,brute

bbf bbf-upd saves about 3% for d=3, angs=[0 0.01] and

bbf-tbb is actually *faster* than bbf by about 8% for d=3



*** Saved results to ~/tmp/snapshot20110424

*** DONE Prepare a table for the article

rottable.m
saved to rottable1.tex in ~/tex/annsearch
bzr revision 96

*** TODO running experiments on d=1,2,3,4,5,6 and angs=-5:0.01:5
    verify when they finish, perhaps evalfast.sh will have to be rerun

    restarting, see nohup.out, nohup2.out

    still running on Mon, Apr 25, d=6 ang=-2 and d=5 ang=4.3

    restarted, check nohup_evalfast, nohup_evalfast2, nohup_evallsh, nohup_evalbrute

    evallsh finished

    evalfast2 finished
    
    evalfast finished

    divided rottable into two.
 
    the results for h=2, ANN and LSH are strange, redo!!!
    yes, the parameters were wrong

    we will not show bbf-upd, it does not seem to help much

    evaleven finished

    surprisingly, laptop is faster for bbf and flann, so we will rerun
    the experiments on the laptop

*** DONE try also even sized neighborhoods


*** TODO Redo experiments on cmpgrid-72

add other methods to testndep -> testndep0, 
testndep.mat -> testndep2.mat

ndep should be an "exact search", so LSH is not used

running on cmpgrid-72, bzr rev 98

edited ndepgraph, ndeptable

did not finish completely, check

ndepgraph for d=5 looks usable

testndep3 will allow to run each method independently

now running

results visualized by ndepgraph2.m

*** DONE testdistr+tabledistr2

creating testdistr2

tabledistr3

the results are almost OK, except FLANN
this will be done in testdistr2flann
 
results OK, bzr revision 109

*** DONE entrgraph2. testentrbbf, testentrann

check if it finished

testentrbbf finished

testentrflann started

rerunning testentrbbf, saving to testentrbbf1e5l, with variable leaf size

testentrbbf finished

testentrflann crashes


testentrbbf+testentrann+testentrflann with d=30 probably could be shown

trying with d=50

usable entrgraph3 -> entrgraphcomb.eps   bzr rev 107


*** DONE running testupdate

*** DONE rerun testentrbbf/ann/flann on the laptop, reduced set of
    parameters to reduce clutter, entrgraph3

    I should compare with the brute force results, for image data

    testentrbbf2
    testentrann2
    testentrflann2


*** TODO theoretical analysis speedup

*** TODO theoretical analysis TBB

*** TODO find good examples

*** TODO make the article to be shorter and more direct

*** TODO compare for a real registration task

** <2011-04-29 Fri>

removing sqrt, we shall return the square of the distance

now everything is faster, bzr revision 111

INCOMPLETEDISTANCE not useful.

now rerunning experiments

*** DONE testndep3

    bbf
    brute
    flann
    ann

    all started
    did not finish for 1e6
  
    restart with linf

    FLANN does not accept linf for kdtree

    ndepgraph3 will show the linf results 

    calling brute for linf

    FLANN for l2 takes 20658s=5h

    on the other hand, it takes 0 for n=100, which makes a problem in
    the graph...
 
    OK, accepted

*** DONE testdistr2

    started

    tabledistr3

    not started correctly, redoing


*** DONE evalrotdata

    started once more with linf norm, will combine

    not good, just show l2 norm


*** DONE testentr

    will not show

    trying to combine bbftrymany and evalrot



    testentrbbf5+testentrann2+testentrflann2 + entrgraph4
   
    now calculating testentrann2+testentrflann2 (saving to ../tmp,
    need to copy)
    with the l2 norm

*** DONE LSH in the last experiment, for d=1

    not done

*** Submitting, revision 116

** <2011-05-12 Thu>

** starting to work on image registration using FLANN

Matlab bindings are in ~/builds/flann-1.6.8-src/build/src/matlab/nearest_neighbors.mexa64

example:

addpath('~/builds/flann-1.6.8-src/src/matlab') ;
addpath('~/builds/flann-1.6.8-src/build/src/matlab') ;
params.algorithms='kdtree' ; params.trees=8 ; params.checks=64 ;
testset=single(rand(128,1000)) ;  
dataset=single(rand(128,10000)) ; 
[results,dists]=flann_search(dataset,testset,5,params) ; 

WARNING: Data must be single otherwise results are incorrect.

** new directory work/increst/registration

** implement entropy_klflann, test_entropy_klflann

works, bzr revision 117

** mutinf_klflann.m 

** neighbmi.m - neighborhood mutual information
   test_neighbmi

   looks good, just slow

   saving images to ~/work/increst/registration/results

*** TODO try for shift, try for normal images

** TODO textfiltmi.m

   (texture)

*** TODO color criterion


** TODO imageregistration

** <2011-05-17 Tue>

created testcrit, run like testcrit('cam','r','ssd')


[f,g,t,y]=testcrit('came','r','nbmi') ; - results good

MI fails for the sqrs example, while nbmi works

lenae (with gradient) is also a good example
with gradient 0.25 it fails nicely

'text' is a good example with translation

** <2011-08-24 Wed>

for the ANN sigproc example, test the formulas by mindist.m
aspectratio*.m

nndistr.m plots analytical formulas for NN distance distributions

looks good even though there are differences wrt to analytic version
linear or even quadratic have an error less than 1%

** <2011-08-29 Mon>

The accuracy problem seems to come from approximating (1-alpha
r^d)^(v-1) around zero only.

** <2011-08-30 Tue>

polynomial approximation of t^(1/d) is tested in approxfrpow.m

we are almost there - approximation of i1 seems to be OK,
approximation of i0 not yet there

correct formula found, bzr revision 

*** TODO calculate real dependencies of errors vs L and d

*** TODO how come the error decreases with d ?!?!?! bzr revision 130

** <2011-08-31 Wed>

*** nndistr3 - shows dependence of the error on 'd'. There are some
numerical problems for higher 'd' so we shall only show for d up to
25. bzr revision 131

*** I implemented new commands in allnn_exp.cpp
    vnucko_ptrue - percentage of correctly found NN

    vnucko_relerr - relative error

    bzr revision 133

*** Now implementing testptrue.m and ptruegraphs to generate the graphs 

** <2011-09-01 Thu>

*** DONE make the same graphs using the theoretical formulas 

ptruesimgraphs.m

Gives some results but not that similar to real results. bzr revision 135

*** DONE rerun testptrue with finer list of Vs and ds and higher nrep

running now with n=1e4, nrep=10
   
