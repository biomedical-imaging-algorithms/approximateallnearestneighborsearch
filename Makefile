# Options
MAKE = make
# Directories
SRC_DIR = ./src
# Building targets
.PHONY: all experiments allnn_exp lean lib
allnn_exp:
	@cd $(SRC_DIR); $(MAKE) -f Makefile allnn_exp

lib:
	@cd $(SRC_DIR); $(MAKE) -f Makefile

all_old:
	@cd $(SRC_DIR); $(MAKE) -f Makefile all

experiments:
	@cd $(SRC_DIR); $(MAKE) -f Makefile experiments

clean:
	@cd $(SRC_DIR); $(MAKE) -f Makefile clean
  